export const chartsColors = ['#d90000', '#007aea', '#009f3c', '#ff6600', '#ff8080', '#4fdaff', '#97e636', '#ffc02e']
export const areaColors = [
    'rgba(255, 0, 0, 0.3)',
    'rgba(0, 122, 234, 0.3)',
    'rgba(0, 159, 60, 0.3)',
    'rgba(255, 102, 0, 0.3)',
    'rgba(255, 122, 122, 0.3)',
    'rgba(79, 218, 255, 0.3)',
    'rgba(151, 230, 54, 0.3)',
    'rgba(255, 192, 46, 0.3)']

export let commonOptions = {
    title: {},
    tooltips:{
        show: true,
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    grid: {},
    legend: [],
    xAxis: [],
    yAxis: [],
    dataZoom: [],
    series: []
}

export let chartsOptions = {
    title: {},

    tooltips:{
        show: true,
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },

    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 68,
        containLabel: true
    },

    legend: [],

    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            show: true,
            lineStyle: {
                color: '#019afa',
                width: 2
            }
        },
        splitLine: {
            show: true,
            lineStyle: {
                type: 'dotted',
                color: '#0d9ef9'
            }
        },
        axisTick: {
            show: false
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            textStyle: {
                fontSize: 14
            },
        }
    }],

    yAxis: [
        {
            type: 'value',
            nameLocation: 'end',
            // min: -100,
            // max: 100,
            nameTextStyle: {
                color: '#999',
                align: 'center'
            },
            axisTick: {
                show: false
            },
            axisLabel: {
                show: false,
                typeStyle: {
                    color: '#333'
                },
                color: '#999',
                formatter: '{value} '
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc'
                }
            },
            splitLine: {
                show: true,
                lineStyle: {
                    type: 'dotted',
                    color: '#0d9ef9'
                }
            }
        }
    ],

    dataZoom: [],

    series: []
}

export let linesOptions =  {
    title: {
        top: '0',
        left: 'center'
    },
    tooltip: {
        trigger: 'axis',
        formatter: (value) => {
            let itemDesc = `${value[0].name}<br />`
            value.forEach(item => {
                itemDesc += `${item.marker}${item.seriesName}：${parseFloat(item.value).toFixed(2)}%<br />`
            })

            return itemDesc
        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    legend: {
        x: 'center',
        icon: 'line',
        bottom: 0,
        data: []
    },
    dataZoom: [{
            type: 'slider',
            show: true,
            height: 30,
            left: '70px',
            right: '70px',
            bottom: '5%',
            start: 0,
            end: 100,
            textStyle: {
                color: '#999',
                fontSize: 11,
            },
        }, {
            type: 'inside'
        }

    ],
    grid: {
        right: '46px',
        bottom: '15%',
        left: '46px',
        top: '80px',
        containLabel: false
    },
    xAxis: [{
        type: 'category',
        data: [],
        nameTextStyle: {
            color: '#999'
        },
        axisLine: {
            onZero: false,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisTick: {
            show: true,
            position: 'bottom',
            inside: false
        },
        axisLabel: {
            show: true,
            textStyle: {
                color: "#666",
                fontSize: 12,
            }
        },
    }],
    yAxis: [{
        type: 'value',
        name: '累计收益率（%）',
        nameTextStyle: {
            color: '#999'
        },
        position: 'left',
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
                type: 'dotted'
            }
        },
        splitLine: {
            lineStyle: {
                type: 'solid',
                color: "#ccc",
            }

        },
        axisLabel: {
            color: '#666',
            fontSize: 12,
        }
    }, ],
    series: []
}

export let areasOptions = {
    title: {
        top: '0',
        left: 'center'
    },
    tooltip: {
        trigger: 'axis',
        formatter: (value) => {
            return `${value[0].name}<br />${value[0].seriesName}：${value[0].marker}${parseFloat(value[0].value).toFixed(2)}%`
        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    legend: {
        x: 'center',
        icon: 'line',
        bottom: 0,
        data: []
    },
    dataZoom: [{
            type: 'slider',
            show: true,
            height: 30,
            left: '70px',
            right: '70px',
            bottom: '5%',
            start: 0,
            end: 100,
            textStyle: {
                color: '#999',
                fontSize: 11,
            },
        }, {
            type: 'inside'
        }

    ],
    grid: {
        right: '46px',
        bottom: '15%',
        left: '46px',
        top: '80px',
        containLabel: false
    },
    xAxis: [{
        type: 'category',
        data: [],
        nameTextStyle: {
            color: '#999'
        },
        axisLine: {
            onZero: false,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisTick: {
            show: true,
        },
        axisLabel: {
            show: true,
            textStyle: {
                color: "#666",
                fontSize: 12,
            }
        },
    }],
    yAxis: [{
        type: 'value',
        name: '动态回撤（%）',
        nameTextStyle: {
            color: '#999'
        },
        position: 'left',
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc',
                type: 'dotted'
            }
        },
        splitLine: {
            lineStyle: {
                type: 'dotted',
                color: "#ccc",
            }

        },
        axisLabel: {
            color: '#666',
            fontSize: 12,
        }
    }, ],
    series: []
}

export let barsOptions = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 68
    },
    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            show: true,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            textStyle: {
                fontSize: 14
            },
        },
    }],
    yAxis: [{
        name: '频数(%)',
        nameLocation: 'end',
        nameTextStyle: {
            color: '#999',
            align: 'left'
        },
        color: '#ccc',
        axisLabel: {
            show: true,
            typeStyle: {
                color: '#333'
            },
            color: '#999'
        },
        axisLine: {
            show: false,
            lineStyle: {
                color: '#666'
            }
        },
        splitLine: {
            lineStyle: {
                type: 'dotted',
                color: '#ccc'
            }
        }
    }],
    dataZoom: [
        {
            type: '',
            height: 30,
            left: '70px',
            right: '70px',
            bottom: 0,
            show: false
        }
    ],
    series: []
}

export let correlationsOption = {
    tooltip: {
        trigger: 'axis',
        formatter: (a) => {
            return `${a[0].name}<br />${a[0].marker}${parseFloat(a[0].value).toFixed(2)}%`
        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 68
    },
    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            onZero: false,
            show: true,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisTick: {
            alignWithLabel: false,
            interval: 0
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            lineHeight: 20,
            textStyle: {
                fontSize: 14
            },
            formatter: (a) => {
                return  `{a|${a.replace('-', '\n')}}`
            },
            rich: {
                a: {
                    fontSize: 13,
                    lineHeight: 20
                }
            }
        },
    }],
    yAxis: [{
        name: '相关性（%）',
        nameLocation: 'end',
        nameTextStyle: {
            color: '#999',
            align: 'left'
        },
        color: '#ccc',
        axisLabel: {
            show: true,
            typeStyle: {
                color: '#333'
            },
            color: '#999'
        },
        axisLine: {
            show: false,
            lineStyle: {
                color: '#ccc'
            }
        },
        splitLine: {
            number: 10,
            lineStyle: {
                type: 'dotted',
                color: '#ccc'
            }
        }
    }],
    dataZoom: [
        {
            type: '',
            height: 30,
            left: '70px',
            right: '70px',
            bottom: 0,
            show: false
        }
    ],
    series: []
}

export let radarOption = {
    title: {
        text: ''
    },
    tooltip: {},
    legend: {
        data: []
    },
    radar: {
        indicator: [],
        center: ['50%', '50%'],
        name: {
            textStyle: {
                color: '#666'
            }
        }
    },
    series: []
}
