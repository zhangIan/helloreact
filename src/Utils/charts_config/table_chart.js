const labelRight = {
    normal: {
        color: '#666',
        position: 'right'
    }
}
const itemS = {
    normal: {
        borderColor: '#55d580'
    }
}

let option = {
   width: '100%',
    tooltip : {
        show: false,
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'none'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    grid: {
        top: 0,
        bottom: 0,
        containLabel: true
    },
    xAxis: {
        min: -2,
        max: 2,
        type : 'value',
        position: 'top',
        axisTick: {show: false},
        axisLabel: {show: false},
        axisLine: {show: false},
        splitLine: {lineStyle:{type:'dashed'}, show: false},
    },
    yAxis: {
        type : 'category',
        offset: -14,
        axisLine: {show: true, lineStyle: {color: '#999', type:'dotted'}},
        axisLabel: {
            show: true, 
            textStyle:{
                color: '#333', 
                fontSize: 14,
                lineHeight: 24
            }, 
            formatter: function (params) {
            return params
        }, rich: {value: {lineHeight: 1}}},
        axisTick: {show: false},
        splitLine: {show: false},
        data : ['机械设备', '化工', '医药生物', '电子', '计算机', '电器设备', '有色金属', '传媒', '公用事业', '房地产', '汽车', '建筑装饰', '轻工制造',
                '农林牧渔', '纺织轻装', '商业贸易', '通讯', '交通运输', '家用电器', '综合', '交通运输', '国防军工', '采掘', '建筑材料', '食品饮料', '休闲服务', '钢铁', '非银金融', '银行'].reverse()
    },
    series : [
        {
            type:'bar',
            barWidth: '16px',
            label: {
                normal: {
                    show: true,
                    formatter: function (obj) {
                        if (obj.data.value > 0) {
                            obj.data.label.position = 'left'
                        }
                        
                        return obj.value + '%'
                    }
                }
            },
            itemStyle: {
                normal: {
                    borderWidth: 1,
                    borderColor: '#fe8282',
                    color: function (params) {
                        let colorSet = {
                            type: 'linear',
                            colorStops: [],
                        }

                        if (params.data.value > 0) {
                            params.data.itemStyle.borderColor  = '#fe8282'
                            colorSet.colorStops = [{
                                offset: 0,
                                color: '#fd7b7b'
                            }, {
                                offset: 1,
                                color: 'rgba(254,212,213,.7)'
                            }]
                        } else {
                           
                            colorSet.colorStops = [{
                                offset: 0,
                                color: '#b6f9ce'
                            }, {
                                offset: 1,
                                color: '#54d583'
                            }] 
                        }
                        
                        
                        return colorSet
                    },
                }
            },
            data:[
                {value: -0.07,itemStyle: itemS, label: labelRight},
                {value: -0.09, itemStyle: itemS, label: labelRight},
                {value: 0.2, itemStyle: itemS, label: labelRight},
                {value: 0.44, itemStyle: itemS, label: labelRight},
                {value: -0.23, itemStyle: itemS, label: labelRight},
                {value: 0.08, itemStyle: itemS, label: labelRight},
                {value: -0.17, itemStyle: itemS, label: labelRight},
                {value: 0.47, itemStyle: itemS, label: labelRight},
                {value: -0.36, itemStyle: itemS, label: labelRight},
                {value: 0.18, itemStyle: itemS, label: labelRight},
                {value: -0.07, itemStyle: itemS, label: labelRight},
                {value: -0.09, itemStyle: itemS, label: labelRight},
                {value: 0.2, itemStyle: itemS, label: labelRight},
                {value: 0.44, itemStyle: itemS, label: labelRight},
                {value: -0.23, itemStyle: itemS, label: labelRight},
                {value: 0.08, itemStyle: itemS, label: labelRight},
                {value: -0.17, itemStyle: itemS, label: labelRight},
                {value: 0.47, itemStyle: itemS, label: labelRight},
                {value: -0.36, itemStyle: itemS, label: labelRight},
                {value: 0.18, itemStyle: itemS, label: labelRight},
                {value: 0.2, itemStyle: itemS, label: labelRight},
                {value: 0.44, itemStyle: itemS, label: labelRight},
                {value: -0.23, itemStyle: itemS, label: labelRight},
                {value: 0.08, itemStyle: itemS, label: labelRight},
                {value: -0.17, itemStyle: itemS, label: labelRight},
                {value: 0.47, itemStyle: itemS, label: labelRight},
                {value: -0.36, itemStyle: itemS, label: labelRight},
                {value: 0.18, itemStyle: itemS, label: labelRight},
                {value: 0.18, itemStyle: itemS, label: labelRight}
            ]
        }
    ]
}

export default option