import React, { Component } from 'react'
import './system.sass'
import SystemStyle from './system.module.sass'
import SystemLogo from '../../Image/system_logo.png'
import Swiper from 'swiper'
import 'swiper/dist/css/swiper.min.css'
import cookie from 'js-cookie'

export default class SystemList extends Component {
    constructor (...props) {
        super(...props)
        this.state ={}
    }

    componentDidMount () {
        document.getElementsByTagName('body')[0].setAttribute('class', 'system_wrapper')
        ;(() => {
            new Swiper('.swiper-container', {
                autoplay: false,
                loop: true,
                pagination: {
                  el: '.swiper-pagination',
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                  }
              })
        })()
    }

    componentWillUnmount () {
        document.getElementsByTagName('body')[0].setAttribute('class', '')
    }

    renderList = () => {
        const token = cookie.get('token')
        const url = process.env.NODE_ENV === 'development' ? 'http://192.168.122.190:8090/netFilter/' : 'http://192.168.1.51:8090/netFilter/'
        const backUrl = process.env.REACT_APP_BACK_URL
        return (
        <div className = "swiper-slide">
            <ul className={`system_list_items`}>
                <li><a href={`${url}data/assets/platform?token=${token ? token : ''}`}>数据资产平台</a></li>
                <li><a  href={`${url}process/management/system?token=${token ? token : ''}`}>流程管理系统</a></li>
                <li><a href={`${url}group/oa/system?token=${token ? token : ''}`}>集团oa系统</a></li>
                <li><a href='/'>汇升资产管理配置系统</a></li>
                <li><a href={`${backUrl}`}>汇升资产配置后台系统</a></li>
                <li><a href='/'>量化MOM多策略绩效系统</a></li>
                <li><a href='/'>量化MOM多策略绩效系统</a></li>
                <li><a href='/'>量化MOM多策略绩效系统</a></li>
            </ul>
        </div>)
    }

    render () {
        return (
            <div className={SystemStyle.system_container}>
                <div className={SystemStyle.system_header}>
                    <img src={SystemLogo} alt='' />
                </div>
                <div className={SystemStyle.system_title}>
                    <h2>统一授权登录平台</h2>
                </div>

                <div className={SystemStyle.swiper_wrapper}>
                    <div className = "swiper-container">
                        <div className = "swiper-wrapper">
                            {this.renderList()}
                            {this.renderList()}
                        </div> 
                        <div className = "swiper-pagination customer-pointer"></div>
                        <div className="swiper-button-next swiper_button iconfont iconr customer_button_next"></div>
                        <div className="swiper-button-prev swiper_button iconfont iconz customer_button_prev"></div>
                    </div>
                </div>

                <div className={SystemStyle.system_position_img}></div>
            </div>
        )
    }
}
