import React, { Component } from 'react'
import 'antd/dist/antd.css'
import './register.sass'
import LogoImg from '../../Image/logo.png'
import Swiper from 'swiper/dist/js/swiper.js'
import { Tabs, Radio } from 'antd'
import Qrcode from '../../Image/qrcode.png'
import ScrollImg from '../../Image/02.png'

import ComForm from '../common/form'

const { TabPane } = Tabs

export default class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            size: 'small',
            defaultActiveKey: '1'
        }
    }


    change = key => {
        this.setState({
            defaultActiveKey: key
        })
    }

    checkRadio = e => {
    }

    radioChange = e => {
    }

    componentDidMount () {
        document.getElementsByTagName('body')[0].setAttribute('class', 'register_container_wrapper')
        ;(() => {
            new Swiper('.swiper-container', {
                autoplay: true,
                loop: true,
                pagination: {
                  el: '.swiper-pagination',
                }
              })
        })()
    }

    componentWillUnmount () {
        document.getElementsByTagName('body')[0].setAttribute('class', '')
    }

    render () {
        return (
            <div className="container">
                <header className="container-header">
                    <div className="header-content">
                        <div className="logo-img">
                            <img src={LogoImg} alt="logo img" />
                        </div>
                        <div className="nav-list">
                            <ul className="clear-fix">
                                <li><a href="/link">基金管理</a></li>
                                <li><a href="/link">投资管理</a></li>
                                <li><a href="/link">资产配置</a></li>
                                <li><a href="/link">投后管理</a></li>
                            </ul>
                        </div>
                    </div>
                </header>

                <div className="content">
                    <div className = "swiper-container">
                        <div className = "swiper-wrapper">
                            <div className = "swiper-slide">
                                <div className="position-swiper">
                                    <img src={ScrollImg} alt="scroll" />
                                </div>
                                <div className = "box4"></div>
                            </div>
                            <div className = "swiper-slide">
                                <div className="position-swiper">
                                    <img src={ScrollImg} alt="scroll" />
                                </div> 
                                <div className="box1"></div>
                            </div> 
                            <div className = "swiper-slide">
                                <div className="position-swiper">
                                    <img src={ScrollImg} alt="scroll" />
                                </div>
                                <div className ="box2"></div>
                            </div> 
                            <div className = "swiper-slide">
                                <div className="position-swiper">
                                    <img src={ScrollImg} alt="scroll" />
                                </div>
                                < div className ="box3"></div>
                            </div>
                        </div> 
                        <div className = "swiper-pagination customer-pointer"></div>
                    </div>


                    <div className="form-content">
                        <Tabs activeKey={this.state.defaultActiveKey} onChange={this.change} size={this.state.size}>
                            <TabPane tab="用户名密码登录" key="1" className="common-form">
                                <ComForm {...this.state} {...this.props} />
                                <ul className="radio-list">
                                        <li>
                                            <Radio onClick={this.checkRadio} onChange={this.radioChange} className="agree-radio"  />
                                            <p className="color-666 font-13">我已阅读并同意汇升投资<a href="www.baidu.com">《用户服务协议》</a>及<a href="www.baidu.com">《隐私保护指引》</a></p>
                                        </li>
                                        <li>
                                            <Radio className="agree-radio"  />
                                            <p className="color-333 font-13">本人承诺符合特定的合格投资者条件</p>
                                        </li>
                                        <li>
                                            <span></span>
                                            <p className="color-666 font-12">根据《私募投资基金募集行为管理办法》之规定，汇升投资只对“具有相应风险识别能力和风险承担能力，投资于单只私募基金的金额不低于100万元，且个人金融资产不低于300万元或者最近三年个人年均收入不低于50万元”的特定投资者宣传、推介相关私募投资基金产品。您需要进行身份认证后才能访问。</p>
                                        </li>
                                </ul>
                            </TabPane>
                            <TabPane tab="手机扫码登录" key="2" className="common-form">
                                <div className="qrcode-scan">
                                    <div className="scan-img">
                                        <img src= {Qrcode} alt="scan" />
                                    </div>
                                </div>
                                <div className="agree-desc">
                                    <ul className="radio-list">
                                            <li>
                                                <Radio className="agree-radio"  />
                                                <p className="color-666 font-13">我已阅读并同意汇升投资<a href="www.baidu.com">《用户服务协议》</a>及<a href="www.baidu.com">《隐私保护指引》</a></p>
                                            </li>
                                            <li>
                                                <Radio className="agree-radio"  />
                                                <p className="color-333 font-13">本人承诺符合特定的合格投资者条件</p>
                                            </li>
                                            <li>
                                                <span></span>
                                                <p className="color-666 font-12">根据《私募投资基金募集行为管理办法》之规定，汇升投资只对“具有相应风险识别能力和风险承担能力，投资于单只私募基金的金额不低于100万元，且个人金融资产不低于300万元或者最近三年个人年均收入不低于50万元”的特定投资者宣传、推介相关私募投资基金产品。您需要进行身份认证后才能访问。</p>
                                            </li>
                                    </ul>
                                </div>
                            </TabPane>
                        </Tabs>
                    </div>
                </div>
            </div>
        )
    }
}