import React, { Component } from 'react'
// import Header from '../layout/inner_header'
import Header from '../layout/product_header'
import home from './home.module.sass'
import SearchTab from '../common/home_tab_search'
import Mine from '../common/home/mine'
import HomeAdvisor from '../common/home/home_advisor'
import './home.sass'
import { Affix, Button, Tabs, Icon, Empty } from 'antd'
import cookie from 'js-cookie'


const Tabpane = Tabs.TabPane
class Home extends  Component{
  constructor (props) {
    super(props)
    this.state ={
      defaultKey: '1',
      visible: false,
      bottom: '40',
      compareStuta: false,
      attentionArr: window.sessionStorage.getItem('attentionArr') ? JSON.parse(window.sessionStorage.getItem('attentionArr')) : {
        fundId: [],
        fundName: []
      },
      tabpane: ['首页', '投顾']
    }
  }

  UNSAFE_componentWillMount () {
    document.getElementsByTagName('body')[0].setAttribute('class', 'home_pages')
  }

  componentWillUnmount () {
    const tokenStatus = cookie.get('token')

    console.log(tokenStatus)
    if (!tokenStatus) {
      window.location.href = '/first-login'
    }
    document.getElementsByTagName('body')[0].setAttribute('class', '')
  }

  componentDidMount () {
    // document.addEventListener('click', (e) => {
    //   console.log(1)
    //   e.stopPropagation()
    //   if (this.state.compareStuta) {
    //     this.setState({compareStuta: false})
    //   }
    // }, false)
  }

  onchange = (key) => {
    this.setState({
      defaultKey: key
    })
  }

  setAttentions = (id, name, status) => {
    // console.log(id, name, status)
    let tempId = this.state.attentionArr.fundId
    let tempName = this.state.attentionArr.fundName
    let tempAttention = {}
    if (status) {
      tempId.push(id)
      tempName.push(name)
    } else {
      tempId = tempId.filter(item => item !== id)
      tempName = tempName.filter(item => item !== name)
    }

    tempAttention.fundId = tempId
    tempAttention.fundName = tempName
    
    this.setState({ attentionArr: tempAttention, compareStuta: true})
    window.sessionStorage.setItem('attentionArr', [JSON.stringify(tempAttention)])
    window.sessionStorage.setItem('stroageId', tempId)
    console.log(window.sessionStorage)
    tempAttention = null
  }

  dropAttention = (e) => {
    const index = e.currentTarget.getAttribute('data-index')
    let temArr = this.state.attentionArr
    temArr.fundId.splice(index, 1)
    temArr.fundName.splice(index, 1)
    this.setState({ attentionArr: temArr})
    
    window.sessionStorage.setItem('attentionArr', [JSON.stringify(temArr)])
    window.sessionStorage.setItem('stroageId', temArr.fundId)
    window.updateStorage()
  }


  compareList = () => {
    this.setState({visible: !this.state.visible})
  }

  clearCompare = () => {
    let temArr = this.state.attentionArr
    temArr.fundId = []
    temArr.fundName = []
    this.setState({ attentionArr: temArr})
    window.sessionStorage.setItem('attentionArr', [JSON.stringify(temArr)])
    window.sessionStorage.setItem('stroageId', temArr.fundId)
    window.updateStorage()
  }

  doCompare = () => {
    const attentionArr = JSON.parse(window.sessionStorage.getItem('attentionArr'))
    if (attentionArr.fundId.length > 1) {
      this.props.history.push({pathname: '/compare', params: '111'})
    }
  }

  renderCompare = () => {
    if (this.state.attentionArr.fundName.length >0) {
      return (<dl className={home.compare_list_items}>
        <dt><span>产品对比列表</span></dt>
        {
          this.state.attentionArr.fundName.map((item, index) => {
          return (<dd key={index}><span>{index+1}</span><a href={`/attention?productId=${this.state.attentionArr.fundId[index]}`}>{item}</a><Icon data-index={index} onClick={(event) => {this.dropAttention(event)}} className='drop_attention' type='delete'/></dd>)
          })
        
        }
        <div className={home.compare_btns_wrapper}><Button className={home.compare_clear_btn} onClick={this.clearCompare}>清空</Button><Button onClick={this.doCompare} className={home.compare_compare_btn}>对比</Button></div>
      </dl>)
    } else {
      return (<dl className={home.compare_list_items}>
        <dt><span>产品对比列表</span></dt>
        <dd><Empty description={'暂无数据'} /></dd>
        <div className={home.compare_btns_wrapper}><Button className={home.compare_clear_btn} onClick={this.clearCompare}>清空</Button><Button className={home.compare_compare_btn}>对比</Button></div>
      </dl>)
    }
  }

  setCompare (e) {
    e.stopPropagation()
    this.setState({compareStuta: !this.state.compareStuta})
  }
  render(){
    const  tabpane = this.state.tabpane
    return(
      <div className={home.home_wrapper + ' home_global'}>
        <Header />
        
        <div className={home.home_container}>
          <div className={home.home_content}>
            <Tabs activeKey={this.state.defaultKey} onChange={this.onchange} className={home.home_tabs}>
              {tabpane.map((item, index) => (<Tabpane tab={item} key={index + 1}>
                {
                  index === 0 ? <SearchTab setAttentions={this.setAttentions} /> : 
                  index === 1 ? <HomeAdvisor setAttentions={this.setAttentions} /> : 
                  index === 1 ? <Mine setAttentions={this.setAttentions} /> : ''
                }
              </Tabpane>))}
            </Tabs>
            
          </div>
        </div>
        <Affix style={{position: 'fixed', bottom: '40%', right: '5px', zIndex: '999'}}>
          <div>
          <Button onClick={this.setCompare.bind(this)} className={`${home.home_compare_btn}`} type='link'><span>对比</span><i style={{display: 'none'}}>7条</i></Button>
          {this.state.compareStuta ? this.renderCompare() : ''}
          </div>
        </Affix>
      </div>
    )
  }
}
export default Home