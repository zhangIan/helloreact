import React, { Component } from 'react'
import { Layout, Tabs, message } from 'antd'
import InnerHeader from '../layout/product_header'
import './advisor.sass'
import ProductStyle from './advisor.module.sass'

import Performance from '../common/advisor_details/performance'
import Company from '../common/advisor_details/company'
import SubBrand from '../common/advisor_details/sub_brand'
import AdvisorTitle from '../common/advisor_details/advisor_title'
import cookie from 'js-cookie'
import { axios } from '../../Utils/index'
import url from 'url'
import qs from 'querystring'

const { Content, Sider } = Layout
const TabPane = Tabs.TabPane

export default class Product extends Component {
    constructor (props) {
        super(props)
        this.state = {
            noTitleKey: 'performance',
            collapsed: true,
            collapasedTrigger: null,
            collapasedWidth: 0
        }
    }

    componentDidMount () {
        document.getElementsByTagName('body')[0].setAttribute('class', 'product-wrapper product_details')
        // this._intAdvisorData()
    }

    onTabChange = (key, type) => {
        this.setState({ [type]: key })
    }

    // 初始化
    async _intAdvisorData () {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
            method: 'POST', 
            url: '/api/hszcpz/org/init', 
            data: qs.stringify({
                token: cookie.get('token'),
                c_org_id: query.id
            })
        })
    
        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }
        
        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)
        
        if (result.result === 'success') {
            // console.log(result)
            return result.orgInfo
        } else {
            message.error('获取数据失败')
        }
  
    }

    componentWillUnmount () {
        document.getElementsByTagName('body')[0].setAttribute('class', '')
    }

    toggleWidth = () => {
        let siderWidth = this.state.collapasedWidth

        if (siderWidth === 0) {
            this.setState({
                collapasedWidth: 200
            })
        } else {
            this.setState({
                collapasedWidth: 0
            }) 
        }
    }

    render () {
          
        return(
            <div className={ProductStyle.product_wrapper}>
                <Layout>
                    <Sider className={ProductStyle.sider_left} theme='light' trigger={this.state.collapasedTrigger} collapsedWidth={this.state.collapasedWidth} collapsible={true} collapsed={this.state.collapsed}>
                        
                    </Sider>
                    <Layout>
                        <InnerHeader />
                        <Content className='mt_20 advisor_container'>
                            <AdvisorTitle _getDetails={this._intAdvisorData.bind(this)} />
                            <Tabs defaultActiveKey="1" className={`tabs_items`}>
                                <TabPane className={`tabs_item`} tab="业绩评价" key="1">
                                    <Performance /> 
                                </TabPane>
                                <TabPane className={`tabs_item`} tab="公司简介" key="2">
                                    <Company /> 
                                </TabPane>
                                <TabPane className={`tabs_item`} tab="旗下产品" key="3">
                                    <SubBrand /> 
                                </TabPane>
                                
                            </Tabs>
                        </Content>
                    </Layout>
                </Layout>
           </div>
        )
    }
}
