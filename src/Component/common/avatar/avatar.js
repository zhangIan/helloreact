import React, { Component } from 'react'

export default class Avatar extends Component {
    render () {
        const { imgSrc, imgTit } = this.props
        return <img src={imgSrc} alt={imgTit} />
    }
}