import React, { Component } from 'react'

import { Card, Icon, Button, Select, Checkbox, DatePicker, Table } from 'antd'
import  echarts from 'echarts'
import locale from 'antd/es/date-picker/locale/zh_CN'


const { Option } = Select



export default class Performance extends Component {
    constructor (props) {
        super(props)

        this.state = {
            topList: {
                key: 'topLeft',
                tab: <Icon type='bell' />
            },
            lfCharts: {
                title: '净值走势图'
            },
            dataSource: [
                {
                    key: 1,
                    year: '2019',
                    jan: '36.7%',
                    feb: '36.7%',
                    mar: '36.7%',
                    apr: '36.7%',
                    may: '36.7%',
                    jun: '36.7%',
                    jul: '36.7%',
                    aug: '36.7%',
                    sep: '36.7%',
                    oct: '36.7%',
                    nov: '36.7%',
                    dec: '36.7%',
                    yearly: '36.7%',
                },
                {
                    key: 2,
                    year: '2018',
                    jan: '36.7%',
                    feb: '36.7%',
                    mar: '36.7%',
                    apr: '36.7%',
                    may: '36.7%',
                    jun: '36.7%',
                    jul: '36.7%',
                    aug: '36.7%',
                    sep: '36.7%',
                    oct: '36.7%',
                    nov: '36.7%',
                    dec: '36.7%',
                    yearly: '36.7%',
                },
                {
                    key: 3,
                    year: '2017',
                    jan: '36.7%',
                    feb: '36.7%',
                    mar: '36.7%',
                    apr: '36.7%',
                    may: '36.7%',
                    jun: '36.7%',
                    jul: '36.7%',
                    aug: '36.7%',
                    sep: '36.7%',
                    oct: '36.7%',
                    nov: '36.7%',
                    dec: '36.7%',
                    yearly: '36.7%',
                }
            ],
            columns: [
                {
                    key: '1',
                    title: () => <span className='performance_tit_color'>年度</span>,
                    dataIndex: 'year',
                    render: (text) => <span className='performance_tit_color'>{text}</span>
                },
                {
                    key: '2',
                    title: <span className='performance_tit_color'>一月</span>,
                    dataIndex: 'jan'
                },
                {
                    key: '3',
                    title: <span className='performance_tit_color'>二月</span>,
                    dataIndex: 'feb'
                },
                {
                    key: '4',
                    title:  <span className='performance_tit_color'>三月</span>,
                    dataIndex: 'mar'
                },
                {
                    key: '5',
                    title:  <span className='performance_tit_color'>四月</span>,
                    dataIndex: 'apr'
                },
                {
                    key: '6',
                    title:  <span className='performance_tit_color'>五月</span>,
                    dataIndex: 'may'
                },
                {
                    key: '7',
                    title:  <span className='performance_tit_color'>六月</span>,
                    dataIndex: 'jun'
                },
                {
                    key: '8',
                    title:  <span className='performance_tit_color'>七月</span>,
                    dataIndex: 'jul'
                },
                {
                    key: '9',
                    title:  <span className='performance_tit_color'>八月</span>,
                    dataIndex: 'aug'
                },
                {
                    key: '10',
                    title:  <span className='performance_tit_color'>九月</span>,
                    dataIndex: 'sep'
                },
                {
                    key: '11',
                    title:  <span className='performance_tit_color'>十月</span>,
                    dataIndex: 'oct'
                },
                {
                    key: '12',
                    title:  <span className='performance_tit_color'>十一月</span>,
                    dataIndex: 'nov'
                },
                {
                    key: '13',
                    title:  <span className='performance_tit_color'>十二月</span>,
                    dataIndex: 'dec'
                },
                {
                    key: '14',
                    title:  <span className='performance_tit_color'>全年</span>,
                    dataIndex: 'yearly'
                }
            ],

            historyDataSource: [
                {
                    key: 1,
                    items: '收益率',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    nearlyYear: '200.89%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                },
                {
                    key: 2,
                    items: '同策略指标',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                },
                {
                    key: 3,
                    items: '同策略排名',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                },
                {
                    key: 4,
                    items: '同策略10%分位值',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                },
                {
                    key: 5,
                    items: '同策略20%分位值',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                },
                {
                    key: 6,
                    items: '同策略30%分位值',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                },
                {
                    key: 7,
                    items: '同策略30%分位值',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                },
                {
                    key: 8,
                    items: '同策略30%分位值',
                    sinceYear: '379.67%',
                    latestMonth: '5.44%',
                    nearlyThree: '41.60%',
                    nearlySix: '124.28%',
                    recentTwo: '366.88%',
                    recentThree: '341.37%'
                }
            ],
            historyColumns: [
                {
                    key: '1',
                    title: <span className='performance_tit_color'>统计项</span>,
                    dataIndex: 'items'
                },
                {
                    key: '2',
                    title: <span className='performance_tit_color'>今年以来</span>,
                    dataIndex: 'sinceYear'
                },
                {
                    key: '3',
                    title: <span className='performance_tit_color'>近一月</span>,
                    dataIndex: 'latestMonth'
                },
                {
                    key: '4',
                    title: <span className='performance_tit_color'>近三月</span>,
                    dataIndex: 'nearlyThree' 
                },
                {
                    key: '5',
                    title: <span className='performance_tit_color'>近半年</span>,
                    dataIndex: 'nearlySix' 
                },
                {
                    key: '6',
                    title: <span className='performance_tit_color'>近一年</span>,
                    dataIndex: 'nearlyYear' 
                },
                {
                    key: '7',
                    title: <span className='performance_tit_color'>近两年</span>,
                    dataIndex: 'recentTwo' 
                },
                {
                    key: '8',
                    title: <span className='performance_tit_color'>近三年</span>,
                    dataIndex: 'recentThree' 
                }
            ],

            historyList: [
                {
                    key: '1',
                    title: <span className='performance_tit_color'>统计项</span>,
                    dataIndex: 'items' 
                },
                {
                    key: '2',
                    title: <span className='performance_tit_color'>收益率</span>,
                    dataIndex: 'rate' 
                },
                {
                    key: '3',
                    title: <span className='performance_tit_color'>收益率排名</span>,
                    dataIndex: 'ranking' 
                },
                {
                    key: '4',
                    title: <span className='performance_tit_color'>最大回撤</span>,
                    dataIndex: 'maxBack' 
                },
                {
                    key: '5',
                    title: <span className='performance_tit_color'>最大回撤排名</span>,
                    dataIndex: 'backRanking' 
                },
                {
                    key: '6',
                    title: <span className='performance_tit_color'>夏普比率</span>,
                    dataIndex: 'sharpe' 
                },
                {
                    key: '7',
                    title: <span className='performance_tit_color'>夏普比率排名</span>,
                    dataIndex: 'sharpeRanking' 
                }
            ],

            rankSource: [
                {
                    key: 1,
                    items: '2019',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                },
                {
                    key: 2,
                    items: '2018',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                },
                {
                    key: 3,
                    items: '2017',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                },
                {
                    key: 4,
                    items: '2016',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                },
                {
                    key: 5,
                    items: '2015',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                },
                {
                    key: 6,
                    items: '2014',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                },
                {
                    key: 7,
                    items: '2013',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                },
                {
                    key: 8,
                    items: '2013',
                    rate: '5.44%',
                    ranking: '41.60%',
                    maxBack: '124.28%',
                    backRanking: '200.89%',
                    sharpe: '366.88%',
                    sharpeRanking: '341.37%'
                }
            ],
        }
    }
    

    componentDidMount () {
        const options = {
            title: {
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                showSymbol: false,
                data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎', '我的关注', '产品一', '产品二'],
                bottom: 0
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '30',
                top:10,
                containLabel: true
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                axisTick: {
                    show: false
                },
                axisLabel: {
                    color: '#666'
                },
                axisLine: {
                    lineStyle: {
                        color: {
                            type: 'linear',
                            x: 0,
                            y: 0,
                            x2: 0,
                            y2: 1,
                            colorStops: [{
                                offset: 0, color: '#ccc' // 0% 处的颜色
                            }, {
                                offset: 1, color: '#ccc' // 100% 处的颜色
                            }],
                            global: false // 缺省为 false
                        }
                    }
                },
                data: ['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis: {
                axisLine: {
                    show: false,
                },
                boundaryGap: false,
                type: 'value',
                axisTick: {
                    show: false
                },
                axisLabel: {
                    color: '#666'
                },
                splitLine: {
                    lineStyle: {
                        type: 'dotted'
                    }
                }
            },
            
            series: [
                {
                    name:'邮件营销',
                    type:'line',
                    stack: '总量',
                    sampling: 'average',
                    showSymbol: false,
                    data:[10, 500, 401, 134, 200, 230, 210],
                },
                {
                    name:'联盟广告',
                    type:'line',
                    stack: '总量',
                    showSymbol: false,
                    sampling: 'average',
                    data:[220, 182, 191, 234, 290, 730, 310]
                },
                {
                    name:'视频广告',
                    type:'line',
                    stack: '总量',
                    showSymbol: false,
                    sampling: 'average',
                    data:[150, 232, 201, 154, 190, 330, 410]
                },
                {
                    name:'直接访问',
                    type:'line',
                    stack: '总量',
                    showSymbol: false,
                    sampling: 'average',
                    data:[320, 332, 301, 334, 390, 330, 320]
                },
                {
                    name:'搜索引擎',
                    type:'line',
                    stack: '总量',
                    showSymbol: false,
                    sampling: 'average',
                    data:[820, 932, 901, 934, 1290, 1330, 1320]
                },
                {
                    name:'我的关注',
                    type:'line',
                    stack: '总量',
                    showSymbol: false,
                    sampling: 'average',
                    data:[820, 932, 901, 934, 1290, 1330, 1320]
                },
                {
                    name:'产品一',
                    type:'line',
                    stack: '总量',
                    showSymbol: false,
                    sampling: 'average',
                    data:[820, 932, 901, 934, 1290, 1330, 1320]
                },
                {
                    name:'产品二',
                    type:'line',
                    stack: '总量',
                    showSymbol: false,
                    sampling: 'average',
                    data:[0, 932, 901, 934, 1290, 1330, 1320]
                }
            ]
        }
        
        const echartsDom = document.getElementById('echart_top_1')
        const myEcharts = echarts.init(echartsDom)
        myEcharts.setOption(options)

        let base = +new Date(1968, 9, 4)
        const oneDay = 24 * 60 * 60 * 1000
        let date = []

        let data = [Math.random() * 300]
        let newData = [Math.random()* 1000]
        let lastData = [Math.random()* 100]

        for (let i = 0; i < 20000; i++) {
            let now = new Date(base += oneDay)
            date.push([now.getFullYear(), now.getMonth() + 1, now.getDate()].join('/'))
            data.push(Math.round((Math.random() - 0.5) * 20 + data[i - 1]))
            newData.push(Math.round(Math.random(10) -2)*100 + newData[i-1])
            lastData.push(Math.round(Math.random()*10)*1000 + lastData[i-1])
        }


        let opt = {tooltip: {
            trigger: 'axis',
                position: function (pt) {
                    return [pt[0], '10%'];
                }
            },
            title: {
                left: 'left',
                // text: '大数据量面积图',
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '30',
                top:10,
                containLabel: true
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: date,
                axisTick: {
                    show: false
                },
                axisLabel: {
                    rotate: 50
                },
                axisLine: {
                    lineStyle: {
                        color: {
                            type: 'linear',
                            x: 0,
                            y: 0,
                            x2: 0,
                            y2: 1,
                            colorStops: [{
                                offset: 0, color: '#ccc'
                            }, {
                                offset: 1, color: '#ccc'
                            }],
                            global: false // 缺省为 false
                        }
                    }
                }
            },
            yAxis: {
                type: 'value',
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false,
                },
                splitLine: {
                    lineStyle: {
                        type: 'dotted'
                    }
                },
            },
            dataZoom: [{
                type: 'inside',
                start: 0,
                end: 10
            }],
            series: [
                {
                    name:'模拟数据1',
                    type:'line',
                    smooth:true,
                    symbol: 'none',
                    sampling: 'average',
                    itemStyle: {
                        color: 'rgb(255, 70, 131)'
                    },
                    areaStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: '#ff6666'
                        }, {
                            offset: 1,
                            color: '#ff6666'
                        }])
                    },
                    data: data
                },
                
            ]
        }

        const echartsDom2 = document.getElementById('echart_top_2')
        echarts.init(echartsDom2).setOption(opt)

        const dataAxis = ['(-1.00,-0.80)', '(-0.80,-0.60)', '(-0.60,-0.40)', '(-0.40,-0.20)', '(-0.20,0.00)', '(0.00,0.20)', '(0.40,0.60)']

        const barData = []

        for (let i = 0; i < dataAxis.length; i++) {
            barData.push((i+2)* 3)
        }
        

        let barOpt = {
            title: {},
            xAxis: {
                data: dataAxis,
                axisLabel: {
                    inside: false,
                    textStyle: {
                        color: '#555'
                    }
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    lineStyle: {
                        color: {
                            type: 'linear',
                            x: 0,
                            y: 0,
                            x2: 0,
                            y2: 1,
                            colorStops: [{
                                offset: 0, color: '#ccc'
                            }, {
                                offset: 1, color: '#ccc'
                            }],
                            global: false // 缺省为 false
                        }
                    }
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '30',
                top:10,
                containLabel: true
            },
            yAxis: {
                name: '频数',
                axisLabel: {
                    show: true,
                    typeStyle: {
                        color: '#999'
                    }
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                },
                splitNumber: 5,
                splitLine: {
                    show: true,
                    lineStyle: { type: 'dotted', color: '#999' }
                }
            },

            dataZoom: [
                {
                    type: 'inside'
                }
            ],

            series: [
                {
                    type: 'bar',
                    itemStyle: {
                        normal: { color: 'rgba(0, 0, 0, 0.1)'}
                    },
                    barGap: '-100%',
                    barCategroryGap: '40%',
                    animal: false
                },
                {
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1,[
                                {offset: 1, color: '#1b8ffe'},
                                {offset: 0.6, color: '#1b8ffe'},
                                {offset: 0, color: '#f5666c'}
                            ])
                        }
                    },
                    data: barData
                }
            ]
        }

        const barDom = document.getElementById('barCharts')
        echarts.init(barDom).setOption(barOpt)

    }

    checkChange = e => {
        console.log(e)
    }

    performanceTitle = (title) => {
        return (
            <div className='performance_icon_container'>
                <div className='performance_icon_top clear-fix'>
                    <div className='performance_icon_left'>
                        <Icon type="file-done" />
                        <span>{title}</span>
                    </div>

                    <div className='performance_icon_right'>
                        <Button type='primary'>添加投顾<Icon type='plus' /></Button>
                    </div>
                </div>


                <div className='performance_icon_bottom'>
                    <Checkbox onChange={this.checkChange} className='check_status'>起点对齐</Checkbox>
                    <Select defaultValue='disabled' className='performance_select' style={{width: '110px'}}>
                        <Option value="jack">基准产产品</Option>
                        <Option value="lucy">基准产产品2</Option>
                        <Option value="disabled" disabled>
                            基准产产品3
                        </Option>
                        <Option value="Yiminghe">基准产产品4</Option>
                    </Select>

                    <Select defaultValue='disabled' className='performance_select' style={{width: '90px'}}>
                        <Option value="jack">近一年</Option>
                        <Option value="lucy">近二年</Option>
                        <Option value="disabled" disabled>
                            近三年
                        </Option>
                        <Option value="Yiminghe">近四年</Option>
                    </Select>

                    <DatePicker locale={locale} placeholder="开始日期" style={{width: '100px', borderRadius: '4px !important'}} />
                    <span style={{color: '#999', padding: '0 6px 0 6px'}}>~</span>
                    <DatePicker locale={locale} placeholder="结束日期" style={{width: '100px', borderRadius: '4px !important', marginRight: '10px'}} />

                    <Select defaultValue='disabled' className='performance_select' style={{width: '110px'}}>
                        <Option value="jack">选择比较基准</Option>
                        <Option value="lucy">选择比较基准1</Option>
                        <Option value="disabled" disabled>
                            选择比较基准2
                        </Option>
                        <Option value="Yiminghe">选择比较基准3</Option>
                    </Select>
                </div>
            </div>
        )
    }


    middlePerformanceTitle = () => {
        return (
            <div className='performance_icon_container'>
                <div className='performance_icon_top clear-fix'>
                    <div className='performance_icon_left'>
                        <Icon type="file-done" />
                        <span>净值走势图</span>
                    </div>

                    <div className='performance_icon_right'>
                        <Button type='primary'>添加投顾<Icon type='plus' /></Button>
                    </div>
                </div>


                <div className='performance_icon_bottom'>
                    <Checkbox onChange={this.checkChange} className='check_status'>起点对齐</Checkbox>
                    <Select defaultValue='disabled' className='performance_select' style={{width: '110px'}}>
                        <Option value="jack">基准产产品</Option>
                        <Option value="lucy">基准产产品2</Option>
                        <Option value="disabled" disabled>
                            基准产产品3
                        </Option>
                        <Option value="Yiminghe">基准产产品4</Option>
                    </Select>

                    <Select defaultValue='disabled' className='performance_select' style={{width: '90px'}}>
                        <Option value="jack">近一年</Option>
                        <Option value="lucy">近二年</Option>
                        <Option value="disabled" disabled>
                            近三年
                        </Option>
                        <Option value="Yiminghe">近四年</Option>
                    </Select>

                    <DatePicker locale={locale} placeholder="开始日期" style={{width: '100px', borderRadius: '4px !important'}} />
                    <span style={{color: '#999', padding: '0 6px 0 6px'}}>~</span>
                    <DatePicker locale={locale} placeholder="结束日期" style={{width: '100px', borderRadius: '4px !important', marginRight: '10px'}} />

                    <Select defaultValue='disabled' className='performance_select' style={{width: '110px'}}>
                        <Option value="jack">选择比较基准</Option>
                        <Option value="lucy">选择比较基准1</Option>
                        <Option value="disabled" disabled>
                            选择比较基准2
                        </Option>
                        <Option value="Yiminghe">选择比较基准3</Option>
                    </Select>
                </div>
            </div>
        )
    }

    historyPerformanceTitle = (title) => {
        return (
            <div className='performance_icon_container'>
                <div className='performance_icon_top clear-fix'>
                    <div className='performance_icon_left'>
                        <Icon type="file-done" />
                        <span>{title}</span>
                    </div>
                </div>
            </div>
        )
    }

    render () {
        return (
            <div className='product_performance_top clear-fix'>
                
                <Card
                bordered={false}
                hoverable={true}
                className='product_performance_cards'
                style={{width: 'calc(50% - 10px)', float: 'left', marginRight: '10px', boxSizing: 'border-box'}}
                title={this.performanceTitle('净值走势图')}
                >
                   <div id="echart_top_1" style={{height: '450px'}}></div> 
                </Card>

                <Card
                bordered={false}
                hoverable={true}
                className='product_performance_cards'
                style={{width: 'calc(50% - 10px)', float: 'left', marginLeft: '10px', boxSizing: 'border-box'}}
                title={this.performanceTitle('回撤走势图')}
                >
                    <div id="echart_top_2" style={{height: '450px'}}></div> 
                </Card>


                <Card
                bordered={false}
                hoverable={true}
                className='product_performance_cards'
                style={{width: 'calc(50% - 10px)', float: 'left', height: '641px', marginRight: '10px', boxSizing: 'border-box'}}
                title={this.performanceTitle('收益-风险指标')}
                >
                    <Table style={{height: '380px'}} pagination={false} dataSource={this.state.historyDataSource} columns={this.state.historyColumns} />
                </Card>

                <Card
                bordered={false}
                hoverable={true}
                className='product_performance_cards'
                style={{width: 'calc(50% - 10px)', float: 'left', marginLeft: '10px', boxSizing: 'border-box'}}
                title={this.performanceTitle('历史周度收益率')}
                >
                    <div id='barCharts' style={{height: '492px'}}></div>
                </Card>


                <Card
                bordered={false}
                hoverable={true}
                className='product_performance_cards'
                style={{width: '100%', boxSizing: 'border-box'}}
                title={this.historyPerformanceTitle('历史月度收益率')}
                >
                    <Table dataSource={this.state.dataSource} pagination={false} columns={this.state.columns} />
                </Card>


                <Card
                bordered={false}
                hoverable={true}
                className='product_performance_cards'
                style={{width: 'calc(50% - 10px)', float: 'left', height: '641px', marginRight: '10px', boxSizing: 'border-box'}}
                title={this.performanceTitle('历史投资能力排名')}
                >
                    <Table dataSource={this.state.historyDataSource} pagination={false} columns={this.state.historyColumns} />
                </Card>

                <Card
                bordered={false}
                hoverable={true}
                className='product_performance_cards'
                style={{width: 'calc(50% - 10px)', float: 'left', height: '641px', marginLeft: '10px', boxSizing: 'border-box'}}
                title={this.performanceTitle('历年排名')}
                >
                    <Table dataSource={this.state.rankSource} pagination={false} columns={this.state.historyList} />
                </Card>

            </div>
        )
    }
}