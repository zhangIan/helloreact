import React, { Component } from 'react'
import { Modal, Tag, Button, Input, Icon, DatePicker, Select } from 'antd'
import '../../../style/common.sass'
import AddModalStyle from '../../../style/common.module.sass'
import CommonTable from '../common_table/index'
import Swiper from 'swiper'

const { Option } = Select


export default class addModal extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            visible: false,
            date_type: 'year',
            tags: ['紫霞一号', '紫霞二号', '紫霞三号', '紫霞四号']
        }
    }
    _showModal () {
        this.setState({visible: true})
        ;(() => {
            new Swiper('.swiper-container', {
                observer:true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    renderBullet: (index, className) => (`<span class=${className}>${index + 1}</span>`)
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                }
            })
        })() 
    }
    _hideModal () {
        this.setState({visible: false})
    }
    componentDidUpdate () {
        const ele = document.querySelector('.swiper-container')
        if (ele) {
            ;(() => {
                new Swiper('.swiper-container', {
                    observer:true,
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
                        renderBullet: (index, className) => (`<span class=${className}>${index + 1}</span>`)
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev'
                    }
                })
            })() 
        }
    }
    async componentDidMount () {
    }

    render () {
        return (
        <Modal
        title=''
        closable={false}
        visible={this.state.visible}
        destroyOnClose={true}
        maskClosable={true}
        centered={false}
        style = {{top: '5%'}}
        className={`add_product_modal_content`}
        wrapClassName={`add_product_modal_container`}
        onCancel={this._hideModal.bind(this)}
        footer={null}>
            <div className={`add_modal_title`}><h1>添加对比产品</h1></div>

            <div className={`${AddModalStyle.compare_product_content} clear-fix`}>
                <div className={`float_left`}>
                    <div className={`${AddModalStyle.left_title} float_left`}><span>对比产品：</span></div>
                    <div className={`float_left`}>
                        {this.state.tags.map((item, index) => <Tag className={`${AddModalStyle.compare_tags}`} key={index} closable={true}>{item}</Tag>)}
                    </div>
                </div>

                <div className={`float_right ${AddModalStyle.product_compare_btns_container}`}>
                    <Button type={'primary'} className={`${AddModalStyle.product_compare_btns} mr_10`}><span className={`iconfont ${AddModalStyle.compare_icons_compares}`}>&#xe61a;</span>对比</Button>
                    <Button type={'primary'} className={`${AddModalStyle.product_compare_btns}`}><span className={`iconfont ${AddModalStyle.compare_icons_delete}`}>&#xe656;</span>清空</Button>
                </div>
            </div>

            <div className={`mt_10`}>
                <div className={`${AddModalStyle.compare_header_content}`}>
                    <ul className={`${AddModalStyle.tab_fixed_content} clear-fix`}>
                        <li className={`position_relative ${AddModalStyle.product_name_input}`}>
                            <Input className='searchProduct' data='fund_name' placeholder={'产品名称'} />
                            {/* { this.renderDropList() } */}
                        </li>
                        <li><Button type="primary" onClick={() => {this.searchProduct()}}>搜索</Button></li>
                        <li><Button type="primary" onClick={ () => { this.resetOptions() }}>重置</Button></li>
                        <li><Button type="primary" onClick={this.toggle}>{this.state.expend ? '收起筛选项' : '展开筛选项'} <Icon type={this.state.expend ? 'up' : 'down'} /></Button></li>
                    </ul>
                </div>
            </div>

            <div className={`${AddModalStyle.modal_swiper_container}`}>
                <div className={`swiper-container ${AddModalStyle.slider_container}`}>
                    <div className={`swiper-wrapper ${AddModalStyle.slider_content}`}>

                        

                        <div className={`swiper-slide`}>

                            <div className={`${AddModalStyle.swiper_select_row} clear-fix`}>
                                <div className={`float_left ${AddModalStyle.select_content_tit}`}>
                                    <strong>大类策略：</strong>
                                    <span className={`${AddModalStyle.select_more}`}>不限</span>
                                </div>

                                <div className={`float_left ${AddModalStyle.select_content_list}`}>
                                    <dl>
                                        <dd className={`customer_selections`}>宏观</dd>
                                        <dd className={`customer_selections`}>股票</dd>
                                        <dd className={`customer_selections`}>市场中性</dd>
                                        <dd className={`customer_selections`}>债券</dd>
                                        <dd className={`customer_selections`}>期货</dd>
                                        <dd className={`customer_selections`}>期权</dd>
                                        <dd className={`customer_selections`}>其他</dd>
                                    </dl>
                                </div>
                            </div>


                            <div className={`${AddModalStyle.swiper_select_row} clear-fix`}>
                                <div className={`float_left ${AddModalStyle.select_content_tit}`}>
                                    <strong>细分策略：</strong>
                                    <span className={`${AddModalStyle.select_more}`}>不限</span>
                                </div>

                                <div className={`float_left ${AddModalStyle.select_content_list}`}>
                                    <div className={`${AddModalStyle.dobule_list} clear-fix`}>
                                        <dl className={`clear-fix pb_10`}>
                                            <dt><span>股票</span></dt>
                                            <dd className={`customer_selections`}>股票多头</dd>
                                            <dd className={`customer_selections`}>量化选股</dd>
                                            <dd className={`customer_selections`}>指数增强</dd>
                                            <dd className={`customer_selections`}>ETF</dd>
                                            <dd className={`customer_selections`}>股票多空</dd>
                                        </dl>

                                        <dl className={`clear-fix pb_10`}>
                                            <dt><span>期货</span></dt>
                                            <dd className={`customer_selections`}>高频</dd>
                                            <dd className={`customer_selections`}>中高频</dd>
                                            <dd className={`customer_selections`}>低频</dd>
                                        </dl>
                                    </div>

                                    <div className={`${AddModalStyle.dobule_list} clear-fix`}>
                                        <dl className={`clear-fix pb_10`}>
                                            <dt><span>市场中性</span></dt>
                                            <dd className={`customer_selections`}>TO</dd>
                                            <dd className={`customer_selections`}>高频</dd>
                                            <dd className={`customer_selections`}>低频</dd>
                                        </dl>

                                        <dl className={`clear-fix pb_10`}>
                                            <dt><span>期权</span></dt>
                                            <dd className={`customer_selections`}>波动曲面套利</dd>
                                            <dd className={`customer_selections`}>做市套利</dd>
                                            <dd className={`customer_selections`}>低频</dd>
                                        </dl>
                                    </div>

                                    <dl className={`clear-fix pb_10`}>
                                        <dt><span>债券</span></dt>
                                        <dd className={`customer_selections`}>利率和高等级信用债</dd>
                                        <dd className={`customer_selections`}>低等级信用债券</dd>
                                        <dd className={`customer_selections`}>可转债</dd>
                                    </dl>

                                    <dl className={`clear-fix`}>
                                        <dt><span>其他</span></dt>
                                        <dd className={`customer_selections`}>相对价值</dd>
                                        <dd className={`customer_selections`}>事件驱动</dd>
                                        <dd className={`customer_selections`}>组合策略</dd>
                                        <dd className={`customer_selections`}>多策略</dd>
                                        <dd className={`customer_selections`}>其他二级策略</dd>
                                        <dd className={`customer_selections`}>新三板</dd>
                                        <dd className={`customer_selections`}>海外基金</dd>
                                        <dd className={`customer_selections`}>货币基金</dd>
                                    </dl>

                                </div>


                            </div>


                            <div className={`${AddModalStyle.swiper_select_row} clear-fix`}>

                                <div className={`float_left ${AddModalStyle.select_content_list} ${AddModalStyle.none_left_content}`}>
                                    <div className={`${AddModalStyle.dobule_list} clear-fix`}>
                                        <dl className={`clear-fix pb_10`}>
                                            <dt><span>相对价值</span></dt>
                                            <dd className={`customer_selections`}>ETF套利</dd>
                                            <dd className={`customer_selections`}>可转债套利</dd>
                                            <dd className={`customer_selections`}>固定收益</dd>
                                            <dd className={`customer_selections`}>分级基金套利</dd>
                                        </dl>

                                        <dl className={`clear-fix pb_10`}>
                                            <dt><span>组合策略</span></dt>
                                            <dd className={`customer_selections`}>MOM</dd>
                                            <dd className={`customer_selections`}>FOF</dd>
                                            <dd className={`customer_selections`}>TOT</dd>
                                        </dl>
                                    </div>

                                    <dl className={`clear-fix`}>
                                        <dt><span>事件驱动</span></dt>
                                        <dd className={`customer_selections`}>并购重组</dd>
                                        <dd className={`customer_selections`}>定向增发</dd>
                                        <dd className={`customer_selections`}>大宗交易</dd>
                                    </dl>
                                </div>


                            </div>

                            
                        </div>

                        <div className={`swiper-slide`}>
                                <div className={`${AddModalStyle.swiper_select_row} clear-fix`}>
                                    <div className={`float_left ${AddModalStyle.select_content_tit}`}>
                                        <strong>产品星级：</strong>
                                    </div>

                                    <div className={`float_left ${AddModalStyle.select_content_list}`}>
                                        <div className={`${AddModalStyle.dobule_list} clear-fix`}>
                                            <dl className={`clear-fix`}>
                                                <dd className={`customer_selections`}>一星</dd>
                                                <dd className={`customer_selections`}>二星</dd>
                                                <dd className={`customer_selections`}>三星</dd>
                                                <dd className={`customer_selections`}>四星</dd>
                                                <dd className={`customer_selections`}>五星</dd>
                                            </dl>

                                            <dl className={`clear-fix`}>
                                                <div className={`float_left ${AddModalStyle.select_content_tit}`}>
                                                    <strong>投顾星级：</strong>
                                                </div>
                                                <dd className={`customer_selections`}>一星</dd>
                                                <dd className={`customer_selections`}>二星</dd>
                                                <dd className={`customer_selections`}>三星</dd>
                                                <dd className={`customer_selections`}>四星</dd>
                                                <dd className={`customer_selections`}>五星</dd>
                                            </dl>
                                        </div>

                                    </div>
                                    
                                </div>

                                <div className={`${AddModalStyle.swiper_select_row} clear-fix`}>
                                    <div className={`float_left ${AddModalStyle.select_content_tit}`}>
                                        <strong>投顾成立时间：</strong>
                                    </div>

                                    <div className={`float_left ${AddModalStyle.select_content_list}`}>
                                        <div className={`${AddModalStyle.dobule_list} clear-fix`}>
                                            <DatePicker className={`${AddModalStyle.select_date_picker}`} placeholder={`起始日期`} />
                                            <span className={`${AddModalStyle.select_date_split}`}>—</span>
                                            <DatePicker className={`${AddModalStyle.select_date_picker}`} placeholder={`截止日期`}  />
                                        </div>

                                    </div>
                                    
                                </div>

                        </div>
                        
                        
                        <div className={`swiper-slide`}>

                                <div className={`${AddModalStyle.swiper_select_row} clear-fix`}>
                                    <div className={`float_left ${AddModalStyle.select_content_tit} ${AddModalStyle.last_modal_tit}`}>
                                        <strong>收益风险区间：</strong>
                                    </div>

                                    <div className={`float_left ${AddModalStyle.select_content_list}`}>
                                        <div className={`${AddModalStyle.dobule_list} clear-fix`}>
                                            <div className={`float_left ${AddModalStyle.left_modal}`}>
                                                <span className={`${AddModalStyle.last_content_tit}`}>收益风险区间：</span>
                                                <Select value={this.state.date_type} className={`common_search_selection ${AddModalStyle.modal_select_options}`}>
                                                    <Option value="year">今年以来</Option>
                                                    <Option value="m1">近一个月</Option>
                                                    <Option value="m3">近三月</Option>
                                                    <Option value="m6">近六月</Option>
                                                    <Option value="y1">近一年</Option>
                                                    <Option value="y2">近二年</Option>
                                                    <Option value="y3">近三年</Option>
                                                    <Option value="y5">近五年</Option>
                                                    <Option value="total">成立以来</Option>
                                                </Select>
                                            </div>

                                            <div className={`clear-fix`}>
                                                <div className={`float_left ${AddModalStyle.select_content_tit}`}>
                                                    <span>回撤：</span>
                                                    <Select value={'section'} className={`common_search_selection ${AddModalStyle.modal_select_options}`}>
                                                        <Option value="section">绝对回撤区间</Option>
                                                        <Option value="ranking">回撤市场排名</Option>
                                                    </Select>
                                                    <Input className={`${AddModalStyle.modal_input_number}`} />
                                                    <span>%</span>
                                                    <strong className={`${AddModalStyle.modal_input_split}`}>~</strong>
                                                    <Input className={`${AddModalStyle.modal_input_number}`} />
                                                    <span>%</span>
                                                </div>
                                                
                                            </div>
                                        </div>

                                    </div>
                                    
                                </div>

                                <div className={`${AddModalStyle.swiper_select_row} clear-fix`}>
                                    

                                    <div className={`float_left ${AddModalStyle.select_content_list} ${AddModalStyle.last_none_title}`}>
                                        <div className={`${AddModalStyle.dobule_list} clear-fix`}>
                                                <div className={`float_left ${AddModalStyle.select_content_tit}`}>
                                                    <span className={`${AddModalStyle.last_content_tit}`}>收益：</span>
                                                    <Select value={'section'} className={`common_search_selection ${AddModalStyle.modal_select_options}`}>
                                                        <Option value="section">绝对收益区间</Option>
                                                        <Option value="ranking">收益市场排名</Option>
                                                    </Select>
                                                    <Input className={`${AddModalStyle.modal_input_number}`} />
                                                    <span>%</span>
                                                    <strong className={`${AddModalStyle.modal_input_split}`}>~</strong>
                                                    <Input className={`${AddModalStyle.modal_input_number}`} />
                                                    <span>%</span>
                                                </div>

                                        </div>

                                    </div>
                                    
                                </div>

                        </div>
                    
                    
                    </div>

                    <div className={`swiper-button-next ${AddModalStyle.modal_swiper_button_next}`}>
                        <span className={`iconfont`}>&#xe60a;</span> 
                    </div>
                    <div className={`swiper-button-prev ${AddModalStyle.modal_swiper_button_prev}`}>
                        <span className={`iconfont`}>&#xe609;</span>
                    </div>
                    <div className={`swiper-pagination ${AddModalStyle.modal_pagination}`}></div>
                </div>
            </div>

            <CommonTable></CommonTable>
        </Modal>)
    }
}