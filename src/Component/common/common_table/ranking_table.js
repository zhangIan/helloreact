import React, { Component } from 'react'
import { Table } from 'antd'

const columns = [
    {
        key: '1',
        dataIndex: 'yearDate',
        title: '',
        width: '120px'
    },
    {
        key: '2',
        dataIndex: 'nReturn',
        title: '收益率',
        render: (t) => t ? parseFloat(t).toFixed(2) + '%' : '--'
    },
    {
        key: '3',
        dataIndex: 'return',
        title: '排名',
        render: (t) => t ? t : '--'
    },
    {
        key: '4',
        dataIndex: 'nMaxDrawdown',
        title: '最大回撤',
        render: (t) => t ? parseFloat(t).toFixed(2) + '%' : '--'
    },
    {
        key: '5',
        dataIndex: 'max_drawdown',
        title: '排名',
        render: (t) => t ? t : '--'
    },
    {
        key: '6',
        dataIndex: 'nSharpeA',
        title: '夏普比率',
        render: (t) => t ? parseFloat(t).toFixed(2) : '--'
    },
    {
        key: '7',
        dataIndex: 'sharpe_a',
        title: '排名',
        render: (t) => t ? t : '--'
    }
]

export default class RankingTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            dataList: []
        }
    }

    async _listData () {
        console.log()
        if (this.props.dataList) {
            const data = await this.props.dataList
            if (data) {
                data.forEach((item, index) => {item.key = index})
                this.setState({dataList: data})
            }
        }
    }

    async UNSAFE_componentWillMount () {
        await this._listData()
    }

    render () {
        const dataSource = this.state.dataList
        return <Table size='middle' columns={columns} dataSource={dataSource} pagination={false} />
    }
}