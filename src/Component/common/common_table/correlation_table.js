import React, { Component } from 'react'
import { Table } from 'antd'

const columns = [
    {
        title: '序号',
        dataIndex: 'serial'
    },
    {
        title: '名称',
        dataIndex: 'name'
    },
    {
        title: '1',
        dataIndex: 'number1'
    },
    {
        title: '2',
        dataIndex: 'number2'
    },
    {
        title: '3',
        dataIndex: 'number3'
    },
    {
        title: '4',
        dataIndex: 'number4'
    },
    {
        title: '5',
        dataIndex: 'number5'
    },
    {
        title: '6',
        dataIndex: 'number6'
    }
]
export default class CorrelationTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    render () {
        return <Table columns={columns} dataSource={[]} />
    }
}