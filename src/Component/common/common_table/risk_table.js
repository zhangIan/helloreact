import React, { Component } from 'react'
import { Table, Tag, Input, Select } from 'antd'

const Option = Select.Option

const columnsText = {
    n_return: {name: '累计收益率'},
    n_return_a: {name: '年化收益率', tag:''},
    n_stdev_a: {name: '波动率', tag:'年化'},
    n_dd_a: {name: '下行标准差'},
    n_sharpe_a: {name: '夏普比率', tag:'年化', percent: '%'},
    // n_sor_a: {name: 'Sortino比率', tag:'年化'},
    // n_calmar_a: {name: 'Calma比率', tag:'年化'},
    // n_spearman: {name: 'Spearman秩相关', selection: true},
    n_info_a: {name: '信息比率', tag:'年化'},
    n_alpah: {name: 'Alpha', tag:'年化'},
    n_beta:  {name: 'Beta'},
    // c_cor_id: {name: '与基准指数相关系数'},
    n_max_drawdown: {name: '最大回撤'},
    // n_mdd_repair_time: {name: '最大回撤补期（天）'},
    n_VaR: {name: 'Var（95% 置信度）', tag:'年化'},
    n_odds: {name: '投资胜率'},
    n_pl_amt_ratio: {name: '平均损益比'},
    n_pl_num_ratio: {name: '盈亏次数'},
    // n_benchmark: {name: '业绩基准'},
    // n_correlation: {name: '相关性', tag:'年化'}
}

const advisorColumnsText = {
    n_return: {name: '累计收益率'},
    n_return_a: {name: '年化收益率', tag:'年化'},
    n_stdev_a: {name: '年化波动率', tag:'年化'},
    n_dd_a: {name: '下行标准差'},
    n_sharpe_a: {name: 'Sharpe比率', tag:'年化', percent: '%'},
    n_sor_a: {name: 'Sortino比率', tag:'年化'},
    n_calmar_a: {name: 'Calma比率', tag:'年化'},
    n_spearman_p: {name: 'Spearman秩相关', selection: true},
    n_info_a: {name: '信息比率', tag:'年化'},
    n_alpah: {name: 'Alpha', tag:'年化'},
    n_beta:  {name: 'Beta'},
    c_cor_id: {name: '与基准指数相关系数'},
    n_max_drawdown: {name: '最大回撤'},
    n_mdd_repair_time: {name: '最大回撤补期（天）'},
    n_VaR: {name: 'Var（95% 置信度）', tag:'年化'},
    n_odds: {name: '投资胜率'},
    n_pl_amt_ratio: {name: '平均损益比'},
    n_pl_num_ratio: {name: '盈亏次数比'},
    n_benchmark: {name: '业绩基准'},
    n_correlation: {name: '相关性', tag:'年化'}
}


const columns = [
    {
        key: '1',
        dataIndex: 'risk',
        title: '收益/风险指标',
        width: 240,
        render: (text, recoder, index) => {

            let elementTag = <span></span>
            if (recoder.risk !== 'c_fund_id') {
                if (!columnsText[recoder.risk].hasOwnProperty('tag')) {
                    if (columnsText[recoder.risk].hasOwnProperty('percent')) {
                        elementTag = <span>{columnsText[recoder.risk].name}<Input></Input></span>
                    } else if (columnsText[recoder.risk].hasOwnProperty('selection')) {
                        elementTag = <span>{columnsText[recoder.risk].name}
                        <Select className={`table_select_tips`} placeholder={'请选择基准'} value={'请选择基准'}>
                            <Option value='1111'>11111</Option>
                        </Select>
                        </span>
                    } else {
                        elementTag = <span>{columnsText[recoder.risk].name}</span>
                    }
                } else {
                    if (columnsText[recoder.risk].hasOwnProperty('percent')) {
                        elementTag =
                        <span>
                            {columnsText[recoder.risk].name}
                            <Input className={`table_columns_rows_input`} defaultValue={'2.5'} />
                            <em className={`table_columns_percent`}>%</em>
                            <Tag className={`table_tags_rates`}>{columnsText[recoder.risk].tag}</Tag>
                        </span>
                    } else {
                        elementTag = <span>{columnsText[recoder.risk].name}<Tag className={`table_tags_rates`}>{columnsText[recoder.risk].tag}</Tag></span>
                    }
                }

            }

            return elementTag
        }
    },
    {
        key: '2',
        dataIndex: 'avg',
        title: ''
    }
]

const advisorColumns = [
    {
        key: '1',
        dataIndex: 'risk',
        title: '收益/风险指标',
        width: 240,
        render: (text, recoder, index) => {

            let elementTag = <span></span>
            // console.log(recoder)
            if (advisorColumnsText.hasOwnProperty(recoder.risk)) {
                elementTag = <span>{advisorColumnsText[recoder.risk].name}</span>
            }

            return elementTag
        }
    },
    {
        key: '2',
        dataIndex: 'avg',
        title: ''
    }
]

export default class RiskTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    async _initData () {
        let riskType = ''
        if (this.props.hasOwnProperty('_riskType') && this.props._riskType === 'advisor') {
            riskType = 'advisor'
        } else {
            riskType = 'product'
        }
        if (!this.props.hasOwnProperty('_initRateRisk')) {
            return
        }
        const data = await this.props._initRateRisk()
        let datas = []
        if (data) {
            if (riskType !== 'advisor') {
                Object.keys(data).forEach((item, index) => {
                    if (columnsText.hasOwnProperty(item)) {
                        datas.push({
                            risk: item,
                            avg: isNaN(data[item]) ? data[item] : parseFloat(data[item]).toFixed(2),
                            key: index
                        })

                    }
                })
            } else if (riskType === 'advisor') {
                Object.keys(data).forEach((item, index) => {
                    if (advisorColumnsText.hasOwnProperty(item)) {
                        datas.push({
                            risk: item,
                            avg: isNaN(data[item]) ? data[item] : parseFloat(data[item]).toFixed(2),
                            key: index
                        })

                    }

                })
            }

        }

        datas=datas.sort()
        if (datas.length > 0) {
            this.setState({dataList: datas})
        }
    }

    async UNSAFE_componentWillMount () {
        await this._initData()
    }

    render () {
        const dataSource = this.state.dataList || []
        let currentColumns = []

        if (this.props.hasOwnProperty('_riskType') && this.props['_riskType'] === 'advisor') {
            currentColumns = advisorColumns
        } else {
            currentColumns = columns
        }

        currentColumns[1].title = this.props.productName

        return <Table className={`table_border_right_list`} dataSource={dataSource} columns={currentColumns} pagination={false} />
    }
}
