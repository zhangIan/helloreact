import React, { Component } from 'react'
import { Table } from 'antd'

const columns = [
    {
        title: '',
        key: '0',
        dataIndex: 'tDate',
        render: (text, recoder, indx) => {
            if (text === 'avg') {
                text = '均值'
            }
            return text
        }
    },
    {
        title: '一月',
        key: '1',
        dataIndex: '01',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '二月',
        key: '2',
        dataIndex: '02',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '三月',
        key: '3',
        dataIndex: '03',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '四月',
        key: '4',
        dataIndex: '04',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '五月',
        key: '5',
        dataIndex: '05',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '六月',
        key: '6',
        dataIndex: '06',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '七月',
        key: '7',
        dataIndex: '07',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '八月',
        key: '8',
        dataIndex: '08',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '九月',
        key: '9',
        dataIndex: '09',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '十月',
        key: '10',
        dataIndex: '10',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '十一月',
        key: '11',
        dataIndex: '11',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '十二月',
        key: '12',
        dataIndex: '12',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    },
    {
        title: '全年',
        key: '13',
        dataIndex: 'nYearReturn',
        render: text => (text ? <span className={parseFloat(text) > 0 ? 'color_number_red' : 'color_number_green'}>{`${isNaN(parseFloat(text)) ? '--' : parseFloat(text).toFixed(2)+'%'}`}</span> : '--')
    }
]

export default class MonthTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    async _initData () {
        if (!this.props.hasOwnProperty('_historyRate')) {
            return
        }
        let dataList = await this.props._historyRate()
        // console.log(dataList)
        if (dataList) {
            for (let i = 0; i < dataList.length; i++) {
                dataList[i].key = i
            }
            this.setState({dataList})
        }
    }

    async componentDidMount () {
        await this._initData()
    }

    render () {
        return <Table dataSource={this.state.dataList || []} columns={columns} pagination={false} />
    }
}
