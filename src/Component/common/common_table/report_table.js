import React, { Component } from 'react'
import { Table } from 'antd'

const columns = [
    {
        dataIndex: 'code',
        title: '品种代码'
    },
    {
        dataIndex: 'short',
        title: '品种简称'
    },
    {
        dataIndex: 'value',
        title: '持仓市值'
    },
    {
        dataIndex: 'assets',
        title: '占基金资产净值比例'
    },
    {
        dataIndex: 'bonds',
        title: '债券类型'
    },
    {
        dataIndex: 'rating',
        title: '信用评级'
    }
]
export default class ReportTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    render () {
        const { dataSource, size } = this.props
        return <Table columns={columns} bordered size={size} dataSource={dataSource} />
    }
}