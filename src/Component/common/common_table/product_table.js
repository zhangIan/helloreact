import React, { Component } from 'react'
import { Table } from 'antd'

const columns = [
    {
        dataIndex: 'code',
        title: ''
    },
    {
        dataIndex: 'short',
        title: '赛亚成长1号'
    },
    {
        dataIndex: 'value',
        title: '塞药美纯'
    },
    {
        dataIndex: 'assets',
        title: '赛亚明星'
    },
    {
        dataIndex: 'bonds',
        title: '赛亚成长1号A'
    },
    {
        dataIndex: 'rating',
        title: '赛亚成长1号B'
    },
    {
        dataIndex: 'rating1',
        title: '赛亚凤凰基金'
    }
]
export default class SubTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    render () {
        const { dataSource, size } = this.props
        return <Table columns={columns} bordered size={size} dataSource={dataSource} />
    }
}