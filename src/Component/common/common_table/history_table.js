import React, { Component } from 'react'
import { Table } from 'antd'

const columns = [
    {
        title: '',
        key: '1',
        width: '120px',
        dataIndex: 'first'
    },
    {
        title: '今年以来',
        key: '2',
        dataIndex: 'year'
    },
    {
        title: '近一个月',
        key: '3',
        dataIndex: 'one'
    },
    {
        title: '近三个月',
        key: '4',
        dataIndex: 'three'
    },
    {
        title: '近六个月',
        key: '5',
        dataIndex: 'six'
    },
    {
        title: '近一年',
        key: '6',
        dataIndex: 'firstYear'
    },
    {
        title: '近二年',
        key: '7',
        dataIndex: 'secondYear'
    }
]

export default class HistoryTable extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    render () {
        return <Table size='middle' columns={columns} dataSource={[]} />
    }
}