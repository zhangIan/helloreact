import React, { Component } from 'react'
import { Table, Input } from 'antd'
import SceneStyle from './style/sceneTable.module.sass'

function renderFunction (t, r) {
    let text = ''
    switch (r['指标']) {
        case '周期数':
            return t
        case 'Calmar比率':
            return isNaN(t) ? t : parseFloat(t).toFixed(2)
        case 'Sortino比率':
            return isNaN(t) ? t : parseFloat(t).toFixed(2)
        case '夏普比率':
            return isNaN(t) ? t : parseFloat(t).toFixed(2)
        default: 
            if (typeof t === 'object' && Array.isArray(t)) {
                t.forEach((item, index) => {
                    if (index < t.length - 1) {
                        text += `${parseFloat(item).toFixed(2)}% ~`
                    } else {
                        text += `${parseFloat(item).toFixed(2)}%`
                    }
                })
            } else {
                text = `${parseFloat(t).toFixed(2)}%`
            }
        
            return text
    }
}

const socketColumns = [
    {key: '1', dataIndex: '指标', title: '指标', width: 260, render: (t, r) => {
        if (t === '夏普比率') {
        const renders =  <span>
                {t}
                <Input className={`table_columns_rows_input`} id={`socketId`} defaultValue={'2.5'} />
                <em className={`table_columns_percent`}>%</em>
            </span>
            return renders
        } else {
            return t
        }
    }},
    {key: '2', dataIndex: '市场上涨', title: '市场上涨', render:  (t, r) => renderFunction(t, r)},
    {key: '3', dataIndex: '市场震荡', title: '市场震荡', render: (t, r) => renderFunction(t, r)},
    {key: '4', dataIndex: '市场下跌', title: '市场下跌', render:  (t, r) => renderFunction(t, r)}
]

const columnsText = {
    conf_intveral: {name: '周收益率区间范围'},
    n_stdev_a: {name: '波动率'},
    return_ratio: {name: '场景收益占比'},
    calmar: {name: 'Calmar比率'},
    max_drawdown: {name: '最大回撤'},
    n_count: {name: '周期数'},
    sharpe: {name: '夏普比率'},
    sortino: {name: 'Sortino比率'},
}

export default class SceneTable extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: []
        }
    }

    _initData (data) {
        
        if (data) {
            let dataKey = Object.keys(data)
            let dataList = []
            console.log(data)
            dataKey.forEach((item, index) => {
                if (item !== 'n_count') {
                    dataList.push({
                        key: index,
                        '指标': columnsText[item].name,
                        '市场上涨': data[item]['市场上涨'],
                        '市场震荡': data[item]['震荡'],
                        '市场下跌': data[item]['市场下跌']
                    })
                } else {
                    dataList.unshift({
                        key: index,
                        '指标': columnsText[item].name,
                        '市场上涨': data[item]['市场上涨'],
                        '市场震荡': data[item]['震荡'],
                        '市场下跌': data[item]['市场下跌']
                    })
                }
            })

            if (dataList.length > 0) {
                this.setState({dataSource: dataList})
            }

        }
    }

    
    render () {
        return <Table className={`${SceneStyle.table_scene}`} bordered columns={socketColumns} dataSource={this.state.dataSource} pagination={false} />
    }
}