import React, { Component } from 'react'
import echarts from 'echarts/lib/echarts'
import { message, Spin, Empty } from 'antd'

export default class CommonBar extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            Loading: true,
            barDom: document.getElementById(this.props.idName),
            barChart: document.getElementById(this.props.idName),
            chartShow: true
        }
    }

    setOptions (options) {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        barChart.showLoading()

        if (options) {
            if (options.series.length > 0) {
                if (options.series[0].data.length > 0) {
                    this.setState({chartShow: true})
                    barChart.setOption(options)
                } else {
                    this.setState({chartShow: false})
                }
            } else {
                this.setState({chartShow: false})  
            }
            
            barChart.hideLoading()
            this.setState({Loading: false})
            window.onresize = barChart.resize
        } else {
            message.error('图表配置项错误')
        }
    }

    _getCharts () {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        return barChart
    }

    getContext () {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        return barChart.getConnectedDataURL()
    }

    render () {
        const { idName } = this.props
        return (<Spin spinning={this.state.Loading} delay={10} tip={'Loading'} style={{width: '100%'}}>
                    <div id={idName} style={{width: '100%', height: '600px', display: this.state.chartShow ? 'block' : 'none'}}></div>
                    <div className={`chart_none_data`} style={{width: '100%', height: '600px', display: !this.state.chartShow ? 'block' : 'none'}}>
                        <Empty style={{display: 'inline-table'}} image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    </div>
                </Spin>)
    }

}