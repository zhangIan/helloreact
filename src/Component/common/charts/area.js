import React, { Component } from 'react'
import 'zrender/lib/svg/svg'

import echarts from 'echarts'
import { message, Spin, Empty } from 'antd'

export default class AreaCharts extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            Loading: true,
            chartShow: true
        }
    }

    setOptions (options) {
        let areaCharts = document.getElementById(`${this.props.idName}`)
        let charts = echarts.init(areaCharts, null, {renderer: 'canvas'})
        
        if (options) {
            if (options.series.length > 0) {
                charts.setOption(options)
                this.setState({Loading: false, chartShow: true})
            } else {
                this.setState({Loading: false, chartShow: false})
            }
            
        } else {
            message.error('没有图标配置项')
        }
    }

    getContext () {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        return barChart.getConnectedDataURL()
    }

    render () {
        const id = this.props.idName
        return <Spin spinning={this.state.Loading} delay={10} tip={'Loading'}>
            <div id={id} style={{width: '100%', height: '600px', display: this.state.chartShow ? 'block' : 'none'}}></div>
            <div className={`chart_none_data`} style={{width: '100%', height: '600px', display: !this.state.chartShow ? 'block' : 'none'}}>
                <Empty style={{display: 'inline-table'}} image={Empty.PRESENTED_IMAGE_SIMPLE} />
            </div>
            </Spin>
    }
}