import React, { Component } from 'react'
import echarts from 'echarts'
import { Spin, Empty } from 'antd'

export default class BarChart extends Component {
    constructor(...props) {
        super(...props)
        this.state ={
            Loading: true,
            chartShow: true
        }
    }

    setBar = (options) => {
        let barCharts = document.getElementById('barCharts')
        let barChart = echarts.init(barCharts)
        barChart.showLoading()
        if (options) {
            barChart.hideLoading()
            if (options.series.length > 0) {
                barChart.setOption(options)
            } else {
                this.setState({chartShow: false})
            }
            
            this.setState({Loading: false})
        }
    }

    getContext () {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        return barChart.getConnectedDataURL()
    }

    render () {
        return <Spin spinning={this.state.Loading} delay={10} tip={'Loading'}>
            <div id="barCharts" style={{width: '100%', height: '500px', display: this.state.chartShow ? 'block' : 'none'}}></div>
            <div className={`chart_none_data`} style={{width: '100%', height: '500px', display: !this.state.chartShow ? 'block' : 'none'}}>
                <Empty style={{display: 'inline-table'}} image={Empty.PRESENTED_IMAGE_SIMPLE} />
            </div>
            </Spin>
    }
}