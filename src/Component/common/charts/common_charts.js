import React, { Component } from 'react'
import echarts from 'echarts/lib/echarts'
import { message, Spin, Empty } from 'antd'

export default class CommonCharts extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            Loading: true,
            chartShow: true
        }
    }

    async setOpitions (options, loading=true) {
        await this.setState({Loading: loading})
        const { idName } = this.props
        const echartsDom = document.getElementById(idName)
        let echart = echarts.init(echartsDom)
        setTimeout(() => {
            if (options) {
                if (options.series.length > 0) {
                    this.setState({chartShow: true})
                    echart.setOption(options)
                } else {
                    this.setState({chartShow: false})
                }
                this.setState({Loading: false})
            } else {
                this.setState({chartShow: false, Loading: false})
                message.destroy()
                message.error('图表配置项设置错误')
            }
        }, 500)
        
    }

    getContext () {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        return barChart.getConnectedDataURL()
    }

    render () {
        const { idName, styles } = this.props
        return (<Spin spinning={this.state.Loading} delay={10} tip={'Loading'}>
            <div id={idName} style={{display: this.state.chartShow ? 'block' : 'none', ...styles}}></div>
            <div className={`chart_none_data`} style={{ display: !this.state.chartShow ? 'block' : 'none', ...styles}}>
                <Empty style={{display: 'inline-table'}} image={Empty.PRESENTED_IMAGE_SIMPLE} />
            </div>
            </Spin>)
    }
}