import React, { Component } from 'react'
import echarts from 'echarts'
import { Spin } from 'antd'

export default class LineCharts extends Component {
    constructor (...props) {
        super(...props)

        this.state = {
            Loading: true
        }
    }

    setOptions = (options) => {
        const { idName } = this.props
        const lineCharts = document.getElementById(idName)
        let charts = echarts.init(lineCharts)

        if (options) {
            charts.setOption(options)
            this.setState({Loading: false})
        }
    }

    getContext () {
        const { idName } = this.props
        let barDom = document.getElementById(idName)
        let barChart = echarts.init(barDom)
        return barChart.getConnectedDataURL()
    }

    render () {
        const { idName } = this.props
        return <Spin spinning={this.state.Loading} delay={10} tip={'Loading'}><div id={idName} style={{width: '100%', height: '600px'}}></div></Spin>
    }
}