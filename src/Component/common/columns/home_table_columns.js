import React from 'react'
import { Checkbox, Select, Tooltip } from 'antd'
import moment from 'moment'
const { Option } = Select
const columns = [
    {
        title: () => {
            return (<span>关注</span>)
        },
        dataIndex: 'attentions',
        key: '1',
        dataId: '',
        width: 50,
        fixed: 'left',
        render: () => (<span className={`iconfont iconguanzhu heart_check`} onClick={this.headerCheck} />)
    },
    {
        title: '对比',
        dataIndex: 'compare',
        key: '2',
        dataId: '',
        width: 50,
        fixed: 'left',
        render: (t, r) => {
            return (<Checkbox id={`${r['fundId']}`} name={r['fundName']} onChange={this.changes}></Checkbox>)
        }
    },
    {
        title: '序号',
        dataIndex: 'key',
        width: 80,
        key: '3',
        dataId: '',
        render: (text) => (<span>{text!== '' ? text : '--'}</span>)
    },
    {
        title: '投顾层级',
        width: 120,
        dataId: '',
        dataIndex: 'consultantLevel',
        ellipsis: true,
        key: '4',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        },
        sorter: (a, b) => a === b
    },
    {
        title: '投资顾问',
        dataIndex: 'fundConsultant',
        key: '5',
        dataId: '',
        ellipsis: true,
        sorter: (a, b) => a === b,
        render: (text, recoder) => (<a  className='table_color' rel="noopener noreferrer" target="_blank" href={`/product?id=${recoder.advisor}`}>{text!== undefined ? text : '--'}</a>) 
    },
    {
        title: '产品名称',
        sorter: true,
        dataIndex: 'fundName',
        key: '6',
        width: 130,
        ellipsis: true,
        dataId: '',
        render: (text, recoder) => (<Tooltip placement="bottom" title={text}><a className='table_color' rel="noopener noreferrer" target="_blank" href={`/advisor?id=${recoder.name}`}>{text!== undefined ? text : '--'}</a></Tooltip>) 
    },
    {
        title: '投资经理',
        sorter: true,
        dataIndex: 'fundManager',
        key: '7',
        dataId: '',
        render: (text) => (<a className='table_color' rel="noopener noreferrer" target="_blank" href='/manager?id=1234'>{text!== undefined ? text : '--'}</a>) 
    },
    {
        title: '资产类别',
        sorter: true,
        key: '8',
        dataId: '',
        dataIndex: 'assetType',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: '投资策略',
        sorter: true,
        key: '9',
        dataId: '',
        dataIndex: 'strategyType',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: '投顾星级',
        sorter: true,
        key: '10',
        dataId: '',
        dataIndex: 'consultantStar',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: '产品星级',
        sorter: true,
        key: '11',
        dataId: '',
        dataIndex: 'fundStar',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: '投顾成立日期',
        sorter: true,
        key: '12',
        dataId: '',
        dataIndex: 'orgFoundationDate',
        render: text => (<span>{moment(new Date(text)).format('YYYY-MM-DD')}</span>)
    },
    {
        title: '产品成立日期',
        sorter: true,
        key: '13',
        dataId: '',
        dataIndex: 'fundFoundationDate',
        render: text => (<span>{moment(new Date(text)).format('YYYY-MM-DD')}</span>)
    },
    {
        title: '最新净值日期',
        sorter: true,
        key: '14',
        dataId: '',
        dataIndex: 'newNavDate',
        render: text => (<span>{moment(new Date(text)).format('YYYY-MM-DD')}</span>)
    },
    {
        title:  () => (
            <div className='home_table_rate'>
                <span>净值类型</span>
                <Select defaultValue='absoluteSharpe' onChange={this.sharpeChange}>
                <Option value="absoluteSharpe">单位净值</Option>
                <Option value="marketSharpe">累计净值</Option>
                <Option value="reset">复权累计净值</Option>
                </Select>
        </div>),
        sorter: true,
        key: '15',
        dataId: '',
        dataIndex: 'fundNav',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: () => (
        <div className='home_table_rate'>
            <span>收益率</span>
            <Select defaultValue='absoluteSharpe' onChange={this.sharpeChange}>
            <Option value="absoluteSharpe">今年以来</Option>
            <Option value="month">近一月</Option>
            <Option value="three">近三月</Option>
            <Option value="six">近六月</Option>
            <Option value="year">近一年</Option>
            <Option value="towYear">近二年</Option>
            <Option value="threeYear">近三年</Option>
            <Option value="began">成立以来年化</Option>
            </Select>
        </div>
    ),
        sorter: true,
        key: '16',
        dataId: '',
        width: 130,
        dataIndex: 'nReturn',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: () => {
            return (<div><Select defaultValue='selectMarket' onChange={this.sharpeChange}>
            <Option value="selectL">同策略</Option>
            <Option value="selectMarket">全市场</Option>
            </Select></div>)
        },
        sorter: true,
        key: '17',
        dataId: '',
        dataIndex: 'cRank',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    }, 
    {
        title: '最大回撤',
        sorter: true,
        key: '18',
        dataId: '',
        dataIndex: 'maxDrawdown',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: 'Sharpe比率',
        sorter: true,
        key: '19',
        dataId: '',
        dataIndex: 'sharpeA',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    },
    {
        title: '信息比率',
        sorter: true,
        key: '20',
        dataId: '',
        dataIndex: 'infoA',
        render: (text) => {
            let content = text === undefined ? '--' : text
            return (<span>{content}</span>)
        }
    }
]

export default columns