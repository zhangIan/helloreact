import React, { Component } from 'react'
import { Input, Button, Icon, Row, Col, DatePicker, Select, message } from 'antd'
import Search from '../../style/tab_search.module.sass'
import '../../style/common.sass'
import HomeTable from './home_table'
import locale from 'antd/es/date-picker/locale/zh_CN'
import { axios } from '../../Utils/index'
import cookie from 'js-cookie'
import moment from 'moment'
// import iscroll from 'iscroll'

const { Option } = Select



export default class TabsSearch extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: [],
            fundSearch: '',
            addressSearch: '',
            rateRanking: '',
            drawdownRanking: '',
            mangerSearch: '',
            expend: false,
            changeN: true,
            backN: true,
            coreSelect: true,
            stock: false,
            marketing: false,
            bonds: false,
            future: false,
            options: false,
            currentPage: 1,
            totalPage: 1,
            pageSize: 20,
            takeDate: new Date().getTime(),
            sumCount: 0,
            searchParams: null,
            loading: false,
            selectionStatus: false,
            asset_type: [],
            strategy_type: [],
            c_asset_type: [],
            c_strategy_type: [],
            consultant_level: [],
            consultant_star: [],
            begin_org_foundation_date: '',
            end_org_foundation_date: '',
            is_reg: [],
            fund_status: [],
            begin_fund_foundation_date: moment(),
            end_fund_foundation_date: moment(),
            begin_new_nav_date: '',
            end_new_nav_date: '',
            issue_type: '',
            fund_star: [],
            date_type: 'year',
            returnType: 'section',
            drawdownType: 'section',
            fund_consultant: '',
            fund_manager: '',
            fund_name: '',
            reg_address: '',
            drawdown_end: '',
            drawdown_begin: '',
            sharpe_end: '',
            sharpe_begin: '',
            return_end: '',
            return_begin: ''
        }
    }

    toggleLoading = (status = false) => {
        this.setState({
             loading: status
        })
    }


    componentWillUnmount () {
        window.__LOADING__ = null
    }


    toggle = () => {
        this.setState({
            expend: !this.state.expend,
            selectionStatus: !this.state.selectionStatus,
            begin_fund_foundation_date: null,
            end_fund_foundation_date: null
        })
    }

    backChange = async val => {
        if (val === 'ranking') {
            await this.setState({backN: false})
        } else {
            await this.setState({backN: true})
        }
        await this.setState({drawdownType: val})

        let data = await axios({
            method: 'POST',
            url: '/api/hszcpz/index/screenRanking',
            data: JSON.stringify({
                token: cookie.get('token'),
                paramType: 'drawdown',
                asset_type: this.state.asset_type,
                c_interval: this.state.c_interval || 'year'
            })
        })

        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }
        data = JSON.parse(data.result)

        if (data.result === 'success') {
            const ranking = data.ranking
            if (ranking) {
                await this.setState({drawdownRanking: ranking})
            }
        }
    }

    inChange = async val => {
        if (val === 'ranking') {
            await this.setState({changeN: false})
        } else {
            await this.setState({changeN: true})
        }
        await this.setState({returnType: val})

        let data = await axios({
            method: 'POST',
            url: '/api/hszcpz/index/screenRanking',
            data: JSON.stringify({
                token: cookie.get('token'),
                paramType: 'return',
                asset_type: this.state.asset_type,
                c_interval: this.state.c_interval || 'year'
            })
        })


        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }
        data = JSON.parse(data.result)

        console.log(data)
        if (data.result === 'success') {
            const ranking = data.ranking
            if (ranking) {
                await this.setState({rateRanking: ranking})
            }
        }
    }

    // 搜索开始
    searchProduct = () => {
        let thisState = this.state
        let searchParams = {
            token: cookie.get('token')
        }
        if (thisState.selectionStatus) {

            if (thisState.return_begin && thisState.selectionStatus) {
                searchParams['return_begin'] = thisState.return_begin
            }

            if (thisState.return_end && thisState.selectionStatus) {
                searchParams['return_end'] = thisState.return_end
            }

            if (thisState.drawdown_begin && thisState.selectionStatus) {
                searchParams['drawdown_begin'] = thisState.drawdown_begin
            }

            if (thisState.drawdown_end && thisState.selectionStatus) {
                searchParams['drawdown_end'] = thisState.drawdown_end
            }


            if (thisState.asset_type.length > 0) {
                searchParams['asset_type'] = thisState.asset_type
            }

            if (thisState.strategy_type.length > 0 && thisState.strategy_type[0]!== null && thisState.asset_type.length > 0) {
                console.log(thisState.asset_type)
                searchParams['strategy_type'] = thisState.strategy_type
            }

            if (thisState.c_strategy_type.length > 0 && thisState.c_strategy_type[0]!== null) {
                searchParams['c_strategy_type'] = thisState.c_strategy_type
            }

            if (thisState.consultant_level.length > 0 && thisState.consultant_level[0]!== null) {
                searchParams['consultant_level'] = thisState.consultant_level
            }

            if (thisState.consultant_star.length > 0 && thisState.consultant_star[0]!== null) {
                searchParams['consultant_star'] = thisState.consultant_star
            }

            if (thisState.begin_org_foundation_date) {
                searchParams['begin_org_foundation_date'] = thisState.begin_org_foundation_date
            }

            if (thisState.end_org_foundation_date) {
                searchParams['end_org_foundation_date'] = thisState.end_org_foundation_date
            }

            if (thisState.is_reg.length > 0 && thisState.is_reg[0]!== null) {
                searchParams['is_reg'] = thisState.is_reg
            }

            if (thisState.fund_status.length > 0 && thisState.fund_status[0]!== null) {
                searchParams['fund_status'] = thisState.fund_status
            }

            if (thisState.begin_fund_foundation_date) {
                searchParams['begin_fund_foundation_date'] = new Date(thisState.begin_fund_foundation_date).getTime() / 1000
            }

            if (thisState.end_fund_foundation_date) {
                searchParams['end_fund_foundation_date'] = new Date(thisState.end_fund_foundation_date).getTime() / 1000
            }

            if (thisState.begin_new_nav_date) {
                searchParams['begin_new_nav_date'] = thisState.begin_new_nav_date
            }

            if (thisState.end_new_nav_date) {
                searchParams['end_new_nav_date'] = thisState.end_new_nav_date
            }

            if (thisState.issue_type.length > 0 && thisState.issue_type[0]!== null) {
                searchParams['issue_type'] = thisState.issue_type
            }

            if (thisState.fund_star.length > 0 && thisState.fund_star[0]!== null) {
                searchParams['fund_star'] = thisState.fund_star
            }

            // eslint-disable-next-line no-mixed-operators
            if (thisState.date_type && thisState.selectionStatus && thisState.selectionStatus && thisState.return_begin &&  thisState.return_end || 
                // eslint-disable-next-line no-mixed-operators
                thisState.date_type && thisState.selectionStatus && thisState.selectionStatus && thisState.drawdown_begin &&  thisState.drawdown_end) {
                searchParams['date_type'] = thisState.date_type
            }

            if (thisState.returnType && thisState.selectionStatus && thisState.return_begin &&  thisState.return_end) {
                searchParams['returnType'] = thisState.returnType
            }

            if (thisState.sharpeType && thisState.selectionStatus) {
                searchParams['sharpeType'] = thisState.sharpeType
            }

            if (thisState.drawdownType && thisState.selectionStatus && thisState.drawdown_begin &&  thisState.drawdown_end) {
                searchParams['drawdownType'] = thisState.drawdownType
            }
            
        }

        if (thisState.find_c_interval) {
            searchParams['find_c_interval'] = thisState.find_c_interval
        }

        if (thisState.find_c_rank_type) {
            searchParams['find_c_rank_type'] = thisState.find_c_rank_type
        }

        let searchInput = document.getElementsByClassName('searchProduct')
        Array.from(searchInput).forEach(item => {
            if (item.value) {
                searchParams[item.getAttribute('data')] = item.value
                let searchHistory = window.localStorage.getItem('searchHistory') || []
                searchHistory.push(item.value)
                window.localStorage.setItem(searchHistory, searchHistory)
                console.log(Array.from(window.localStorage.getItem(searchHistory)))
            }
        })

        console.log(this.state)

        this.setState({searchParams})
        this._axiosData(searchParams)

    }

    beginDate = (e, str) => {
        this.setState({begin_org_foundation_date: new Date(str).getTime() / 1000})
    }

    endDate = (e, str) => {
        this.setState({end_org_foundation_date: new Date(str).getTime() / 1000})
    }

    beginFund = async (e, str) => {
        console.log(str)
        this.setState({begin_fund_foundation_date: moment(str)})
    }
    endFund = async (e, str) => {
        await this.setState({end_fund_foundation_date: moment(str)})
        this.searchProduct()
    }

    beginNav = (e, str) => {
        this.setState({begin_new_nav_date: new Date(str).getTime() / 1000})
    }

    endNav = (e, str) => {
        this.setState({end_new_nav_date: new Date(str).getTime() / 1000})
    }

    selectionCheck = e => {
        this.setState({date_type: e})
    }

    downloadExcel = () => {
        window.location.href=`http://192.168.1.51:8090/netFilter/hszcpz/index/exportExcel?token=${cookie.get('token')}`
    }

    attetionParent = () => {
        this.props.setAttentions()
    }

    getTableData = (params, sumCount = 0, takeDate = new Date().getTime(), pageNo, find_c_rank_type, find_c_interval) => {
        if (params) {
            window.__LOADING__(false)
            this.setState({dataSource: params, sumCount: sumCount, takeDate: takeDate, currentPage: pageNo, find_c_rank_type: find_c_rank_type, find_c_interval: find_c_interval})
        }
    }

    _axiosData = (params) => {
        axios(window.__LOADING__)({
            // method: 'GET',
            method: 'POST',
            url: '/api/hszcpz/index/selectByScreen',
            data: JSON.stringify({
                pageSize:  this.state.pageSize,
                pageNo: 1,
                ...params
            })
        })
        .then(data => {
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }

            this.setState({loading: false})
            // console.log(data)
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }
            data = JSON.parse(data.result)
            console.log(data)

            if (data.result === 'success') {
                let dataList = data.sumlist
                // console.log(data.sumlist)
                if (dataList) {
                    for (let i = 0; i < dataList.length; i++) {
                        dataList[i].key = i  + 1
                    }
                }

                this.setState({
                    dataSource: dataList,
                    totalPage: data.pageCount,
                    takeDate: data.takeDate || new Date().getTime(),
                    sumCount: data.sumCount,
                    currentPage: 1
                })
            } else {
                if (data.code !== '200') {
                    message.info(data.errMsg || '获取数据失败')
                } else {
                    message.info('获取数据失败')
                }
            }
        })
    }


    _initData = () => {

        axios(window.__LOADING__)({
            method: 'POST',
            url: '/api/hszcpz/index/selectInit',
            data: JSON.stringify({
                pageSize:  this.state.pageSize,
                pageNo: this.state.currentPage,
                token: cookie.get('token')
            })
        })
        .then(data => {
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }

            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }
            data = JSON.parse(data.result)
            console.log(data)

            if (data.result === 'success') {
                let dataList = data.sumlist
                if (dataList) {
                    for (let i = 0; i < dataList.length; i++) {
                        dataList[i].key = i  + 1
                    }
                }

                this.setState({
                    dataSource: dataList,
                    totalPage: data.pageCount,
                    takeDate: data.takeDate || new Date().getTime(),
                    sumCount: data.sumCount
                })
            } else {
                if (data.code !== '200') {
                message.info(data.errMsg || '获取数据失败')
                } else {
                message.info('获取数据失败')
                }
            }
        })
    }

    topOptions = () => {
        let searchInput = document.getElementsByClassName('searchProduct')

        Array.from(searchInput).forEach(item => {
           item.value = ''
        })
    }

    resetState = () => {
        this.setState({
            asset_type: [],
            strategy_type: [],
            consultant_level: [],
            consultant_star: [],
            begin_org_foundation_date: '',
            end_org_foundation_date: '',
            is_reg: [],
            fund_status: [],
            begin_fund_foundation_date: '',
            end_fund_foundation_date: '',
            begin_new_nav_date: '',
            end_new_nav_date: '',
            issue_type: '',
            fund_star: [],
            returnType: '',
            sharpeType: '',
            drawdownType: '',
            fund_consultant: '',
            fund_manager: '',
            fund_name: '',
            reg_address: ''
        })
    }

    resetOptions = async () => {

        let selectElement = document.getElementsByClassName('customer_select_checked')
        let dataInput = document.getElementsByClassName('ant-calendar-picker-input')
        let selectInput = document.querySelectorAll('.common_search_selection .ant-select-selection-selected-value')
        Array.from(selectElement).forEach(item => {
            item.setAttribute('class', 'customer_selections')
            const targetEle = item
            const attributeStatus = item.getAttribute('data-type')
            targetEle.setAttribute('class', 'customer_selections')
            this.setState({[attributeStatus]: null})

            this.resetState()

        })
        dataInput[0].removeAttribute('readonly')
        dataInput[0].value = '-'
        Array.from(dataInput).forEach(item => {
            item.removeAttribute('readOnly')
            console.log(item.value)
            item.value = ''
        })

        await this.setState({fundSearch: '', mangerSearch: '', fund_name: '', addressSearch: '', return_begin: null,
        return_end: null,
        drawdown_begin: null,
        date_type: null,
        returnType: null,
        begin_fund_foundation_date: null,
        end_fund_foundation_date: null,
        drawdownType: null,
        drawdown_end: null})

        Array.from(selectInput).forEach(item => item.innerHTML = '')
        if (this.state.fund_name === '' || this.state.selectionStatus) {
            this._initData()
        }
    }

    fundChange (e) {
        this.setState({fundSearch: e.target.value})
    }

    mangerChange (e) {
        this.setState({mangerSearch: e.target.value})
    }

    addressChange (e) {
        this.setState({addressSearch: e.target.value})
    }

    async nameChange (e) {
        this.setState({fund_name: e.target.value})
    }

    async searchLike (e) {
        const currentValue = e.target.value

        if (currentValue !== '' && (/\S+/.test(currentValue))) {
            let data = await axios({
                method: 'POST',
                url: '/api/hszcpz/index/selectByLike',
                data: JSON.stringify({
                    fund_name: currentValue,
                    token: cookie.get('token')
                })
            })

            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }

            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }
            data = JSON.parse(data.result)

            if (data.result === 'success') {
                const fundName = data.fundNames
                if (fundName.length > 0 && currentValue !== '') {
                    this.setState({dropList: fundName})
                }
            } else {
                this.setState({dropList: null})
            }


        }
        if (currentValue === '') {
            this.setState({dropList: null})
            return
        }
    }

    async componentDidMount () {
        window.__LOADING__ = this.toggleLoading
        let dls = document.getElementsByClassName('customer_selections')
        let iList = document.querySelectorAll('.stars_lister_wrapper i')

        this._initData()

        Array.from(iList).forEach(item => {
            item.addEventListener('click', e => {
                e.target.setAttribute('class', 'stars_item_checked')
            }, false)
        })


        Array.from(dls).forEach(item => {
            item.addEventListener('click', (e) => {
                const classStatus = e.target.getAttribute('class')
                const targetEle = e.target
                const attributeStatus = e.target.getAttribute('data-type')
                const id = e.target.getAttribute('id')
                let temp = null
                const eleContainer = e.target.innerHTML

                if (classStatus === 'customer_selections') {
                    targetEle.setAttribute('class', 'customer_selections customer_select_checked')
                    this.setState({[id]: eleContainer})

                    switch (attributeStatus) {
                        case 'asset_type' :
                            temp = this.state.asset_type
                            temp.push(eleContainer)
                            this.setState({asset_type: temp})
                            break

                        case 'c_asset_type' :
                            temp = this.state.c_asset_type
                            temp.push(eleContainer)
                            this.setState({c_asset_type: temp})
                            break

                        case 'strategy' :
                            temp = this.state.strategy
                            temp.push(eleContainer)
                            this.setState({strategy: temp})
                            break
                        case 'strategy_type':
                            temp = this.state.strategy_type
                            temp.push(eleContainer)
                            this.setState({strategy_type: temp})
                            console.log(this.state.strategy_type)
                            break

                        case 'c_strategy_type':
                            temp = this.state.c_strategy_type
                            temp.push(eleContainer)
                            this.setState({c_strategy_type: temp})
                            console.log(this.state.c_strategy_type)
                            break

                        case 'fund_star':
                            temp = this.state.fund_star
                            temp.push(e.target.getAttribute('title'))
                            this.setState({fund_star: temp})
                            break

                        case 'consultant_star':
                            temp = this.state.consultant_star
                            temp.push(e.target.getAttribute('title'))
                            this.setState({consultant_star: temp})
                            break

                        default:
                            break
                    }



                } else {
                    targetEle.setAttribute('class', 'customer_selections')
                    switch (attributeStatus) {
                        case 'asset_type' :
                            temp = this.state.asset_type
                            temp.splice(temp.indexOf(eleContainer), 1)
                            this.setState({asset_type: temp})
                            break

                        case 'c_asset_type' :
                            temp = this.state.c_asset_type
                            temp.splice(temp.indexOf(eleContainer), 1)
                            this.setState({c_asset_type: temp})
                            break

                        case 'strategy' :
                            temp = this.state.strategy
                            temp.splice(temp.indexOf(eleContainer), 1)
                            this.setState({strategy: temp})
                            break

                        case 'strategy_type' :
                            temp = this.state.strategy_type
                            temp.splice(temp.indexOf(eleContainer), 1)
                            this.setState({strategy_type: temp})
                            console.log(this.state.strategy_type)
                            break

                        case 'c_strategy_type':
                            temp = this.state.c_strategy_type
                            temp.splice(temp.indexOf(eleContainer), 1)
                            this.setState({c_strategy_type: temp})
                            break

                        case 'fund_star':
                            temp = this.state.fund_star
                            temp.splice(temp.indexOf(e.target.getAttribute('title')), 1)
                            this.setState({fund_star: temp})
                            break

                        case 'consultant_star':
                            temp = this.state.consultant_star
                            temp.splice(temp.indexOf(e.target.getAttribute('title')), 1)
                            this.setState({consultant_star: temp})
                            break

                        default:
                            break
                    }

                    this.setState({[id]: null})
                }

                this.searchProduct()

            }, false)
        })

        document.addEventListener('click', (e) => {
            e.stopPropagation();
            this.setState({dropList: null})
        }, false)
    }

    renderDropList () {
        let listItem = ''
        if (this.state.dropList) {
            let list = this.state.dropList
            let arrList = []
            for(let i =0; i < 11; i++) {
                if (i < list.length) {
                    arrList.push(<li key={i} onClick={this.enterSearch.bind(this)}>{list[i]}</li>)
                    continue
                }
            }

            listItem = <div id="customerDrop" className={`customer_drop_down`}>
                <ul className={`customer_drop_list`}>
                    {arrList}
                </ul>
            </div>
        }

        // console.log(document.getElementById('customerDrop'))

        return listItem
    }

    searchFocus (e) {
        console.log(e.target.value)
    }

    async enterSearch (e) {
        const searchValue = e.target.value || e.target.innerHTML
        await this.setState({fund_name: searchValue, dropList: null})
        this.searchProduct()

    }

    changeInputs (e) {
        const stateId = e.target.getAttribute('id')
        this.setState({[stateId]: e.target.value})
    }


    render () {
        // const menu = (
        // <Menu className={`${Search.assets_hover_tips}`}>
        //     <Menu.Item key="1">搜索同时属于两类大类资产下的产品</Menu.Item>
        // </Menu>)
        return (
            <div className={Search.tab_container}>
                <div className={Search.tab_content}>
                    <ul className={Search.tab_fixed_content}>
                        <li className={`position_relative ${Search.product_name_input}`}>
                            <Input className='searchProduct' onKeyUp={this.searchLike.bind(this)} onPressEnter={this.enterSearch.bind(this)} value={this.state.fund_name} type='text' onChange={this.nameChange.bind(this)} data='fund_name' placeholder={'产品名称'} />
                            { this.renderDropList() }
                        </li>
                        <li><Button type="primary" onClick={() => {this.searchProduct()}}>搜索</Button></li>
                        <li><Button type="primary" onClick={this.toggle}>{this.state.expend ? '收起筛选项' : '展开筛选项'} <Icon type={this.state.expend ? 'up' : 'down'} /></Button></li>
                        <li><Button type="primary" onClick={ () => { this.resetOptions() }}>重置</Button></li>
                    </ul>

                    <div id='scrollHeight' className={Search.tab_slider_content} style={{display: this.state.expend ? 'block' : 'none'}}>
                        <div className={`${Search.scroll_slider_container}`}>
                        <Row className={`${Search.tab_tit_row} clear-fix`}>
                            <Col className='float_left' style={{width: '80px'}}><span className={`${Search.tab_column_tit}`}>大类资产：</span></Col>
                            <Col span={12}>
                                <dl className='clear-fix'>
                                    <dt>不限</dt>
                                    <dd data-type='c_asset_type' id='stock' className='customer_selections'>股票</dd>
                                    <dd data-type='c_asset_type' id='marketing' className='customer_selections'>市场中性</dd>
                                    <dd data-type='c_asset_type' id='bonds' className='customer_selections'>债券</dd>
                                    <dd data-type='c_asset_type' id='future' className='customer_selections'>期货</dd>
                                    <dd data-type='c_asset_type' id='options' className='customer_selections'>期权</dd>
                                    <dd data-type='c_asset_large' id='large' className='customer_selections'>宏观</dd>
                                    <dd data-type='c_asset_other' id='other' className='customer_selections'>其他</dd>
                                </dl>
                            </Col>
                            {/* <div className={`float_right`}>
                                <Dropdown overlay={menu}>
                                    <Switch checkedChildren='ON' unCheckedChildren='OFF' defaultChecked className={`${Search.tab_switch_check}`} />
                                </Dropdown>
                            </div> */}
                        </Row>

                        {/* <Row type='flex' className={`${Search.tab_tit_row}`} justify='start'>
                            <Col style={{width: '80px'}}><span className={Search.tab_column_tit}>大类资产：</span></Col>
                            <Col span={20}>
                                <dl className='clear-fix other_tags'>
                                    <dt>不限</dt>
                                    <dd data-type='asset_type' id='stockOptions' className='customer_selections'>股票策略</dd>
                                    <dd data-type='asset_type' id='boardOptions' className='customer_selections'>管理期货</dd>
                                    <dd data-type='asset_type' id='valueOptions' className='customer_selections'>相对价值</dd>
                                    <dd data-type='asset_type' id='eventOptions' className='customer_selections'>事件驱动</dd>
                                    <dd data-type='asset_type' id='bondsOptions' className='customer_selections'>债券策略</dd>
                                    <dd data-type='asset_type' id='macroOptions' className='customer_selections'>宏观策略</dd>
                                    <dd data-type='asset_type' id='groupOptions' className='customer_selections'>组合策略</dd>
                                    <dd data-type='asset_type' id='moreOptions' className='customer_selections'>多策略</dd>
                                    <dd data-type='asset_type' id='otherOptions' className='customer_selections'>其他一级策略</dd>
                                </dl>
                            </Col>
                        </Row> */}

                        <Row className={Search.tab_tit_row} type='flex' justify='start'>
                            <Col style={{width: '80px'}}><span className={Search.tab_column_tit}>投资策略：</span></Col>
                            <Col>
                                <dl className='clear-fix'>
                                    <dt>不限</dt>
                                </dl>
                            </Col>
                            <Col>
                                <ul style={{display: this.state.stock ? 'block': 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>股票</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>主观权益</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>量化选股</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>指数增强</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>ETF</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>期货</li>
                                </ul>

                                <ul style={{display: this.state.marketing ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>市场中性</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>TO</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>高频</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>低频</li>
                                </ul>

                                <ul style={{display: this.state.bonds ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>债券</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>利率和高等级信用债</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>低等级信用债</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>可转债</li>
                                </ul>

                                <ul style={{display: this.state.future ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>期货</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>高频</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>中高频</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>低频</li>
                                </ul>

                                <ul style={{display: this.state.options ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>期权</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>波动率曲面套利</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>做市套利</li>
                                    <li data-type='c_strategy_type' className='customer_selections'>择时</li>
                                </ul>


                                <ul style={{display: this.state.stockOptions ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>其他</li>
                                    <li data-type='strategy_type' className='customer_selections'>相对价值</li>
                                    <li data-type='strategy_type' className='customer_selections'>事件驱动</li>
                                    <li data-type='strategy_type' className='customer_selections'>组合策略</li>
                                    <li data-type='strategy_type' className='customer_selections'>多策略</li>
                                    <li data-type='strategy_type' className='customer_selections'>其他二级策略</li>
                                    <li data-type='strategy_type' className='customer_selections'>新三板</li>
                                    <li data-type='strategy_type' className='customer_selections'>海外基金</li>
                                    <li data-type='strategy_type' className='customer_selections'>货币基金</li>
                                </ul>


                                <div style={{display: (
                                    !(this.state.moreOptions)&&
                                    !(this.state.groupOptions)&&
                                    !(this.state.otherOptions)&&
                                    !(this.state.bondsOptions)&&
                                    !(this.state.boardOptions)&&
                                    !(this.state.macroOptions)&&
                                    !(this.state.eventOptions)&&
                                    !(this.state.valueOptions)&&
                                    !(this.state.stockOptions)&&
                                    !(this.state.options) &&
                                    !(this.state.bonds) &&
                                    !(this.state.future) &&
                                    !(this.state.marketing) &&
                                    !(this.state.stock)) ? 'block' : 'none'}} className={Search.core_rating}>请选择大类资产</div>
                            </Col>
                        </Row>

                        <Row type='flex' className={Search.tab_title_split} justify='start'>
                            <Col style={{width: '110px'}}><span className={Search.tab_column_tit}>产品星级：</span></Col>
                            <Col>
                                <dl className='clear-fix'>
                                    <dd data-type='fund_star' title='1' id='fund_star_1' className='customer_selections'>一星</dd>
                                    <dd data-type='fund_star' title='2' id='fund_star_2' className='customer_selections'>二星</dd>
                                    <dd data-type='fund_star' title='3' id='fund_star_3' className='customer_selections'>三星</dd>
                                    <dd data-type='fund_star' title='4' id='fund_star_4' className='customer_selections'>四星</dd>
                                    <dd data-type='fund_star' title='5' id='fund_star_5' className='customer_selections'>五星</dd>
                                </dl>
                            </Col>

                            <Col className={Search.line_split_tab}>
                                <span className={Search.tab_normal_tit}>投顾星级：</span>
                                {
                                    (<div className={`${Search.stars_wrapper} stars_lister_wrapper`}>
                                        <dl className='clear-fix'>
                                            <dd data-type='consultant_star' id='consultant_star_1' title='1' className='customer_selections'>一星</dd>
                                            <dd data-type='consultant_star' id='consultant_star_2' title='2' className='customer_selections'>二星</dd>
                                            <dd data-type='consultant_star' id='consultant_star_3' title='3' className='customer_selections'>三星</dd>
                                            <dd data-type='consultant_star' id='consultant_star_4' title='4' className='customer_selections'>四星</dd>
                                            <dd data-type='consultant_star' id='consultant_star_5' title='5' className='customer_selections'>五星</dd>
                                        </dl>
                                    </div>)
                                }
                            </Col>
                        </Row>

                        <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: '110px'}}><span className={Search.tab_column_tit}>产品成立时间：</span></Col>
                            <Col>
                                <span></span>
                                <DatePicker locale={locale} onChange={this.beginFund} value={this.state.begin_fund_foundation_date} className={`${Search.tab_search_radius} date_picker_content`} placeholder='起始日期' />
                                <span className={Search.split_tit}></span>
                                <DatePicker locale={locale} onChange={this.endFund} value={this.state.end_fund_foundation_date} className={`${Search.tab_search_radius}  date_picker_content`} placeholder='截止日期' />
                            </Col>
                        </Row>

                        <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: 'calc(100% - 122px)'}}>
                                <Row type='flex' className='pb_10'>
                                    <Col>
                                        <div className={Search.tab_selection_wrapper}>
                                            <span className={Search.tab_selections_tit}>收益风险区间：</span>
                                            <Select value={this.state.date_type} onChange={this.selectionCheck.bind(this)} className={`${Search.selection_content} common_search_selection`}>
                                                <Option value="year">今年以来</Option>
                                                <Option value="m1">近一个月</Option>
                                                <Option value="m3">近三月</Option>
                                                <Option value="m6">近六月</Option>
                                                <Option value="y1">近一年</Option>
                                                <Option value="y2">近二年</Option>
                                                <Option value="y3">近三年</Option>
                                                <Option value="y5">近五年</Option>
                                                <Option value="total">成立以来</Option>
                                            </Select>
                                        </div>
                                    </Col>

                                    <Col>
                                    <div className={Search.tab_selection_wrapper}>
                                        <span className={Search.tab_selections_tit}>Sharpe比率：</span>
                                        <Select defaultValue='section' onChange={this.sharpeChange} className={Search.selection_content}>
                                            <Option value="section">绝对Sharpe比区间</Option>
                                            <Option value="ranking">Sharpe比市场排名</Option>
                                        </Select>
                                        <Input id='sharpe_begin' className={Search.percent_tab_input} />
                                        <span>{this.state.sharpeN ? '%' : '/N'}</span>
                                        <span className={Search.selection_percent_split}>~</span>
                                        <Input id='sharpe_end' className={Search.percent_tab_input} /><span>{this.state.sharpeN ? '%' : '/N'}</span>
                                    </div>
                                    </Col>
                                </Row>

                                <Row type='flex' className='pb_10'>
                                    <Col>
                                        <div className={Search.tab_selection_wrapper}>
                                            <span className={Search.tab_selections_tit}>收益：</span>
                                            <Select value={this.state.returnType} onChange={this.inChange} className={`${Search.selection_content} common_search_selection`}>
                                                <Option value="section">绝对收益区间</Option>
                                                <Option value="ranking">收益市场排名</Option>
                                            </Select>
                                            <Input id='return_begin' onChange={this.changeInputs.bind(this)} className={Search.percent_tab_input} value={this.state.return_begin} />
                                            <span>{this.state.changeN ? '%' : `/1`}</span>
                                            <span className={Search.selection_percent_split}>~</span>
                                            <Input id='return_end' onChange={this.changeInputs.bind(this)} className={Search.percent_tab_input} value={this.state.return_end} /><span>{this.state.changeN ? '%' : `/${this.state.rateRanking}`}</span>
                                        </div>


                                    </Col>
                                    <Col>
                                        <div>
                                            <span className={Search.tab_selections_tit}>回撤：</span>
                                            <Select value={this.state.drawdownType} onChange={this.backChange} className={`${Search.selection_content} common_search_selection`}>
                                                <Option value="section">绝对回撤区间</Option>
                                                <Option value="ranking">回撤市场排名</Option>
                                            </Select>
                                            <Input id='drawdown_begin' onChange={this.changeInputs.bind(this)} className={Search.percent_tab_input} value={this.state.drawdown_begin} />
                                            <span>{this.state.backN ? '%' : `/1`}</span>
                                            <span className={Search.selection_percent_split}>~</span>
                                            <Input id='drawdown_end' onChange={this.changeInputs.bind(this)} className={Search.percent_tab_input} value={this.state.drawdown_end} /><span>{this.state.backN ? '%' : `/${this.state.drawdownRanking}`}</span>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>

                        </Row>
                        </div>
                    </div>
                </div>

                <div className={Search.slider_container}>
                    <div className={Search.tab_buttons}>
                        <ul className='clear-fix'>
                            <li>产品表现</li>
                            <li>
                                <Button type="primary" onClick={() => {this.downloadExcel()}}><Icon type="download" /> 下载 </Button>
                            </li>
                        </ul>
                    </div>
                    <HomeTable getTableData={this.getTableData} attetionParent={this.props.setAttentions} {...this.state} />
                </div>
            </div>
        )
    }
}
