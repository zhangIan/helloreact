import React, { Component } from 'react'
import ProductStyle from '../../product/product.module.sass'
import WrapperHeader from './wrapper_header'
import { axios } from '../../../Utils/index'
import cookie from 'js-cookie'
import { message, Table } from 'antd'
import url from 'url'
import { Spin } from 'antd'
import moment from 'moment'

const productColumns = [
    {keys: 'cFundName', title: '产品名称', next: '产品简称', nextKey: 'cFundAbbreName'},
    {keys: 'cFoundationDate', title: '成立日期', next: '结束日期', nextKey: 'cEndDate'},
    {keys: 'cFundIssuer', title: '基金发行人全称', next: '基金经理', nextKey: 'cFundManager'},
    {keys: 'cAssetScale', title: '管理规模（万）', next: '开放日', nextKey: 'cOpenDate'},
    {keys: 'cFeeSubscription', title: '认购费率', next: '赎回费率', nextKey: 'cFeeRedemption'},
    {keys: 'cFeeManagement', title: '固定管理费率', next: '业绩报酬计提', nextKey: 'cFeePerformance'},
    {keys: 'cStopLossLine', title: '止损线', next: '预警线', nextKey: 'cWarningLine'}
]

const strategyColumns = [
    {keys: 'cFundType', title: '产品策略类型', next: '', nextKey: ''},
    {keys: 'cStrategyLong', title: '股票多头策略占比（%）', next: '股票多空策略占比（%）', nextKey: 'cStrategyLongShort'},
    {keys: 'cStrategyNeutral', title: '市场中性策略占比（%）', next: '套利类策略占比（%）', nextKey: 'cStrategyArbitrage'},
    {keys: 'cStrategyCta', title: 'CTA策略占比（%）', next: '其他策略占比（%）', nextKey: 'cStrategyOther'},
    {keys: 'nNav', title: '最新净值', next: '最新净值时间', nextKey: 'cNavDateundId'},
    {keys: 'cTotalReturn', title: '总收益（%）', next: '年化收益', nextKey: 'cTotalReturnA'},
    {keys: 'cTotalMaxDrawdown', title: '最大回撤', next: 'Sharpe比率', nextKey: 'cTotalSharpeA'}
]
export default class Infos extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    async _initData (params) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
            url: '/api/hszcpz/product/proInfo',
            method: 'POST',
            data: JSON.stringify({
                fund_id: query.id,
                token: cookie.get('token'),
                ...params
            })
        })

        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)
        // console.log(result)
        if (result.result === 'success') {
            // console.log(result, 'advisor items-------')
            return result.proInfo
        } else {
            message.error('获取数据失败')
        }
    }

    async _initStrategy (params) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
            url: '/api/hszcpz/product/proStrategy',
            method: 'POST',
            data: JSON.stringify({
                fund_id: query.id,
                token: cookie.get('token'),
                ...params
            })
        })

        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)

        if (result.result === 'success') {
            return result.proStrategy
        } else {
            message.error('获取数据失败')
        }
    }

    async UNSAFE_componentWillMount () {
        const initData = await this._initData()
        const intStrategy = await this._initStrategy()

        if (initData) {
            initData.cFoundationDate = moment(initData.cFoundationDate).format('YYYY-MM-DD')
            initData.cEndDate = initData.cEndDate ? moment(initData.cEndDate).format('YYYY-MM-DD') : '--'
            this.setState({
                productInfos: initData
            })
        }

        if (intStrategy) {
            intStrategy.cNavDateundId = intStrategy.cNavDateundId ? moment(intStrategy.cNavDateundId).format('YYYY-MM-DD') : '--'
            intStrategy.cStrategyCta = intStrategy.cStrategyCta ? parseFloat(intStrategy.cStrategyCta).toFixed(2) : '0.00'
            intStrategy.cStrategyLongShort = intStrategy.cStrategyLongShort ? parseFloat(intStrategy.cStrategyLongShort).toFixed(2) : '0.00'
            intStrategy.cStrategyArbitrage = intStrategy.cStrategyArbitrage ? parseFloat(intStrategy.cStrategyArbitrage).toFixed(2) : '0.00'
            intStrategy.nNav = intStrategy.nNav ? parseFloat(intStrategy.nNav).toFixed(2) : '0.00'
            intStrategy.cTotalReturn = intStrategy.cTotalReturn ? parseFloat(intStrategy.cTotalReturn).toFixed(2) : '0.00'
            intStrategy.cTotalReturnA = intStrategy.cTotalReturnA ? parseFloat(intStrategy.cTotalReturnA).toFixed(2) : '0.00'
            intStrategy.cTotalMaxDrawdown = intStrategy.cTotalMaxDrawdown ? parseFloat(intStrategy.cTotalMaxDrawdown).toFixed(2) : '0.00'
            intStrategy.cTotalSharpeA = intStrategy.cTotalSharpeA ? parseFloat(intStrategy.cTotalSharpeA).toFixed(2) : '0.00'
            this.setState({
                strategyInfos: intStrategy
            })
        }
    }



    render () {
        const { productInfos, strategyInfos } = this.state
        return (
        <div className={ProductStyle.charts_items_list}>
            <div className={ProductStyle.charts_item_bg}>
                <WrapperHeader title={`产品基本资料`} iconStatus={true} />
                <div className={`${ProductStyle.infos_table_padding}`}>
                    <Spin spinning={productInfos ? false : true}>
                    {productInfos ? <table className={`customer_table_list customer_table_border_red mt_10`}>
                        <tbody>
                            {productColumns.map((item, index) => {
                            return (
                            <tr key={index}>
                                <td>{item.title}</td>
                                <td>{productInfos[item.keys] ? productInfos[item.keys] : '--'}</td>
                                <td>{item.next}</td>
                                <td>{productInfos[item.nextKey] ? productInfos[item.nextKey] : '--'}</td>
                            </tr>)
                            })}
                        </tbody>
                     </table> : <Table dataSource={[]} />}
                     </Spin>
                </div>
            </div>
            <div className={`${ProductStyle.charts_item_bg} pb_40`}>
                <WrapperHeader title={`产品策略指标`} iconStatus={true} />
                <div className={`${ProductStyle.infos_table_padding}`}>
                    <Spin spinning={strategyInfos ? false : true}>
                    {strategyInfos ? <table className={`customer_table_list customer_table_border_red mt_10`}>
                                    <tbody>
                                        {strategyColumns.map((item, index) => {
                                            let result = null
                                            if (item.next) {
                                            result = <tr key={index}>
                                                        <td>{item.title}</td>
                                                        <td>{strategyInfos[item.keys] ? strategyInfos[item.keys] : '--'}</td>
                                                        <td>{item.next}</td>
                                                        <td>{strategyInfos[item.nextKey] ? strategyInfos[item.nextKey] : '--'}</td>
                                                    </tr>
                                            } else {
                                                result = <tr key={index}>
                                                            <td>{item.title}</td>
                                                            <td colSpan={3}>{strategyInfos[item.keys] ? strategyInfos[item.keys]: '--'}</td>
                                                        </tr>
                                            }
                                        return result
                                        })}
                                    </tbody>
                                </table> : <Table dataSource={[]} />}
                    </Spin>
                </div>
            </div>
        </div>)
    }
}
