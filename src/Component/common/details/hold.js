import React, { Component } from 'react'
import ProductStyle from '../../product/product.module.sass'
import WrapperHeader from './wrapper_header'
import CommonBar from '../../common/charts/common_bar'
import echarts from 'echarts/lib/echarts'
import CommonChart from '../../common/charts/common_charts'
import locale from 'antd/es/date-picker/locale/zh_CN'
// 导入公共的表格样式
import { chartsColors } from '../../../Utils/charts_config/charts_config'
import TableChartsConfig from '../../../Utils/charts_config/table_chart'
import { axios } from '../../../Utils/index'
import cookie from 'js-cookie'
import { message, DatePicker } from 'antd'
import moment from 'moment'

// import ReportTable from '../common_table/report_table'
import CommonCharts from '../../common/charts/common_charts'
import url from 'url'

let gridOptions = {
    legend: {
        icon: 'rect',
        data: ['金属', '农产品', '能化', '黑色系', '金融', '贵金属'],
        align: 'left',
        bottom: 0,
        color: '#eee'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 26,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        },
        formatter: (value) => {
            let itemDesc = `${value[0].name}`
            
            value.forEach(item => {
                itemDesc += `<br />${item.marker} ${item.seriesName}： ${item.value}%`
            })

            return itemDesc
        }
    },
    dataZoom: [{
            type: 'inside',
            height: 30,
            left: '70px',
            right: '70px',
            bottom: 0,
            start: 0,
            realtime: true
    }],
    grid: {
        show: true,
        borderwidth: 1,
        borderColor: '#ccc',
        top: '20px',
        right: '10px',
        left: '50px',
        bottom: 80
    },
    xAxis: {
        data: [],
        axisLabel: {
            rotate: 45,
            textStyle: {
                color: '#999'
            }
        },
        axisTick: {
            show: false
        },
        axisLine: {
            show: false
        },
        z: 10
    },
    yAxis: [
        {
            name: '',
            nameLocation: 'end',
            nameTextStyle: {
                color: '#999',
                align: 'center'
            },
            color: '#ccc',
            axisLabel: {
                show: true,
                typeStyle: {
                    color: '#333'
                },
                color: '#999',
                formatter: '{value} '
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc'
                }
            },
            splitNumber: 6,
            splitLine: {
                lineStyle: {
                    type: 'dotted',
                    color: '#ccc'
                }
            }
        }
    ],
    series: []
}

let pieOptions = {
            
            title: {
                x:'center',
                top: 10,
                text: '保证金（%）',
                textStyle: {
                    color: '#999',
                    fontSize: 20,
                    fontWeight: '500'
                }
            },
            tooltip : {
                show: true,
                trigger: 'item',
                formatter: (value) => {
                    let toolTip = ``
                    toolTip += `${value.marker}${value.name}：${value.value}%`
                    return toolTip
                }
            },
            grid: {
                containLabel: true,
                top: 0,
                x: 'center'
            },
            legend: {
                show: false,
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 20,
                bottom: 20,
                data: []
            },
            series : [ 
                {
                    type: 'pie',
                    animation: true,
                    color: chartsColors,
                    radius: ['32%', '54%'],
                    center: ['50%', '50%'],
                    animationEasing: 'elasticOut',
                    animationDelay: function (idx) {
                        return Math.random() * 500;
                    },
                    labelLine: {
                        show: true,
                        normal: {
                            length: 60,
                            length2: 30,
                            lineStyle: {
                                color: '#ccc'
                            }
                        }
                    },
                    label: {
                        show: true,
                        formatter: '{b}: {d}% ',
                        margin: 10,
                        alignTo: 'edge',
                        padding: [10, 5, 10, 6],
                        textStyle: {
                            color: '#666',
                            fontSize: 13
                        }
                    },
                    data: [
                        {value: 300, name: '非金属材料'},
                        {value: 500, name: '能源化工'},
                        {value: 900, name: '农副产品'},
                        {value: 1200, name: '股投'},
                        {value: 329, name: '贵金属'}
                    ],
                }
            ]
        }

const holderOptions = {
    tooltip: {
        trigger: 'axis',
        formatter:  (value) => {
            if (value.length > 1) {
                return `${value[1].name}<br />
                持仓信息：${value[0].marker}${parseFloat(value[0].value).toFixed(2)}%<br />
                ${value[1].seriesName}：${value[1].marker}${parseFloat(value[1].value).toFixed(2)}
                ` 
            } else {
                return `${value[0].name}<br />
                持仓信息：${value[0].marker}${parseFloat(value[0].value).toFixed(2)}%` 
            }
            
        },
        axisPointer: {
            type: 'line',
            lineStyle: {
                width: 60,
                color: 'rgba(0, 0, 0, 0.1)'
            },
            z: -1
        }
    },
    grid: {
        top: '50px',
        right: '46px',
        left: '46px',
        bottom: 88,
        y: 100
    },
    
    legend: {
        x: 'center',
        icon: 'line',
        bottom: -2,
        data: []
    },
    xAxis: [{
        type: 'category',
        data: [],
        axisLine: {
            show: true,
            lineStyle: {
                color: '#ccc'
            }
        },
        axisLabel: {
            margin: 10,
            color: '#999',
            show: true,
            textStyle: {
                fontSize: 14
            },
        },
    }],
    yAxis: [
        {
            name: '仓位(%)',
            type: 'value',
            nameLocation: 'end',
            splitNumber: 5,
            nameTextStyle: {
                color: '#999',
                align: 'center'
            },
            color: '#ccc',
            axisLabel: {
                show: true,
                typeStyle: {
                    color: '#333'
                },
                color: '#999',
                formatter: '{value} '
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc'
                }
            },
            splitLine: {
                lineStyle: {
                    type: 'dotted',
                    color: '#ccc'
                }
            }
        },
        {
            name: '复权累计净值',
            nameLocation: 'end',
            max: 3,
            interval: 3 / 5,
            type: 'value',
            nameTextStyle: {
                color: '#999',
                align: 'center'
            },
            color: '#ccc',
            axisLabel: {
                show: true,
                typeStyle: {
                    color: '#333'
                },
                color: '#999',
                formatter: value => {
                    return parseFloat(value).toFixed(2)
                }
            },
            axisLine: {
                show: false,
                lineStyle: {
                    color: '#ccc'
                }
            },
            splitLine: {
                lineStyle: {
                    type: 'dotted',
                    color: '#ccc'
                }
            }
        }
],
    dataZoom: [
        {
            type: 'slider',
            height: 30,
            left: '70px',
            right: '70px',
            bottom: 24
        }
    ],
    series: [
        {
            type: 'bar',
            data:  [],
            barMaxWidth: '30px',
            itemStyle: {
                normal: {
                    color: '#fea062',
                    barBorderRadius: [1, 1, 1, 1],
                }
            }
        },
        {
            name:'',
            type: 'line',
            data: [],
            symbol: false,
            symbolSize: 0,
            yAxisIndex: 1,
            lineStyle: {
                normal: {
                    shadowColor: 'rgba(0, 0, 0, 0.2)',
                    shadowOffsetY: 2,
                    shadowOffsetX: 2,
                    shadowBlur: 5
                }
            },
            itemStyle: {
                normal: {
                    color: '#ff2b2b',
                }
            },
        }
]
}



export default class Hold extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    // 股票多投持仓
    async _getHold (startDate, endDate) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
                url: '/api/hszcpz/product/inventoryOverview',
                method: 'POST',
                data: JSON.stringify({
                    fundId: query.id,
                    token: cookie.get('token'),
                    startDate: startDate || moment(new Date().getTime() - (365 * 24 * 60 * 60 * 1000)).format('YYYY-MM-DD'),
                    endDate: endDate || moment(new Date()).format('YYYY-MM-DD')
                })
            })
            
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }
            
            let result = JSON.parse(data.result)
            // console.log(result)
            if (result.status === 'success') {
                let resultArr = {
                    legend: [],
                    xDate: [],
                    lineData: [],
                    barData: [],
                    max: ''
                }
                let results = result.result
                
                resultArr.legend.push(result.nav_name)
                resultArr.max = result.nav_max
                results.forEach(item => {
                    resultArr.xDate.push(item.t_date_std)
                    resultArr.lineData.push(item.nav)
                    resultArr.barData.push(item.position_ratio)
                })
                
                return resultArr
            }
    }
    // 行业配置
    async _tectonic (startDate, endDate) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
                url: '/api/hszcpz/product/tectonic',
                method: 'POST',
                data: JSON.stringify({
                    fundId: query.id,
                    token: cookie.get('token'),
                    beginTime: startDate || (Date.parse(new Date()) - 365 * 24 * 60 * 60 * 1000) / 1000,
                    endTime: endDate || (Date.parse(new Date())) / 1000
                })
            })
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }

            let result = JSON.parse(data.result)
            // console.log(result)
            if (result.result === 'success') {
                let datas = result.tectonic
                let xDate = Object.keys(datas)
                let returnData = {}
                let firstData = [],  secData = [], threeData = [], lastData = [], legend = []

                // eslint-disable-next-line no-unused-vars
                for (let item in datas) {
                    let keys = Object.keys(datas[item])
                    legend = keys
                    firstData.push(datas[item][keys[0]])
                    secData.push(datas[item][keys[1]])
                    threeData.push(datas[item][keys[2]])
                    lastData.push(datas[item][keys[3]])
                }
                
                returnData.xDate = xDate
                returnData.firstData = firstData
                returnData.secData = secData
                returnData.threeData = threeData
                returnData.lastData = lastData
                returnData.legend = legend
                // console.log(returnData)
                return returnData
            }
    }
    // 行业集中度
    async _concentration (startDate, endDate) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
                url: '/api/hszcpz/product/industryConcentration',
                method: 'POST',
                data: JSON.stringify({
                    fundId: query.id,
                    token: cookie.get('token'),
                    beginTime: startDate || (Date.parse(new Date()) - 365 * 24 * 60 * 60 * 1000) / 1000,
                    endTime: endDate || (Date.parse(new Date())) / 1000
                })
            })
            
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }

            let result = JSON.parse(data.result)
            // console.log(result)
            if (result.result === 'success') {
                // this.setState({productInfo: result.product})
                let resultObject = {xDate: [], valueData: []}
                let resultContent = result.industryConcentration
                let objectKeys = Object.keys(resultContent ? resultContent : {})
                resultObject.xDate = objectKeys

                objectKeys.forEach(item => {
                    resultObject.valueData.push(resultContent[item])
                })

                // console.log(resultObject)
                return resultObject
            }
    }

    // 期货板块配置
    async _futuresConcentration (startDate, endDate) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
                url: '/api/hszcpz/product/tectonicFutures',
                method: 'POST',
                data: JSON.stringify({
                    fundId: query.id,
                    token: cookie.get('token'),
                    beginTime: startDate  || (Date.parse(new Date()) - 365 * 24 * 60 * 60 * 1000) / 1000,
                    endTime: endDate || (Date.parse(new Date())) / 1000
                })
            })
            
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }

            let result = JSON.parse(data.result)
            
            if (result.result === 'success') {
                let futureResult = {xDate: [], legend: [], valueData1: [], valueData2: [], valueData3: [], valueData4: [], valueData5: [], valueData6: []}
                const resultContent = result.tectonicFutures
                futureResult.xDate = Object.keys(resultContent)
                
                if (futureResult.xDate.length > 0) {
                    futureResult.legend = Object.keys(resultContent[futureResult.xDate[0]])
                }
                
                futureResult.xDate.forEach(item => {
                    let keys = Object.keys(resultContent[item])
                    futureResult.valueData1.push(resultContent[item][keys[0]])
                    futureResult.valueData2.push(resultContent[item][keys[1]])
                    futureResult.valueData3.push(resultContent[item][keys[2]])
                    futureResult.valueData4.push(resultContent[item][keys[3]])
                    futureResult.valueData5.push(resultContent[item][keys[4]])
                    futureResult.valueData6.push(resultContent[item][keys[5]])
                })

                // console.log(futureResult)
                return futureResult
            }
    }

    // 期货 品种分布 饼状图
    async _futuresPie (checkDate) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
                url: '/api/hszcpz/product/varietyPie',
                method: 'POST',
                data: JSON.stringify({
                    fundId: query.id,
                    token: cookie.get('token'),
                    tDate: checkDate || Date.parse(new Date()) / 1000
                })
            })
            
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }

            let result = JSON.parse(data.result)
            // console.log(result)
            if (result.result === 'success') {
                // this.setState({productInfo: result.product})
                let pieResult = [{name: '', value: ''}]
                const resultContent = result.varietyPie
                // console.log(result.varietyPie)
                resultContent.forEach(item => {
                    pieResult.push({
                        value: item.depositRatio,
                        name: item.cVariety,
                        getDate: result.getDate
                    })
                })

                pieResult = pieResult.filter(item => item['name'] !== '' && item['value'] !== '')
                
                return pieResult
            }
    }

    // 期货 品种分布 柱状图
    async _futuresBar (checkDate) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
                url: '/api/hszcpz/product/varietyBar',
                method: 'POST',
                data: JSON.stringify({
                    fundId: query.id,
                    token: cookie.get('token'),
                    // tDate: checkDate || '1339084800'
                    tDate: checkDate || Date.parse(new Date()) / 1000
                })
            })
            
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }

            let result = JSON.parse(data.result)
            // console.log(result)
            if (result.result === 'success') {
                const barResult = result.varietyBar
                let barsData = {legend: [], longBar: [], shortBar: [], getDate: result.getDate}
                // console.log(barResult)
                barResult.forEach(item => {
                    barsData.legend.push(item.cVariety)
                    barsData.longBar.push(item.longDepositRatio)
                    barsData.shortBar.push(-item.shortDepositRatio)
                })

                return barsData
            }
    }

    // 期货仓位总览
    async _futuresHold (startDate, endDate) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
                url: '/api/hszcpz/product/forwardInventoryOverview',
                method: 'POST',
                data: JSON.stringify({
                    fundId: query.id,
                    token: cookie.get('token'),
                    startDate: startDate || moment(new Date().getTime() - (365 * 24 * 60 * 60 * 1000)).format('YYYY-MM-DD'),
                    endDate: endDate ||  moment(new Date().getTime()).format('YYYY-MM-DD')
                })
            })
            
            message.destroy()
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }

            let result = JSON.parse(data.result)
            // console.log(result)
            if (result.status === 'success') {
                let resultArr = {
                    legend: [],
                    xDate: [],
                    lineData: [],
                    barData: [],
                    max: ''
                }
                let results = result.result
                
                resultArr.legend.push(result.nav_name)
                resultArr.max = result.nav_max
                results.forEach(item => {
                    resultArr.xDate.push(item.t_date_std)
                    resultArr.lineData.push(item.nav)
                    resultArr.barData.push(item.position_ratio)
                })
                
                return resultArr
            }
    }


    async componentDidMount () {
        // 股票行业配置
        const tectonic = await this._tectonic() || { xDate: [], firstData: [], secData: [], threeData: [], lastData: [], legend: []}
        
        // 股票持仓信息
        const holderData = await this._getHold() || { legend: [], xDate: [], lineData: [], barData: [], max: 3 }
        // 股票行业集中度
        const resultObject = await this._concentration() || {xDate: [], valueData: []}
        // 期货 板块配置
        const futureObject = await this._futuresConcentration() || {xDate: [], legend: [], valueData1: [], valueData2: [], valueData3: [], valueData4: [], valueData5: [], valueData6: []}
        
        // 期货饼图
        const pieData = await this._futuresPie() || [{name: '', value: ''}]
        // 期货仓位总览
        const futureHold = await this._futuresHold() || { legend: [], xDate: [], lineData: [], barData: [], max: 3 }

        const futureBar = await this._futuresBar() || {legend: [], longBar: [], shortBar: [], getDate: ''}

        holderOptions.legend.data = holderData.legend
        holderOptions.xAxis[0].data = holderData.xDate
        holderOptions.yAxis[0].max = 100
        holderOptions.yAxis[1].max = holderData.max
        holderOptions.yAxis[1].interval = holderData.max / 5

        holderOptions.series[0].data = holderData.barData
        holderOptions.series[1].data = holderData.lineData
        holderOptions.series[1].name = holderData.legend[0]

        this.refs.holderBar.setOptions(holderOptions)

        const stackOptions = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            legend: {
                icon: 'rect',
                data: tectonic.legend || [],
                align: 'left',
                bottom: 0,
                color: '#eee',
                show: false
            },
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            xAxis: [{
                type: 'category',
                data: tectonic.xDate || [],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    max: 100,
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
        ],
            dataZoom: [
                {
                    type: 'inside',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0,
                    start: tectonic.xDate.length > 100 ?  90 : 0,
                    realtime: true
                },{
                    type: 'slider',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0,
                    start: tectonic.xDate.length > 100 ?  90 : 0,
                    realtime: true,
                    zoomLock: true
                }
            ],
            series: [
                {
                    name: tectonic.legend[0]|| '',
                    type: 'bar',
                    data: tectonic.firstData || [],
                    stack: 'four',
                    barMaxWidth: '30px',
                    large: true,
                    itemStyle: {
                        normal: {
                            color: '#55aee8',
                            barBorderRadius: [1, 1, 1, 1],
                        }
                    }
                },
                {
                    name: tectonic.legend[1]|| '',
                    type: 'bar',
                    data: tectonic.secData || [],
                    stack: 'four',
                    barMaxWidth: '30px',
                    large: true,
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fe8687',
                        }
                    },
                },
                {
                    name: tectonic.legend[2]|| '',
                    type: 'bar',
                    data: tectonic.threeData || [],
                    symbol: false,
                    large: true,
                    symbolSize: 0,
                    barMaxWidth: '30px',
                    stack: 'four',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#f6b78c',
                        }
                    },
                },
                {
                    name: tectonic.legend[3]|| '',
                    type: 'bar',
                    data: tectonic.lastData || [],
                    symbol: false,
                    symbolSize: 0,
                    barMaxWidth: '30px',
                    large: true,
                    stack: 'four',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#ff6666',
                        }
                    },
                }
        ]
        }

        const commonOptions = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            xAxis: [{
                type: 'category',
                data: ['2018/10/31', '2018/09/30', '2018/08/31', '2018/07/31', '2018/06/30', '2018/05/31', '2018/04/30', '2018/03/31', '2018/02/28'],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
        ],
            dataZoom: [
                {
                    type: '',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0
                }
            ],
            series: [
            ]
        }

        let lineOptions = Object.assign({}, commonOptions)
        lineOptions.xAxis[0].boundaryGap= false
        
        lineOptions.series = [{
            data: [400, 932, 901, 934, 1290, 1300, 1320, 900, 400],
            type: 'line',
            stack: '总量',
            smooth: true,
            itemStyle: {
                normal: {
                    color: 'rgba(255, 105, 105, 1)',
                }
            },
            areaStyle: {
                normal: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        offset: 0,
                        color: 'rgba(255, 105, 105, 0.6)'
                    }, {
                        offset: 1,
                        color: 'rgba(255, 105, 105, 0.02)'
                    }])
                }
            }
        }]


        window.lineOptions = lineOptions
        // 仓位总览
        stackOptions.tooltip.formatter = (value) => {
            let tips = `${value[0].name} <br />`
            value.forEach(item => {
                tips +=  `${item.marker}${item.seriesName.indexOf('series') >= 0 ? '前五大行业累计持仓' : item.seriesName}：${parseFloat(item.data).toFixed(2)}%<br />`
            })
            return tips 
        }
        
        this.refs.holderStack.setOptions(stackOptions)
        stackOptions.xAxis[0].data = resultObject.xDate
        stackOptions.yAxis[0].name = '权重（%）'
        stackOptions.series = [{
            type: 'bar',
            data: resultObject.valueData || [],
            barMaxWidth: '30px',
            large: true,
            lineStyle: {
                normal: {
                    shadowColor: 'rgba(0, 0, 0, 0.2)',
                    shadowOffsetY: 2,
                    shadowOffsetX: 2,
                    shadowBlur: 5
                }
            },
            itemStyle: {
                normal: {
                    color: '#fe8687',
                }
            },
        }]
        // 行业集中度
        this.refs.industryStack.setOptions(stackOptions)

        
        holderOptions.legend.data = futureHold.legend
        holderOptions.xAxis[0].data = futureHold.xDate
        holderOptions.yAxis[1].max = futureHold.max
        holderOptions.series[0].data = futureHold.barData
        holderOptions.series[1].data = futureHold.lineData
        this.refs.bonds2Charts.setOptions(holderOptions)


        gridOptions.xAxis.data = futureObject.xDate
        gridOptions.legend.data = futureObject.legend
        gridOptions.yAxis[0].max = 100
        gridOptions.legend.bottom = 0
        gridOptions.grid.bottom = 90
        gridOptions.series = [
            {
                name: futureObject.legend[0],
                type: 'bar',
                stack: 'six',
                barMaxWidth: '20px',
                itemStyle: {
                    normal: {
                        color: chartsColors[0]
                    }
                },
                data: futureObject.valueData1
            },
            {
                name: futureObject.legend[1],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color:  chartsColors[1]
                    }
                },
                data: futureObject.valueData2
            },
            {
                name: futureObject.legend[2],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color:  chartsColors[2]
                    }
                },
                data: futureObject.valueData3
            },
            {
                name:futureObject.legend[3],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color:  chartsColors[3]
                    }
                },
                data: futureObject.valueData4
            },
            {
                name: futureObject.legend[4],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color: chartsColors[4]
                    }
                },
                data: futureObject.valueData5
            },
            {
                name: futureObject.legend[5],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color: chartsColors[5]
                    }
                },
                data: futureObject.valueData6
            }
        ]
        this.refs.gride2Charts.setOptions(gridOptions)

        pieOptions.tooltip.formatter = (value) => {
            var toolTip = `${moment(value.data.getDate).format('YYYY-MM-DD')}<br />`;
            toolTip += "".concat(value.marker).concat(value.name, "\uFF1A").concat(value.value, "%");
            return toolTip;
          }
        pieOptions.series[0].data = pieData
        this.refs.bondsPie2Charts.setOptions(pieOptions)

        // this.refs.bondsPie3Charts.setOptions(pieOptions)

        let futureOptions = {
    
            tooltip : {
                trigger: 'axis',
                axisPointer : {            
                    type : 'shadow'
                },
                
                formatter: function (params) {
                    let items = `${futureBar.getDate ? moment(futureBar.getDate).format('YYYY-MM-DD') : moment(new Date()).format('YYYY-MM-DD')}<br />${params[0].name}`
                    const itemDesc = [`<span style='display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:14px; background: linear-gradient(to bottom, #fe5c5a 0%,#faa565 100%);'></span>`,
                                    `<span style='display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:14px; background: linear-gradient(to bottom, rgba(172, 237, 255, 1) 0%, rgba(14, 190, 247, 1) 100%);'></span>`]
                    params.forEach((item, index) => {
                        let desc = ''
                        if (item.seriesName.indexOf('空头') !== -1) {
                            desc = itemDesc[1]
                        } else {
                            desc = itemDesc[0]
                        }
                        items += `<br /> ${desc}${item.seriesName}：${Math.abs(item.value)}%`
                    })

                    return items
                }
            },
            legend: {
                data:['多头保证金比例', '空头保证金占比'],
                left: "center",
                icon: 'circle'
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '30'
            },
            xAxis : [
                {
                    interval: 1,
                    axisLabel: {
                        margin: 8,
                        color: '#999',
                        show: true,
                        textStyle: {
                            fontSize: 14
                        },
                    },
                    axisLine: {
                        lineStyle: {
                            width: 2,
                            color: '#0d9ef9'
                        }
                    },
                    axisTick: {
                        show: false
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            type: 'dashed',
                            color: '#0d9ef9'
                        }
                    },
                    type : 'category',
                    data : futureBar.legend
                }
            ],
            yAxis : [
                {
                    interval: 25,
                    max: 100,
                    min: -100,
                    axisLabel:{
                        show: false
                    },
                    axisLine:{
                        show: false
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            type: 'dashed',
                            color: '#0d9ef9'
                        }
                    },
                    type : 'value',
                    axisTick : {show: false}
                }
            ],
            series : []
        }
        
        futureOptions.series = [
            
            {
                name: '多头保证金比例',
                type: 'bar',
                data: futureBar.longBar,
                barWidth: '20px',
                lineStyle: {
                    normal: {
                        shadowColor: 'rgba(0, 0, 0, 0.2)',
                        shadowOffsetY: 2,
                        shadowOffsetX: 2,
                        shadowBlur: 5
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        color: '#999',
                        formatter: function(params){return Math.abs(params.value) + '%'}
                    }
                },
                itemStyle: {
                    normal: {
                        padding: 6,
                        shadowColor: '#fe5c5a',
                        shadowBlur: 6,
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: '#fe5c5a'
                        }, {
                            offset: 1,
                            color: '#faa565'
                        }]),
                        barBorderRadius: [20, 20, 20, 20]
                    }
                },
            },
            {
                name: '空头保证金占比',
                type: 'bar',
                data: futureBar.shortBar,
                barWidth: '20px',
                itemStyle: {
                    normal: {
                        top: 3,
                        shadowColor: 'rgba(14, 190, 247, 0.8)',
                        shadowBlur: 6,
                        color:  new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(172, 237, 255, 1)'
                        }, {
                            offset: 1,
                            color: 'rgba(14, 190, 247, 1)'
                        }]),
                        barBorderRadius: [20, 20, 20, 20],
                    }
                },
                label: {
                    normal: {
                        show: true,
                        position: 'bottom',
                        color: '#999',
                        padding: 6,
                        formatter: function(params){return Math.abs(params.value) + '%'}
                    }
                }
            }
        ]

        this.refs.futureCharts2.setOpitions(futureOptions)
    }

    renderTable = (chartsRef) =>{
        let dlTable = []
        for (let i  = 1; i < 13; i++) {
            dlTable.push(
            <dl key={`talbe-${i}`}>
                <dt>{`201${i}1231`}</dt>
                <dd>
                    <CommonCharts idName={`${chartsRef}${i}`} ref={`${chartsRef}${i}`} styles={{width: '204%', height: '100%'}}/>
                </dd>
            </dl>)
        }
        
        return dlTable
    }

    // 股票多投 仓位总览
    async checkDateRender (moment, str, ref, target) {
        if (target === 'start') {
            await this.setState({startDate: str})
        } else {
            await this.setState({endDate: str})
        }

        let dataPramas = {
            startDate: this.state.startDate,
            endDate:  this.state.endDate,
        }

        const data = await this._getHold(dataPramas.startDate, dataPramas.endDate) || { legend: [], xDate: [], lineData: [], barData: [], max: 3 }

        holderOptions.legend.data = data.legend
        holderOptions.xAxis[0].data = data.xDate
        holderOptions.yAxis[1].max = data.max
        holderOptions.yAxis[1].interval = data.max / 5

        holderOptions.series[0].data = data.barData
        holderOptions.series[1].data = data.lineData
        holderOptions.series[1].name = data.legend[0]

        this.refs[ref].setOptions(holderOptions)

    }
    // 股票多投，板块配置
    async checkStackRender (moment, str, ref, target) {
        if (target === 'start') {
            await this.setState({startDate: new Date(str).getTime() / 1000 })
        } else {
            await this.setState({endDate: new Date(str).getTime() / 1000})
        }

        let dataPramas = {
            startDate: this.state.startDate || '',
            endDate:  this.state.endDate || '',
        }

        const data = await this._tectonic(dataPramas.startDate, dataPramas.endDate) || { xDate: [], firstData: [], secData: [], threeData: [], lastData: [], legend: []}
        const stackOptions = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            legend: {
                icon: 'rect',
                data: data.legend || [],
                align: 'left',
                bottom: 0,
                color: '#eee',
                show: false
            },
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            xAxis: [{
                type: 'category',
                data: data.xDate || [],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    max: 100,
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
        ],
            dataZoom: [
                {
                    type: 'inside',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0,
                    start: data.xDate.length > 100 ?  90 : 0,
                    realtime: true
                },{
                    type: 'slider',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0,
                    start:  data.xDate.length > 100 ?  90 : 0,
                    realtime: true,
                    zoomLock: true
                }
            ],
            series: [
                {
                    name: data.legend[0]|| '',
                    type: 'bar',
                    data: data.firstData || [],
                    stack: 'four',
                    barMaxWidth: '30px',
                    large: true,
                    itemStyle: {
                        normal: {
                            color: '#55aee8',
                            barBorderRadius: [1, 1, 1, 1],
                        }
                    }
                },
                {
                    name: data.legend[1]|| '',
                    type: 'bar',
                    data: data.secData || [],
                    stack: 'four',
                    barMaxWidth: '30px',
                    large: true,
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fe8687',
                        }
                    },
                },
                {
                    name: data.legend[2]|| '',
                    type: 'bar',
                    data: data.threeData || [],
                    symbol: false,
                    large: true,
                    symbolSize: 0,
                    barMaxWidth: '30px',
                    stack: 'four',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#f6b78c',
                        }
                    },
                },
                {
                    name: data.legend[3]|| '',
                    type: 'bar',
                    data: data.lastData || [],
                    symbol: false,
                    symbolSize: 0,
                    barMaxWidth: '30px',
                    large: true,
                    stack: 'four',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#ff6666',
                        }
                    },
                }
        ]
        }

        this.refs[ref].setOptions(stackOptions)
    }

    // 股票多投， 行业集中度
    async checkIndustryRender (moment, str, ref, target) {
        if (target === 'start') {
            await this.setState({startDate: new Date(str).getTime() / 1000 })
        } else {
            await this.setState({endDate: new Date(str).getTime() / 1000})
        }

        let dataPramas = {
            startDate: this.state.startDate || '',
            endDate:  this.state.endDate || '',
        }

        const data = await this._concentration(dataPramas.startDate, dataPramas.endDate) || {xDate: [], valueData: []}

        const stackOptions = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            legend: {
                icon: 'rect',
                data: [],
                align: 'left',
                bottom: 0,
                color: '#eee',
                show: false
            },
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            xAxis: [{
                type: 'category',
                data: data.xDate || [],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    max: 100,
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
        ],
            dataZoom: [
                {
                    type: 'inside',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0,
                    start: data.xDate.length > 100 ?  90 : 0,
                    realtime: true
                },{
                    type: 'slider',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0,
                    start: data.xDate.length > 100 ?  90 : 0,
                    realtime: true,
                    zoomLock: true
                }
            ],
            series: [{
                type: 'bar',
                data: data.valueData || [],
                barMaxWidth: '30px',
                large: true,
                lineStyle: {
                    normal: {
                        shadowColor: 'rgba(0, 0, 0, 0.2)',
                        shadowOffsetY: 2,
                        shadowOffsetX: 2,
                        shadowBlur: 5
                    }
                },
                itemStyle: {
                    normal: {
                        color: '#fe8687',
                    }
                },
            }]
        }

        this.refs[ref].setOptions(stackOptions)
    }

    // 期货，仓位总览
    async checkBondsRender (moment, str, ref, target) {
        if (target === 'start') {
            await this.setState({startDate: str })
        } else {
            await this.setState({endDate: str})
        }

        let dataPramas = {
            startDate: this.state.startDate || '',
            endDate:  this.state.endDate || '',
        }

        const data = await this._futuresHold(dataPramas.startDate, dataPramas.endDate) || { legend: [], xDate: [], lineData: [], barData: [], max: 3 }

        holderOptions.legend.data = data.legend
        holderOptions.xAxis[0].data = data.xDate
        holderOptions.yAxis[1].max = data.max
        holderOptions.yAxis[1].interval = data.max / 5

        holderOptions.series[0].data = data.barData
        holderOptions.series[1].data = data.lineData
        holderOptions.series[1].name = data.legend[0]

        this.refs[ref].setOptions(holderOptions)

    }

    async checkGrideRender (moment, str, ref, target) {
        if (target === 'start') {
            await this.setState({startDate: new Date(str).getTime() / 1000 })
        } else {
            await this.setState({endDate: new Date(str).getTime() / 1000})
        }

        let dataPramas = {
            startDate: this.state.startDate || '',
            endDate:  this.state.endDate || '',
        }

        const data = await this._futuresConcentration(dataPramas.startDate, dataPramas.endDate) || {xDate: [], legend: [], valueData1: [], valueData2: [], valueData3: [], valueData4: [], valueData5: [], valueData6: []}

        gridOptions.xAxis.data = data.xDate
        gridOptions.legend.data = data.legend
        gridOptions.yAxis[0].max = 100
        gridOptions.legend.bottom = 0
        gridOptions.grid.bottom = 90
        gridOptions.series = [
            {
                name: data.legend[0],
                type: 'bar',
                stack: 'six',
                barMaxWidth: '20px',
                itemStyle: {
                    normal: {
                        color: chartsColors[0]
                    }
                },
                data: data.valueData1
            },
            {
                name: data.legend[1],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color:  chartsColors[1]
                    }
                },
                data: data.valueData2
            },
            {
                name: data.legend[2],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color:  chartsColors[2]
                    }
                },
                data: data.valueData3
            },
            {
                name:data.legend[3],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color:  chartsColors[3]
                    }
                },
                data: data.valueData4
            },
            {
                name: data.legend[4],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color: chartsColors[4]
                    }
                },
                data: data.valueData5
            },
            {
                name: data.legend[5],
                type: 'bar',
                barMaxWidth: '20px',
                stack: 'six',
                itemStyle: {
                    normal: {
                        color: chartsColors[5]
                    }
                },
                data: data.valueData6
            }
        ]

        this.refs[ref].setOptions(gridOptions)
    }

    async checkDoubleChart (moment, str, ref, target) {
        const checkDate = str ? new Date(str).getTime() / 1000 : ''
        const pieData = await this._futuresPie(checkDate) || [{name: '', value: ''}]
        const lineData = await this._futuresBar(checkDate) || {legend: [], longBar: [], shortBar: []}
        let futureOptions = {
    
            tooltip : {
                trigger: 'axis',
                axisPointer : {            
                    type : 'shadow'
                },
                
                formatter: (params) => {
                    let items = `${params[0].name}`
                    const itemDesc = [`<span style='display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:14px; background: linear-gradient(to bottom, #fe5c5a 0%,#faa565 100%);'></span>`,
                                    `<span style='display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:14px; background: linear-gradient(to bottom, rgba(172, 237, 255, 1) 0%, rgba(14, 190, 247, 1) 100%);'></span>`]
                    params.forEach((item, index) => {
                        let desc = ''
                        if (item.seriesName.indexOf('空头') !== -1) {
                            desc = itemDesc[1]
                        } else {
                            desc = itemDesc[0]
                        }
                        items += `<br /> ${desc}${item.seriesName}：${Math.abs(item.value)}%`
                    })

                    return items
                }
            },
            legend: {
                data:['多头保证金比例', '空头保证金占比'],
                left: "center",
                icon: 'circle'
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '30'
            },
            xAxis : [
                {
                    interval: 1,
                    axisLabel: {
                        margin: 8,
                        color: '#999',
                        show: true,
                        textStyle: {
                            fontSize: 14
                        },
                    },
                    axisLine: {
                        lineStyle: {
                            width: 2,
                            color: '#0d9ef9'
                        }
                    },
                    axisTick: {
                        show: false
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            type: 'dashed',
                            color: '#0d9ef9'
                        }
                    },
                    type : 'category',
                    data : lineData.legend
                }
            ],
            yAxis : [
                {
                    interval: 25,
                    max: 100,
                    min: -100,
                    axisLabel:{
                        show: false
                    },
                    axisLine:{
                        show: false
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            type: 'dashed',
                            color: '#0d9ef9'
                        }
                    },
                    type : 'value',
                    axisTick : {show: false}
                }
            ],
            series : [
                {
                    name: '多头保证金比例',
                    type: 'bar',
                    data: lineData.longBar,
                    barWidth: '20px',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            color: '#999',
                            formatter: function(params){return Math.abs(params.value) + '%'}
                        }
                    },
                    itemStyle: {
                        normal: {
                            padding: 6,
                            shadowColor: '#fe5c5a',
                            shadowBlur: 6,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: '#fe5c5a'
                            }, {
                                offset: 1,
                                color: '#faa565'
                            }]),
                            barBorderRadius: [20, 20, 20, 20]
                        }
                    },
                },
                {
                    name: '空头保证金占比',
                    type: 'bar',
                    data: lineData.shortBar,
                    barWidth: '20px',
                    itemStyle: {
                        normal: {
                            top: 3,
                            shadowColor: 'rgba(14, 190, 247, 0.8)',
                            shadowBlur: 6,
                            color:  new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                                offset: 0,
                                color: 'rgba(172, 237, 255, 1)'
                            }, {
                                offset: 1,
                                color: 'rgba(14, 190, 247, 1)'
                            }]),
                            barBorderRadius: [20, 20, 20, 20],
                        }
                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'bottom',
                            color: '#999',
                            padding: 6,
                            formatter: function(params){return Math.abs(params.value) + '%'}
                        }
                    }
                }
            ]
        }

        pieOptions.series[0].data = pieData
        this.refs.bondsPie2Charts.setOptions(pieOptions)
        // console.log(futureOptions)
        this.refs.futureCharts2.setOpitions(futureOptions)
    }

    _disableDate (current) {
        return current.day() !== 5
    }

    exportPDF () {
        // console.log(window.lineOptions)
        axios({
            method: 'POST',
            url: '/api/hszcpz/index/exportPDF',
            data: JSON.stringify({token: cookie.get('token'), optionJson: [window.lineOptions, TableChartsConfig]})
        })
        .then(res => {
            // console.log(res)
            if (res.status === 'success') {
                let data = JSON.parse(res.result)
                // console.log(data)
                if (data.result === 'success') {
                    window.location.href = `http://${data.url}?token=${cookie.get('token')}`
                } else {
                    message.error('')
                }
            }
        })
    }

    render () {
        return (
        <div>
            <div className={ProductStyle.holder_wrapper}>
                <div className={ProductStyle.holder_btn_dec}><span className={'iconfont iconqushitu'}></span>股票多头</div>
                <WrapperHeader title='仓位总览' fn={this.exportPDF} iconStatus={true}>
                    <div className='float_right product_select_date'>
                        <DatePicker locale={locale} onChange={(moment, str) => { this.checkDateRender(moment, str,'holderBar', 'start')}} placeholder={'开始日期'} />
                        <strong>~</strong>
                        <DatePicker locale={locale} onChange={(moment, str) => { this.checkDateRender(moment, str,'holderBar', 'end')}} placeholder={'结束日期'} />
                    </div>
                </WrapperHeader>
                <CommonBar idName='holderBarLine' ref='holderBar' />

                <div className={`mt_40`}>
                    <WrapperHeader title='板块配置' iconStatus={true}>
                        <div className='float_right product_select_date'>
                            <DatePicker locale={locale} onChange={(moment, str) => { this.checkStackRender(moment, str,'holderStack', 'start')}} placeholder={'开始日期'} />
                            <strong>~</strong>
                            <DatePicker locale={locale} onChange={(moment, str) => { this.checkStackRender(moment, str,'holderStack', 'end')}}  placeholder={'结束日期'} />
                        </div>
                    </WrapperHeader>
                    <CommonBar idName='holderBarStack' ref='holderStack' />
                </div>

                <div className={`mt_40`}>
                    <WrapperHeader title='行业集中度' iconStatus={true}>
                        <div className='float_right product_select_date'>
                            <DatePicker onChange={(moment, str) => { this.checkIndustryRender(moment, str,'industryStack', 'start')}} locale={locale} placeholder={'开始日期'} />
                            <strong>~</strong>
                            <DatePicker onChange={(moment, str) => { this.checkIndustryRender(moment, str,'industryStack', 'end')}} locale={locale}  placeholder={'结束日期'} />
                        </div>
                    </WrapperHeader>
                    <CommonBar idName='industryBarStack' ref='industryStack' />
                </div>

            </div>

            <div className={`${ProductStyle.holder_wrapper} mt_20`}>
                <div className={ProductStyle.holder_btn_dec}><span className={'iconfont iconqihuo'}></span>期货</div>
                <WrapperHeader title='仓位总览' iconStatus={true}>
                    <div className='float_right product_select_date'>
                        <DatePicker locale={locale} onChange={(moment, str) => { this.checkBondsRender(moment, str,'bonds2Charts', 'start')}} placeholder={'开始日期'} />
                        <strong>~</strong>
                        <DatePicker locale={locale} onChange={(moment, str) => { this.checkBondsRender(moment, str,'bonds2Charts', 'end')}} placeholder={'结束日期'} />
                    </div>
                </WrapperHeader>
                <CommonBar idName='bonds2Bars' ref='bonds2Charts' />
                <div className={`mt_40`}>
                    <div className={`${ProductStyle.bonds_contents} clear-fix`}>
                        <WrapperHeader title='板块配置' iconStatus={true}>
                            <div className='float_right product_select_date'>
                                <DatePicker onChange={(moment, str) => { this.checkGrideRender(moment, str,'gride2Charts', 'start')}} locale={locale} placeholder={'开始日期'} />
                                <strong>~</strong>
                                <DatePicker onChange={(moment, str) => { this.checkGrideRender(moment, str,'gride2Charts', 'end')}} locale={locale}  placeholder={'结束日期'} />
                            </div>
                        </WrapperHeader>
                        <CommonBar idName='grid2Charts' ref='gride2Charts' />

                        <div className={`mt_40`}>
                            <WrapperHeader title='资产配比' iconStatus={true}>
                                <div className='float_right product_select_date'>
                                    <span className='date_select_label'>统计日期</span>
                                    <DatePicker
                                    showToday={false}
                                    disabledDate={this._disableDate.bind(this)}
                                     onChange={(moment, str) => { this.checkDoubleChart(moment, str)}} locale={locale} label={'riqi'} />
                                </div>
                            </WrapperHeader>
                            <div className={`float_left ${ProductStyle.bonds_left}`}>
                                <div className='mt_10'>
                                    <CommonBar idName='bondsPie2Charts' ref='bondsPie2Charts' />
                                </div>
                            </div>
                            
                            <div className={`float_right ${ProductStyle.bonds_right}`}>
                                
                                <div className='mt_10'>  
                                    <CommonChart idName={'futureCharts2'} ref={'futureCharts2'} styles={{width: '100%', height: '600px'}} />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className={`mt_40`}></div>
            </div>
        </div>
        )
    }
}