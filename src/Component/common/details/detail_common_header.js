import React, { Component } from 'react'
import { Button, Icon, DatePicker, Select, Cascader } from 'antd'

const { Option } = Select

let options = [
    {
      value: '综合',
      label: '综合',
      children: [
        {
          value: '沪深300',
          label: '沪深300',
        },
        {
            value: '中证500',
            label: '中证500',
        },
        {
            value: '上证指数',
            label: '上证指数',
        },
      ],
    },
    {
      value: '朝阳永续',
      label: '朝阳永续',
      children: [
        {
          value: '股票多头',
          label: '股票多头',
        },
      ],
    },
  ]

export default class CommonHeaders extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            navType: 'n_added_nav'
        }
    }

    displayRender (label) {
        return label[label.length - 1]
    }

    valueChange (value) {
        // console.log(value)
    }

    async UNSAFE_componentWillMount () {
        if (this.props.hasOwnProperty('_cascaderTags')) {
            const menuList = await this.props._cascaderTags()
            // console.log(menuList)
            options = menuList
        }
    }

    async componentDidMount () {
        // console.log(this.props)
    }

    render () {
        
        return (
        <div className={'product_common_header clear-fix'}>
            <div className={'float_left'}>
                <div className={'float_left left_common_header_content'}>
                    <span>统计周期：</span>
                    <Select style={{width: '140px'}} placeholder='今年以来'>
                        <Option value="year">今年以来</Option>
                        <Option value="m1">近一个月</Option>
                        <Option value="m3">近三月</Option>
                        <Option value="m6">近六月</Option>
                        <Option value="y1">近一年</Option>
                        <Option value="y2">近二年</Option>
                        <Option value="y3">近三年</Option>
                        <Option value="y5">近五年</Option>
                        <Option value="total">成立以来</Option>
                    </Select>
                </div>
                <div className={'float_left left_common_header_content ml_10'}>
                    <DatePicker className={'product_common_date'} placeholder={'开始日期'} />
                    <strong className={'header_content_split'}>~</strong>
                    <DatePicker className={'product_common_date'} placeholder={'结束日期'} />
                </div>
                
                <div className={'float_left left_common_header_content ml_10'}>
                    <span>净值类型：</span>
                    <Select value={this.state.navType} style={{width: '130px'}} placeholder='复权累计净值'>
                        <Option value='n_added_nav'>复权累计净值</Option>
                        <Option value='n_nav'>单位净值</Option>
                        <Option value='n_swanav'>累计净值</Option> 
                    </Select>
                </div>

                <div id='cascader_container' className={'float_left left_common_header_content ml_10'}>
                    <span>对比指标：</span>
                    <Cascader onChange={this.valueChange.bind(this)} displayRender={this.displayRender.bind(this)} getPopupContainer={() => document.getElementById('cascader_container')} className={`select_cascader_wrapper`} options={options} placeholder={'请选择比较基准'} expandTrigger="hover" />
                </div>

                <div className={'float_left left_common_header_content ml_10'}>
                    <Button type='ghost' onClick={this.props._addModal}><Icon type='plus' className={'product_common_header_plus'} />{this.props._type === 'advisor' ? '添加对比投顾': '添加对比产品'}</Button>
                </div>
                <div className={'float_left left_common_header_content ml_10'}>
                    <Button type='danger'>重置</Button>
                </div>
                
            </div>
            <div className={'float_right'}>
               
                {/* <Button type='danger'>导出</Button> */}
            </div>
        </div>)
    }
}