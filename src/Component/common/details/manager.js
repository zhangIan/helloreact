import React, { Component } from 'react'

import { Table } from 'antd'

const columns = [
    {
        key: '1',
        dataIndex: 'name',
        title: '基金经理'
    },
    {
        key: '2',
        dataIndex: 'time',
        title: '任职时间'
    },
    {
        key: '3',
        dataIndex: 'position',
        title: '职务'
    },
    {
        key: '4',
        dataIndex: 'number',
        title: '管理基金数'
    },
    {
        key: '5',
        dataIndex: 'college',
        title: '毕业院校'
    },
    {
        key: '6',
        dataIndex: 'education',
        title: '学历'
    },
    {
        key: '7',
        dataIndex: 'year',
        title: '投资年限'
    }
]

export default class ManagerList extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    render () {
        return (
            <Table dataSource={[]} columns={columns} />
        )
    }
}