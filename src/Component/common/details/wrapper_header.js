import React, { Component } from 'react'

export default class WrapperHeader extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }
    exportFn () {
        if (this.props.fn) {
            this.props.fn()
        } else {
            console.log(111)
        }
    }
    render () {
        const { title, iconStatus } = this.props
        
        const showIcon = iconStatus ? true : false

        return <div className='clear-fix charts_nav_header'>
        <div className='float_left'>
            <strong className={`charts_title chart_item_title`}>{title}</strong>
        </div>
        
        <div className='float_right'>
            {showIcon ? '' :<span onClick={this.exportFn.bind(this)} className='iconfont echarts_exports iconCombinedShape'></span>}
        </div>
        {this.props.children}
    </div>
    }
}