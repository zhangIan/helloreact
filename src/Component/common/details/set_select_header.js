import React from 'react'

const setSelectHeader = (title) => {
    return (
        <div className='clear-fix charts_nav_header'>
            <div className='float_left'>
                <strong className={`charts_title chart_item_title`}>{title}</strong>
            </div>
            <div className='float_right'>
                <span className='iconfont echarts_exports iconCombinedShape'></span>
            </div>
        </div>
    )
}

export default setSelectHeader