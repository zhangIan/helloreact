import  React, { Component } from 'react'
import { Row, Col, Tag, Button, Icon } from 'antd'
import ProductStyle from '../../product/product.module.sass'
import moment from 'moment'


export default class Detail extends Component {
    constructor (...props) {
        super(...props)

        this.state = {}
    }

    async componentDidMount () {
        const data = await this.props.productInfo()
        if (data) {
            this.setState({productInfo: data})
        }
    }

    tags (items) {
        const tagsList = []

        if (items) {
            items.forEach((item, index) => {
                tagsList.push(<Tag className={ProductStyle.border_tags}key={index}>{item}</Tag>)
            })
        }
        return tagsList
    }

    fundStar (star) {
        if (star === null || star === undefined) {
            return <em className='color_666'>--</em>
        }
        const starList = [
        <em key='1' className='iconfont iconxingxing color_grey'></em>,
        <em key='2' className='iconfont iconxingxing color_grey'></em>,
        <em key='3' className='iconfont iconxingxing color_grey'></em>,
        <em key='4' className='iconfont iconxingxing color_grey'></em>,
        <em key='5' className='iconfont iconxingxing color_grey'></em>]
        const stars = parseInt(star)
        if (stars >= 0) {
            for(let i = 0; i < stars; i++) {
                starList[i] = <em key={i} className='iconfont iconxingxing color_yellow'></em>
            }
        }
        return starList
    }

    fundManager (manager) {
        let result = []
        if (manager && typeof manager === 'object') {
           const keys = Object.keys(manager)
           keys.forEach((item, index) => {
               if (index === keys.length-1) {
                result.push(<a key={index} href={`/manager?id=${item}`} className={`color_red`}>{manager[item]}</a>)
               } else {
                result.push(<a key={index} href={`/manager?id=${item}`} className={`color_red`}>{manager[item]}、</a>)
               }
           })
        } else {
            result = `--`
        }

        return result
    }

    fundAdvisor (manager) {
        let result = []
        if (manager && typeof manager === 'object') {
           const keys = Object.keys(manager)
           keys.forEach((item, index) => {
               if (index === keys.length-1) {
                result.push(<a key={index} href={`/advisor?id=${item}`} className={`color_red`}>{manager[item]}</a>)
               } else {
                result.push(<a key={index} href={`/advisor?id=${item}`} className={`color_red`}>{manager[item]}、</a>)
               }
           })
        } else {
            result = `--`
        }

        return result
    }

    _fundCheck () {
        let permission = null
        let productPermission = {
            downLoad: true
        }

        try {
            permission = JSON.parse(window.localStorage.getItem('permission'))
        } catch (e) {
            // console.log(e)
        }

        if (permission) {
            permission.forEach(item => {
                if (item.perssion_code === '/sys/products/home/anticon-download') {
                    productPermission.downLoad = false
                }
            })
        }

        return productPermission
    }

    render () {
        let infoDetails = {}
        let Premission = this._fundCheck()
        if (this.state.productInfo) {
            infoDetails = this.state.productInfo
        }

        return (
            <div className={ProductStyle.product_info_wrapper}>
                <Row type='flex' className={ProductStyle.product_info_name}>
                    <Col span={7} className={ProductStyle.product_info_left}>
                        <div className={ProductStyle.info_top}>
                            <h2 className={ProductStyle.product_name} data-status={`[${infoDetails['fund_status'] ? infoDetails['fund_status'] === '运行中' ? '存续中' : '已终止' : '--'}]`}>{infoDetails['fund_name'] ? infoDetails['fund_name'] : '--'}</h2>
                            <div className={ProductStyle.product_cat}><span>策略分类：</span><em>{infoDetails['strategy_type'] ? infoDetails['strategy_type'] : '--'}</em></div>
                        </div>
                        <div className={`${ProductStyle.product_info_bottom} clear-fix`}>
                            <div className={`${ProductStyle.info_tags} ${ProductStyle.product_info_tags}`}>
                                {this.tags(infoDetails.proLable)}
                                {/* <Tag className={ProductStyle.border_tags}>上升市道</Tag>
                                <Tag className={ProductStyle.border_tags}>量化</Tag> */}
                            </div>
                            <div className={ProductStyle.info_stars}>
                                <span>产品星级：</span>
                                {this.fundStar(infoDetails.fund_star)}
                            </div>
                        </div>
                    </Col>
                    <Col span={9} className={ProductStyle.middle_line}>
                        <ul className={`${ProductStyle.middle_infos} clear-fix`}>
                            <li>
                                <div><span>成立时间：</span><em>{infoDetails['fund_foundation_date'] ? moment(infoDetails['fund_foundation_date']).format('YYYY-MM-DD') : '--'}{`${infoDetails['dateByNow'] ? '[' + infoDetails['dateByNow'] + ']' : '--'}`}</em></div>
                                <div><span>基金经理：</span><em className='color_red'>{infoDetails['fund_manager'] ? this.fundManager(infoDetails['fund_manager']) : '--'}</em></div>
                            </li>
                            <li>
                                <div><span>备案编号：</span><em>{infoDetails['reg_code'] ? infoDetails['reg_code'] : '--'}</em></div>
                                <div><span>投资顾问：</span><em className='color_red'>{infoDetails['proAdviser'] ? this.fundAdvisor(infoDetails['proAdviser']) : '--'}</em></div>
                            </li>
                            <li>
                                <div><span>基金管理人：</span><em className='color_red'>{infoDetails['fund_manager_org'] ? infoDetails['fund_manager_org'].map((element, index) => (index + 1) <  infoDetails['fund_manager_org'].length ? element + '、': element) : '--'}</em></div>
                                <div><span>办公地点：</span><em>{infoDetails['work_city'] ? infoDetails['work_city'] : '--'}</em></div>
                            </li>
                        </ul>
                    </Col>
                    <Col span={8} className={`${ProductStyle.middle_line} ${ProductStyle.right_buttons}`}>
                        <div className={`float_right`}>
                        {/* <Button type='danger'>已关注</Button> */}

                        <Button type='danger' onClick={this.props._report} disabled={Premission.downLoad}>导出报告<Icon type="caret-down" /></Button>
                        </div>
                    </Col>
                </Row>


                <Row type='flex' className={ProductStyle.product_info_price}>
                    
                    <Col span={6} className={ProductStyle.product_info_left_bottom}>
                        <div className={`${ProductStyle.product_nav_value}`}><strong className={`${ProductStyle.fund_nav_tit}`}>单位净值{infoDetails['new_nav_date'] ? `（${moment(infoDetails['new_nav_date']).format('YYYY-MM-DD')}）` : '--'}</strong><em className={`${ProductStyle.fund_rate_value}`}>{infoDetails['fund_nav'] ? parseFloat(infoDetails['fund_nav']).toFixed(4) : '--'}</em></div>
                        {/* <ul className={`${ProductStyle.info_bottom_list} clear-fix`}>
                            <li>
                                <span>净值</span>
                                <strong>{infoDetails['fund_nav'] ? parseFloat(infoDetails['fund_nav']).toFixed(4) : '--'}</strong>
                            </li>
                            <li>
                                <span>累计净值：</span>
                                <em>{infoDetails['fund_added_nav'] ? parseFloat(infoDetails['fund_added_nav']).toFixed(4) : '--'}</em>
                            </li>
                        </ul>

                        <ul className={`${ProductStyle.info_items} clear-fix`}>
                            <li className={ProductStyle.info_date}>
                                <span>净值日期：</span>
                                <strong>{infoDetails['new_nav_date'] ? moment(infoDetails['new_nav_date']).format('YYYY-MM-DD') : '--'}</strong>
                            </li>
                            <li>
                                <span>复权累计净值：</span>
                                <em>{infoDetails['fund_swanav'] ? parseFloat(infoDetails['fund_swanav']).toFixed(4) : '--'}</em>
                            </li>
                        </ul> */}
                    </Col>

                    <Col span={8} className={`${ProductStyle.middle_line_bottom}`}>
                        <div className={`float_left`}><strong className={`${ProductStyle.fund_nav_tit}`}>累计收益率&nbsp;</strong><em className={`${ProductStyle.fund_rate_value}`}>{infoDetails['cumulativeReturn'] ? parseFloat(infoDetails['cumulativeReturn']).toFixed(2) + '%' : '--'}</em></div>
                        <div className={`float_right`}><strong className={`${ProductStyle.fund_nav_tit}`}>累计年化收益率&nbsp;</strong><em className={`${ProductStyle.fund_rate_value}`}>{infoDetails['cumulativeReturnYear'] ? parseFloat(infoDetails['cumulativeReturnYear']).toFixed(2) + '%' : '--'}</em></div>
                        {/* <dl className={ProductStyle.bottom_middle_items}>
                            <dt>收益率</dt>
                            <dd>
                                <Row>
                                    <Col span={8}>
                                        <span>{infoDetails['proReturnM'] ? parseFloat(infoDetails['proReturnM']).toFixed(3) + '%' : '--'}</span>
                                        <strong>近3个月</strong>
                                    </Col>
                                    <Col span={8}>
                                        <span>{infoDetails['proReturnYear'] ? parseFloat(infoDetails['proReturnYear']).toFixed(3) + '%' : '--'}</span>
                                        <strong>今年以来</strong>
                                    </Col>
                                    <Col span={8}>
                                        <span>{infoDetails['proReturnY'] ? parseFloat(infoDetails['proReturnY']).toFixed(3) + '%' : '--'}</span>
                                        <strong>一年以来</strong>
                                    </Col>
                                </Row>
                            </dd>
                        </dl> */}
                    </Col>

                    <Col span={10} className={`${ProductStyle.middle_line_bottom}`}>
                        <div className={`float_left`}><strong className={`${ProductStyle.fund_nav_tit}`}>最大回撤（成立以来）</strong><em className={`${ProductStyle.fund_rate_value}`}>{infoDetails['maxDrawdown'] ? parseFloat(infoDetails['maxDrawdown']).toFixed(2) + '%' : '--'}</em></div>
                        <div className={`float_right`}><strong className={`${ProductStyle.fund_nav_tit}`}>Sharpe比率（成立以来）</strong><em className={`${ProductStyle.fund_rate_value}`}>{infoDetails['sharpeA'] ? parseFloat(infoDetails['sharpeA']).toFixed(2) : '--'}</em></div>
                        {/* <dl className={ProductStyle.bottom_middle_items}>
                            <dt>成立以来</dt>
                            <dd>
                                <Row type='flex' justify='space-around'>
                                    <Col span={8}>
                                        <span className='color-666'>{infoDetails['maxDrawdown'] ? parseFloat(infoDetails['maxDrawdown']).toFixed(2) + '%' : '--'}</span>
                                        <strong>最大回撤</strong>
                                    </Col>
                                    <Col span={8}>
                                        <span className='color-666'>{infoDetails['sharpeA'] ? parseFloat(infoDetails['sharpeA']).toFixed(2) : '--'}</span>
                                        <strong>Sharpe比率</strong>
                                    </Col>
                                </Row>
                            </dd>
                        </dl> */}
                    </Col>
                </Row>

            </div>
        )
    }
}
