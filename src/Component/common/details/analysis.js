import React, { Component } from 'react'
import ProductStyle from '../../product/product.module.sass'
import WrapperHeader from './wrapper_header'
import CommonBar from '../../common/charts/common_bar'
import echarts from 'echarts/lib/echarts'
import CommonChart from '../../common/charts/common_charts'
import locale from 'antd/es/date-picker/locale/zh_CN'
// 导入公共的表格样式
import { chartsOptions } from '../../../Utils/charts_config/charts_config'
import TableChartsConfig from '../../../Utils/charts_config/table_chart'
import { axios } from '../../../Utils/index'
import cookie from 'js-cookie'
import { message, DatePicker } from 'antd'

import ReportTable from '../common_table/report_table'
import CommonCharts from '../../common/charts/common_charts'

export default class Analysis extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    async componentDidMount () {
        const holderOptions = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            xAxis: [{
                type: 'category',
                data: ['2018-10', '2018-09', '2018-08', '2018-07', '2018-06', '2018-05', '2018-04', '2018-03', '2018-02'],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '仓位(%)',
                    type: 'value',
                    nameLocation: 'end',
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                },
                {
                    name: '复权累计净值',
                    nameLocation: 'end',
                    type: 'value',
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
        ],
            dataZoom: [
                {
                    type: '',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0
                }
            ],
            series: [
                {
                    type: 'bar',
                    data: [0.19, 0.24, 0.13, 0.28, 0.11, 0.26, 0.22, 0.41, 0.28],
                    barWidth: '30px',
                    itemStyle: {
                        normal: {
                            color: '#fea062',
                            barBorderRadius: [1, 1, 1, 1],
                        }
                    }
                },
                {
                    type: 'line',
                    data: [8, 3, 9, 10, 6, 7, 5, 3, 1],
                    symbol: false,
                    symbolSize: 0,
                    barWidth: '30px',
                    yAxisIndex: 1,
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#ff2b2b',
                        }
                    },
                },
                {
                    type: 'line',
                    data: [2, 5, 4, 7, 9, 10, 6, 7, 5],
                    symbol: false,
                    symbolSize: 0,
                    barWidth: '30px',
                    yAxisIndex: 1,
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#1890ff',
                        }
                    },
                }
        ]
        }

        

        const stackOptions = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            xAxis: [{
                type: 'category',
                data: ['2018/10/31', '2018/09/30', '2018/08/31', '2018/07/31', '2018/06/30', '2018/05/31', '2018/04/30', '2018/03/31', '2018/02/28'],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
        ],
            dataZoom: [
                {
                    type: '',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0
                }
            ],
            series: [
                {
                    type: 'bar',
                    data: [9, 5, 7, 3, 6, 4, 7, 4, 8],
                    barWidth: '30px',
                    stack: 'three',
                    itemStyle: {
                        normal: {
                            color: '#55aee8',
                            barBorderRadius: [1, 1, 1, 1],
                        }
                    }
                },
                {
                    type: 'bar',
                    data: [8, 3, 9, 10, 6, 7, 5, 3, 1],
                    stack: 'three',
                    barWidth: '30px',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fe8687',
                        }
                    },
                },
                {
                    type: 'bar',
                    data: [2, 5, 4, 7, 9, 10, 6, 7, 5],
                    symbol: false,
                    symbolSize: 0,
                    stack: 'three',
                    barWidth: '30px',
                    lineStyle: {
                        normal: {
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#f6b78c',
                        }
                    },
                }
        ]
        }

        
        const industryOptions = {
            legend: {
                data: ['2017', '2018', '2019'],
                align: 'left',
                bottom: 0,
                color: '#eee'
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: 0
                }
            },
            xAxis: [{
                type: 'category',
                data: ['机械设备', '化工', '医药生物', '电子', '计算机', '电器设备', '有色金属', '传媒', '公用事业', '汽车', '建筑装饰', '轻工制造'],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
            ],
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            series: [
                
                {
                    name: '2017',
                    type: 'bar',
                    barGap: 0,
                    barWidth: '14px',
                    itemStyle: {
                        normal: {
                            color: '#55aee8',
                        }
                    },
                    data: [300, 200, 700, 400, 600, 800, 500, 200, 169, 876, 934,1032]
                },
                {
                    name: '2018',
                    type: 'bar',
                    barWidth: '14px',
                    itemStyle: {
                        normal: {
                            color: '#ff908f',
                        }
                    },
                    data: [300, 200, 700, 400, 600, 800, 500, 200, 169, 231, 398, 471]
                },
                {
                    name: '2019',
                    type: 'bar',
                    barWidth: '14px',
                    itemStyle: {
                        normal: {
                            color: '#ffbb8e',
                        }
                    },
                    data: [300, 200, 700, 400, 600, 800, 500, 200, 169, 190, 900, 470]
                }
            ]
        }

        const commonOptions = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 60,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            grid: {
                top: '50px',
                right: '46px',
                left: '46px',
                bottom: 68
            },
            xAxis: [{
                type: 'category',
                data: ['2018/10/31', '2018/09/30', '2018/08/31', '2018/07/31', '2018/06/30', '2018/05/31', '2018/04/30', '2018/03/31', '2018/02/28'],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#ccc'
                    }
                },
                axisLabel: {
                    margin: 10,
                    color: '#999',
                    show: true,
                    textStyle: {
                        fontSize: 14
                    },
                },
            }],
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
        ],
            dataZoom: [
                {
                    type: '',
                    height: 30,
                    left: '70px',
                    right: '70px',
                    bottom: 0
                }
            ],
            series: [
            ]
        }

        let lineOptions = Object.assign({}, commonOptions)
        lineOptions.xAxis[0].boundaryGap= false

        lineOptions.series = [{
            data: [400, 932, 901, 934, 1290, 1300, 1320, 900, 400],
            type: 'line',
            stack: '总量',
            smooth: true,
            itemStyle: {
                normal: {
                    color: 'rgba(255, 105, 105, 1)',
                }
            },
            areaStyle: {
                normal: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                        offset: 0,
                        color: 'rgba(255, 105, 105, 0.6)'
                    }, {
                        offset: 1,
                        color: 'rgba(255, 105, 105, 0.02)'
                    }])
                }
            }
        }]

        let gridOptions = {
            legend: {
                icon: 'rect',
                data: ['金属', '农产品', '能化', '黑色系', '金融', '贵金属'],
                align: 'left',
                bottom: 0,
                color: '#eee'
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line',
                    lineStyle: {
                        width: 26,
                        color: 'rgba(0, 0, 0, 0.1)'
                    },
                    z: -1
                }
            },
            grid: {
                show: true,
                borderwidth: 1,
                borderColor: '#ccc',
                top: '20px',
                right: '10px',
                left: '50px',
                bottom: 80
            },
            xAxis: {
                data: ['20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430','20190430'],
                axisLabel: {
                    rotate: 45,
                    textStyle: {
                        color: '#999'
                    }
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                },
                z: 10
            },
            yAxis: [
                {
                    name: '',
                    nameLocation: 'end',
                    nameTextStyle: {
                        color: '#999',
                        align: 'center'
                    },
                    color: '#ccc',
                    axisLabel: {
                        show: true,
                        typeStyle: {
                            color: '#333'
                        },
                        color: '#999',
                        formatter: '{value} '
                    },
                    axisLine: {
                        show: false,
                        lineStyle: {
                            color: '#ccc'
                        }
                    },
                    splitNumber: 6,
                    splitLine: {
                        lineStyle: {
                            type: 'dotted',
                            color: '#ccc'
                        }
                    }
                }
            ],
            series: [
                {
                    name:'金属',
                    type: 'bar',
                    stack: 'five',
                    barWidth: '20px',
                    itemStyle: {
                        normal: {
                            color: '#7ed0e8'
                        }
                    },
                    data: [0.3, 0.4, 0.5, 0.1, 0.3, 0.4, 0.5, 0.1, 0.3, 0.4, 0.5, 0.1, 0.3, 0.4, 0.5, 0.1, 0.5, 0.1]
                },
                {
                    name:'农产品',
                    type: 'bar',
                    barWidth: '20px',
                    stack: 'five',
                    itemStyle: {
                        normal: {
                            color: '#ff908f'
                        }
                    },
                    data: [0.3, 0.1, 0.1, 0.2, 0.3, 0.1, 0.1, 0.2, 0.3, 0.1, 0.1, 0.2, 0.3, 0.1, 0.1, 0.2, 0.1, 0.2]
                },
                {
                    name:'能化',
                    type: 'bar',
                    barWidth: '20px',
                    stack: 'five',
                    itemStyle: {
                        normal: {
                            color: '#f2e384'
                        }
                    },
                    data: [0.2, 0.15, 0.1, 0.1, 0.2, 0.15, 0.1, 0.1, 0.2, 0.15, 0.1, 0.1, 0.2, 0.15, 0.1, 0.1, 0.1, 0.1]
                },
                {
                    name:'黑色系',
                    type: 'bar',
                    barWidth: '20px',
                    stack: 'five',
                    itemStyle: {
                        normal: {
                            color: '#56c2f3'
                        }
                    },
                    data: [0.1, 0.15, 0.1,0.3, 0.1, 0.15, 0.1,0.3, 0.1, 0.15, 0.1,0.3, 0.1, 0.15, 0.1,0.3, 0.1,0.3]
                },
                {
                    name:'金融',
                    type: 'bar',
                    barWidth: '20px',
                    stack: 'five',
                    itemStyle: {
                        normal: {
                            color: '#ffbb8e'
                        }
                    },
                    data: [0.05, 0.15, 0.1, 0.15, 0.05, 0.15, 0.1, 0.15, 0.05, 0.15, 0.1, 0.15, 0.05, 0.15, 0.1, 0.15, 0.1, 0.15]
                },
                {
                    name:'贵金属',
                    type: 'bar',
                    barWidth: '20px',
                    stack: 'five',
                    itemStyle: {
                        normal: {
                            color: '#e4a5dc'
                        }
                    },
                    data: [0.05, 0.05, 0.1, 0.15, 0.05, 0.05, 0.1, 0.15, 0.05, 0.05, 0.1, 0.15, 0.05, 0.05, 0.1, 0.15, 0.1, 0.15]
                }
            ]
        }

        let pieOptions = {
            
            title: {
                x:'center',
                top: 10,
                text: '保证金占比',
                textStyle: {
                    color: '#999',
                    fontSize: 20,
                    fontWeight: '500'
                }
            },
            tooltip : {
                show: false,
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            grid: {
                containLabel: true,
                top: 0,
                x: 'center'
            },
            legend: {
                show: false,
                type: 'scroll',
                orient: 'vertical',
                right: 10,
                top: 20,
                bottom: 20
            },
            series : [ 
                {
                    type: 'pie',
                    animation: false,
                    color: ['rgba(175, 163, 245, 1)', 'rgba(0, 212, 136, 1)', 'rgba(64, 238, 213, 1)', 'rgba(60, 175, 255, 1)', 'rgba(241, 188, 76, 1)'],
                    radius: ['32%', '54%'],
                    center: ['50%', '50%'],
                    labelLine: {
                        normal: {
                            length: 20,
                            length2: 30,
                            lineStyle: {
                                color: '#ccc'
                            }
                        }
                    },
                    label: {
                        formatter: '{b}: {d}% ',
                        textStyle: {
                            color: '#666',
                            fontSize: 14
                        }
                    },
                    data: [
                        {value: 300, name: '非金属材料'},
                        {value: 500, name: '能源化工'},
                        {value: 900, name: '农副产品'},
                        {value: 1200, name: '股投'},
                        {value: 329, name: '贵金属'}
                    ],
                },

                {
                    radius: ['50%', '54%'],
                    center: ['50%', '50%'],
                    type: 'pie',
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    animation: false,
                    tooltip: {
                        show: false
                    },
                    itemStyle: {
                        normal: {
                            color:'rgba(250,250,250,0.5)'
                        }
                    },
                    data: [{
                        value: 1,
                    }],
                }
            ]
        }

        window.lineOptions = lineOptions

        // this.refs.industryStack.setOptions(stackOptions)

        this.refs.industrySet.setOptions(industryOptions)

        this.refs.marketStack.setOptions(stackOptions)

        this.refs.marketIndustrySet.setOptions(industryOptions)

        this.refs.smoothLine.setOptions(lineOptions)

        this.refs.bondsCharts.setOptions(holderOptions)
        
        this.refs.grideCharts.setOptions(gridOptions)

        this.refs.pieCharts.setOptions(pieOptions)

        this.refs.bondsPieCharts.setOptions(pieOptions)

        this.refs.bonds2Charts.setOptions(holderOptions)

        this.refs.gride2Charts.setOptions(gridOptions)

        this.refs.bondsPie2Charts.setOptions(pieOptions)

        this.refs.bondsPie3Charts.setOptions(pieOptions)

        let futureOptions = Object.assign({}, chartsOptions)
        futureOptions.xAxis[0].data = ['2012年', '2014年', '2016年']
        futureOptions.xAxis[0].offset = 20
        futureOptions.xAxis[0].axisLine.lineStyle.color = '#019afa'
        futureOptions.xAxis[0].axisTick.show = false

        futureOptions.yAxis[0].axisLine.show = false
        futureOptions.yAxis[0].name = '户/亿元'
        futureOptions.yAxis[0].splitLine.lineStyle.color = '#0d9ef9'
        

        futureOptions.series = [
            {
                type: 'bar',
                data: [9, 5, 7, 3, 6, 4, 7, 4, 8],
                barWidth: '30px',
                itemStyle: {
                    normal: {
                        top: 3,
                        shadowColor: 'rgba(14, 190, 247, 0.8)',
                        shadowBlur: 6,
                        color:  new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: 'rgba(14, 190, 247, 1)'
                        }, {
                            offset: 1,
                            color: 'rgba(172, 237, 255, 1)'
                        }]),
                        barBorderRadius: [20, 20, 20, 20],
                    }
                },
                label: {
                    normal: {
                        show: true,
                        formatter: '产值',
                        position: 'bottom',
                        color: '#999',
                        padding: 6,
                    }
                }
            },
            {
                type: 'bar',
                data: [8, 3, 9, 10, 6, 7, 5, 3, 1],
                barWidth: '30px',
                lineStyle: {
                    normal: {
                        shadowColor: 'rgba(0, 0, 0, 0.2)',
                        shadowOffsetY: 2,
                        shadowOffsetX: 2,
                        shadowBlur: 5
                    }
                },
                label: {
                    normal: {
                        show: true,
                        formatter: '利税',
                        position: 'bottom',
                        color: '#999',
                        padding: 6,
                    }
                },
                itemStyle: {
                    normal: {
                        padding: 6,
                        shadowColor: '#fe5c5a',
                        shadowBlur: 6,
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                            offset: 0,
                            color: '#fe5c5a'
                        }, {
                            offset: 1,
                            color: '#faa565'
                        }]),
                        barBorderRadius: [20, 20, 20, 20]
                    }
                },
            }
        ]

        this.refs.futureCharts.setOpitions(futureOptions)
        this.refs.futureCharts2.setOpitions(futureOptions)

        let barC = this.refs
        let tableChart = []
        let socketChart = []
        for (var i in barC) {
            if (i.indexOf('tableCharts') !== -1) {
                tableChart.push(i)
            }
            if (i.indexOf('tableSocketCharts') !== -1) {
                socketChart.push(i)
            }
        }

        tableChart.forEach(item => {
            this.refs[item].setOpitions(TableChartsConfig)  
        })

        socketChart.forEach(item => {
            this.refs[item].setOpitions(TableChartsConfig)
        })

    }

    renderTable = (chartsRef) =>{
        let dlTable = []
        for (let i  = 1; i < 13; i++) {
            dlTable.push(
            <dl key={`talbe-${i}`}>
                <dt>{`201${i}1231`}</dt>
                <dd>
                    <CommonCharts idName={`${chartsRef}${i}`} ref={`${chartsRef}${i}`} styles={{width: '204%', height: '100%'}}/>
                </dd>
            </dl>)
        }
        
        return dlTable
    }

    exportPDF () {
        console.log(window.lineOptions)
        axios({
            method: 'POST',
            url: '/api/hszcpz/index/exportPDF',
            data: JSON.stringify({token: cookie.get('token'), optionJson: [window.lineOptions, TableChartsConfig]})
        })
        .then(res => {
            console.log(res)
            if (res.status === 'success') {
                let data = JSON.parse(res.result)
                console.log(data)
                if (data.result === 'success') {
                    window.location.href = `http://${data.url}?token=${cookie.get('token')}`
                } else {
                    message.error('')
                }
            }
        })
    }

    async exprotWorld () {
        const dataBase = this.refs.industryStack.getContext()
        const baseData = this.refs.industrySet.getContext()
        let wordOptions = []
        wordOptions.push({
            moduleName: '资产配置', 
            moduleContent: [{
            textInfo: {
                leftTop: '',
                rightTop: ''
            },
            chartBase64: dataBase
        }]})

        wordOptions.push({
            moduleName: '行业配置', 
            moduleContent: [{
            textInfo: {
                leftTop: '',
                rightTop: ''
            },
            chartBase64: baseData
        }]})
        console.log(baseData)
        let data = await axios({
            method: 'POST',
            url: '/api/hszcpz/exportWord/produceProductWordFile',
            data: JSON.stringify({
                token: cookie.get('token'), 
                requestData: wordOptions
            })
        })

        if (data.status === 'success') {
            try {
                let json = JSON.parse(data.result)
                console.log(json)
                if (json.status === 'success') {
                    const url = `http://192.168.1.51:8090/netFilter/hszcpz/exportWord/exportProductWordFile?token=${cookie.get('token')}&fileName=${json.fileName}&fileDate=${json.fileDate}`
                    window.location.href = url
                }
            } catch (err) {
                console.log(err)
                message.error('解析出错')
            }
            
        } else {
            message.info('获取数据失败')
        }
    }


    render () {
        return (
        <div>
            <div className={ProductStyle.holder_wrapper}>
                <div className={ProductStyle.holder_btn_dec}><span className={'iconfont iconqushitu'}></span>股票多头</div>
                <WrapperHeader title='Brinson模型归因' fn={this.exportPDF}>
                    <div className='float_right product_select_date'>
                        <DatePicker locale={locale} placeholder={'开始日期'} />
                        <strong>~</strong>
                        <DatePicker locale={locale}  placeholder={'结束日期'} />
                    </div>
                </WrapperHeader>

                {/* <div className={`mt_40`}>
                    <WrapperHeader title='Barra模型归因' fn={this.exportPDF}>
                        <div className='float_right product_select_date'>
                            <DatePicker locale={locale} placeholder={'开始日期'} />
                            <strong>~</strong>
                            <DatePicker locale={locale}  placeholder={'结束日期'} />
                        </div>
                    </WrapperHeader>
                </div> */}


                {/* <div className={`mt_40`}>
                    <WrapperHeader fn={this.exprotWorld.bind(this)} title='行业集中度'>
                        <div className='float_right product_select_date'>
                            <DatePicker locale={locale} placeholder={'开始日期'} />
                            <strong>~</strong>
                            <DatePicker locale={locale}  placeholder={'结束日期'} />
                        </div>
                    </WrapperHeader>
                    <CommonBar idName='industryBarStack' ref='industryStack' />
                </div> */}

                <div className={`mt_40`}>
                    <WrapperHeader title='行业配置'>
                        <div className='float_right product_select_date'>
                            <span className='date_select_label'>统计日期</span>
                            <DatePicker locale={locale} label={'riqi'} />
                        </div>
                    </WrapperHeader>
                    <CommonBar idName='industrySetBar' ref='industrySet' />
                </div>

                <div className={`${ProductStyle.table_charts_content} mt_20`}>
                    <dl>
                        <dt></dt>
                        <dd></dd>
                    </dl>
                    {this.renderTable('tableSocketCharts')}

                </div>
            </div>


            <div className={`${ProductStyle.holder_wrapper} mt_20`}>
                <div className={ProductStyle.holder_btn_dec}><span className={'iconfont iconshichang'}></span>市场中性</div>
                <WrapperHeader title='板块配置'>
                    <div className='float_right product_select_date'>
                        <DatePicker locale={locale} placeholder={'开始日期'} />
                        <strong>~</strong>
                        <DatePicker locale={locale}  placeholder={'结束日期'} />
                    </div>
                </WrapperHeader>
                <CommonBar idName='marketBarStack' ref='marketStack' />

                <div className={`mt_40`}>
                    <WrapperHeader title='行业集中度'>
                        <div className='float_right product_select_date'>
                            <DatePicker locale={locale} placeholder={'开始日期'} />
                            <strong>~</strong>
                            <DatePicker locale={locale}  placeholder={'结束日期'} />
                        </div>
                    </WrapperHeader>
                    <CommonBar idName='smoothCharts' ref='smoothLine' />
                </div>

                <div className={`mt_40`}>
                    <WrapperHeader title='行业配置'>
                        <div className='float_right product_select_date'>
                            <span className='date_select_label'>统计日期</span>
                            <DatePicker locale={locale} label={'riqi'} />
                        </div>
                    </WrapperHeader>
                    <CommonBar idName='marketSetBar' ref='marketIndustrySet' />
                </div>

                <div className={`${ProductStyle.table_charts_content} mt_20`}>
                    <dl>
                        <dt></dt>
                        <dd></dd>
                    </dl>
                    {this.renderTable('tableCharts')}

                </div>
            </div>


            <div className={`${ProductStyle.holder_wrapper} mt_20`}>
                <div className={ProductStyle.holder_btn_dec}><span className={'iconfont iconzhaiquan'}></span>债券</div>
                <WrapperHeader title='板块配置'></WrapperHeader>
                <CommonBar idName='bondsBars' ref='bondsCharts' />
                <div className={`mt_40`}>
                    <div className={`${ProductStyle.bonds_contents} clear-fix`}>
                        <div className={`float_left ${ProductStyle.bonds_left}`}>
                            <WrapperHeader title='资产配比'></WrapperHeader>
                            <CommonBar idName='gridCharts' ref='grideCharts' />
                            <div className='mt_10'>
                                <div className='float_right product_select_date'>
                                    <span className='date_select_label'>统计日期</span>
                                    <DatePicker locale={locale} label={'riqi'} />
                                </div>
                                <CommonBar idName='pieCharts' ref='pieCharts' />
                                </div>
                            <WrapperHeader title='券种配置'>
                                <div className='float_right product_select_date'>
                                    <span className='date_select_label'>统计日期</span>
                                    <DatePicker locale={locale} label={'riqi'} />
                                </div>
                            </WrapperHeader>
                            <div className='mt_10'><CommonBar idName='bondsPieCharts' ref='bondsPieCharts' /></div>
                        </div>
                        <div className={`float_right ${ProductStyle.bonds_right}`}>
                            <WrapperHeader title='重仓债券'></WrapperHeader>
                            <div className={`${ProductStyle.year_report} mt_20`}>
                                <div className={`${ProductStyle.table_content}`}>
                                    <h2 className={`${ProductStyle.title_header_tit}`}>2019中报</h2>
                                    <ReportTable size={'small'} />
                                </div>

                                <div className={`${ProductStyle.table_content} mt_20`}>
                                    <h2 className={`${ProductStyle.title_header_tit}`}>2019季报</h2>
                                    <ReportTable size={'small'} />
                                </div>

                                <div className={`${ProductStyle.table_content} mt_20`}>
                                    <h2 className={`${ProductStyle.title_header_tit}`}>2019季报</h2>
                                    <ReportTable size={'small'} />
                                </div>

                                <div className={`${ProductStyle.table_content} mt_20`}>
                                    <h2 className={`${ProductStyle.title_header_tit}`}>2019季报</h2>
                                    <ReportTable size={'small'} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div className={`${ProductStyle.holder_wrapper} mt_20`}>
                <div className={ProductStyle.holder_btn_dec}><span className={'iconfont iconqihuo'}></span>期货</div>
                <WrapperHeader title='仓位总览'><div className='float_right'></div></WrapperHeader>
                <CommonBar idName='bonds2Bars' ref='bonds2Charts' />
                <div className={`mt_40`}>
                    <div className={`${ProductStyle.bonds_contents} clear-fix`}>
                        <WrapperHeader title='板块配置'></WrapperHeader>
                        <div className={`float_left ${ProductStyle.bonds_left}`}>
                            
                            <CommonBar idName='grid2Charts' ref='gride2Charts' />
                            
                            <div className='mt_20'>
                                <WrapperHeader title='品种分布'>
                                    <div className='float_right product_select_date'>
                                        <span className='date_select_label'>统计日期</span>
                                        <DatePicker locale={locale} label={'riqi'} />
                                    </div>
                                </WrapperHeader>
                                <CommonBar idName='bondsPie3Charts' ref='bondsPie3Charts' />
                            </div>
                        </div>
                        <div className={`float_right ${ProductStyle.bonds_right}`}>
                            <div className='mt_10'>
                                <div className='float_right product_select_date'>
                                    <span className='date_select_label'>统计日期</span>
                                    <DatePicker locale={locale} label={'riqi'} />
                                </div>
                                <CommonBar idName='bondsPie2Charts' ref='bondsPie2Charts' />
                            </div>
                            <div className='mt_10'>
                                <WrapperHeader title='资产配比'>
                                    <div className='float_right product_select_date'>
                                        <span className='date_select_label'>统计日期</span>
                                        <DatePicker locale={locale} label={'riqi'} />
                                    </div>
                                </WrapperHeader>
                                <CommonChart idName={'futureCharts'} ref={'futureCharts'} styles={{width: '100%', height: '400px'}} />

                                <CommonChart idName={'futureCharts2'} ref={'futureCharts2'} styles={{width: '100%', height: '400px'}} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className={`mt_40`}></div>
            </div>
        </div>
        )
    }
}