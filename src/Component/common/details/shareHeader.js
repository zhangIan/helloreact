import React from 'react'
import { Checkbox } from 'antd'
const shareHeader = (title, checked, fn) => {
    const checkStatus = checked ? true : false
    return (
        <div className='clear-fix charts_nav_header'>
            <div className='float_left'>
                <strong className={`charts_title chart_item_title`}>{title}</strong>
                <Checkbox checked={checkStatus}>起点对齐</Checkbox>
            </div>
            <div className='float_right'>
                <span onClick={fn} className='iconfont echarts_exports iconCombinedShape'></span>
            </div>
        </div>
    )
}

export default shareHeader