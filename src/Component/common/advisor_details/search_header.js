import React, { Component } from 'react'
import { Button, Input, DatePicker, Select } from 'antd'

const { Option } = Select

export default class CommonHeaders extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    render () {
        
        return (
        <div className={'product_common_header clear-fix'}>
            <div className={'float_left'}>
                <div className={'float_left left_common_header_content'}>
                    <span>文件类型：</span>
                    <Select style={{width: '130px'}} defaultValue={'公司资料'}>
                        <Option value='公司资料'>公司资料</Option>
                    </Select>
                </div>
                <div className={'float_left left_common_header_content ml_20'}>
                    <DatePicker className={'product_common_date'} placeholder={'开始日期'} />
                    <strong className={'header_content_split'}>~</strong>
                    <DatePicker className={'product_common_date'} placeholder={'结束日期'} />
                </div>
                <div className={'float_left left_common_header_content ml_20'}>
                    <Select style={{width: '130px'}} defaultValue={'标题检索'}>
                        <Option value='标题检索'>标题检索</Option>
                    </Select>
                </div>
                <div className={'float_left left_common_header_content ml_20'}>
                    <Input className={'file_search_input'} />
                </div>
                <div className={'float_left left_common_header_content ml_20'}>
                    <Button type='danger'>搜索</Button>
                </div>
            </div>
            <div className={'float_right'}>
                <Button type='danger'>导出</Button>
            </div>
        </div>)
    }
}