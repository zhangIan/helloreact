import React, { Component } from 'react'
import ProductStyle from '../../product/product.module.sass'
import WrapperHeader from '../details/wrapper_header'
import { axios } from '../../../Utils/index'
import cookie from 'js-cookie'
import url from 'url'
import { message, Table } from 'antd'

export default class Company extends Component {
    constructor(props) {
        super(props)
        this.state = {
            holderInof: [
                {dataIndex: 'shareholders', key: '1', title: '股东名称'},
                {dataIndex: 'amount', key: '2', title: '出资金额（万）'},
                {dataIndex: 'type', key: '3', title: '出资方式'},
                {dataIndex: 'proportion', key: '4', title: '出资比例（%）'}
            ],
            coreInfo: [
                {dataIndex: 'person_name', key: '1', title: '姓名', width: 100},
                {dataIndex: 'duty', key: '2', title: '职务', width: 100},
                {dataIndex: 'resume', key: '3', title: '简介'},
                {dataIndex: 'working_years', key: '4', title: '从业年限', width: 100, render: (r) => r ? r : '--'},
                {dataIndex: 'invest_years', key: '5', title: '实盘经验年限', width: 80, render: (r) => r ? r : '--'},
                {dataIndex: 'asset_mgt_scale', key: '6', title: '过往管理规模（亿）', width: 110, render: (r) => r ? r : '--'}
            ],
            companyInfo: [
                {dataIndex: 'department_name', key: '1', title: '部门全称'},
                {dataIndex: 'department_number', key: '2', title: '部门人数'},
                {dataIndex: 'department_duty', key: '3', title: '部门主要职能'},
                {dataIndex: 'department_leader', key: '4', title: '主要负责人'}
            ],
            investInfo: [
                {dataIndex: 'investment_process', key: '1', title: '策略名称', width: 120, render: (r) => r ? r : '--'},
                {dataIndex: 'investment_idea', key: '2', title: '策略独特性介绍', render: (r) => r ? r : '--'}
            ],
            strategyInfo: [
                {dataIndex: 'strategy_name', key: '1', title: '策略名称', width: 120, render: (r) => r ? r : '--'},
                {dataIndex: 'strategy_introduction', key: '2', title: '策略独特性介绍', render: (r) => r ? r : '--'}
            ],
            baseInfo: [
                {column_name: '公司全称', last_name: '公司简称', org_full_name: 'org_full_name', org_name: 'org_name'},
                {column_name: '登记编号', last_name: '法定代表人', org_full_name: 'reg_code', org_name: 'legal_person'},
                {column_name: '登记日期', last_name: '成立日期', org_full_name: 'reg_time', org_name: 'foundation_date'},
                {column_name: '是否具备投顾资格', last_name: '组织机构代码', org_full_name: 'is_qualification', org_name: 'org_code'},
                {column_name: '注册资本（万元）', last_name: '实缴资本（万元）', org_full_name: 'reg_capital', org_name: 'real_capital'},
                {column_name: '员工人数（人）', last_name: '基金经理人数（人）', org_full_name: 'total_number', org_name: 'touyan_number'},
                {column_name: '资产管理规模（亿元）', last_name: '已发行产品数量', org_full_name: 'asset_mgt_scale', org_name: 'issued_num'},
                {column_name: '注册地址', last_name: '', org_full_name: 'reg_address', org_name: null},
            ]
        }
    }

    async _getReturnOrg (params) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
            url: '/api/hszcpz/org/getHhhsOrgInfo',
            method: 'POST',
            data: JSON.stringify({
                orgId: query.id,
                token: cookie.get('token'),
                ...params
            })
        })
        
        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }
        
        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)
        
        if (result.status === 'success') {
            console.log(result, 'getReturnCal-------')
            return result.result
        } else {
            message.error('获取数据失败')
        }

    }

    async componentDidMount () {
        const data = await this._getReturnOrg()
        let holderSource = data ? data.shareHolderCapitalOwn.hasOwnProperty('shareholder_structure') ? data.shareHolderCapitalOwn.shareholder_structure : [] : []
        let baseSource = null
        let coreSource = []
        let companySource = [], strategySource = [], investSource = null
        
        if (holderSource) {
            holderSource.forEach(item => {
                item.key = item.row_name
                item.proportion = parseFloat(item.proportion).toFixed(2) * 100  + '%'
            })
        }

        if (data && data.orgBasicInfo) {
            baseSource = data.orgBasicInfo
            baseSource.real_capital = parseFloat(baseSource.real_capital).toFixed(2)
            baseSource.reg_capital = parseFloat(baseSource.reg_capital).toFixed(2)
        }

        if (data && data.hasOwnProperty('coreStudyPersonnel')) {
            if (data.coreStudyPersonnel.hasOwnProperty('team_core_touyan_member')) {
                coreSource = data.coreStudyPersonnel.team_core_touyan_member
                coreSource.forEach((item) => {
                    item.key = item.row_name
                })
            }
        }

        if (data && data.hasOwnProperty('corpOrganStructure')) {
            if (data.corpOrganStructure.hasOwnProperty('department_structure')) {
                companySource = data.corpOrganStructure.department_structure
                companySource.forEach(item => {
                    item.key = item.row_name
                })
            }
        }

        if (data && data.hasOwnProperty('mainInvestStrategy')) {
            if (data.mainInvestStrategy.hasOwnProperty('investment_strategy')) {
                strategySource = data.mainInvestStrategy.investment_strategy
                strategySource.forEach(item => {
                    item.key = item.row_name
                })
            }
        }

        if (data && data.hasOwnProperty('investConceptProcess')) {
            if (data.investConceptProcess.hasOwnProperty('investment_idea')) {
                investSource = data.investConceptProcess
            }
        }
        
        
        await this.setState({investSource: investSource, holderSource: holderSource, baseSource, coreSource: coreSource, companySource: companySource, strategySource: strategySource})
    }

    render () {
        let { holderInof, holderSource, coreInfo, coreSource, investSource, companyInfo, baseInfo, baseSource, companySource, strategyInfo, strategySource } = this.state
        
        return (
        
            <div className={ProductStyle.charts_items_list}>
                <div className={`${ProductStyle.none_bg} mt_20 clear-fix`}>
                    <div className={`float_left ${ProductStyle.history_list}`}>
                        <div className={ProductStyle.none_bg_wrapper}>
                            <WrapperHeader title={'投顾基本资料'} iconStatus={true}></WrapperHeader>
                            {baseSource ? <table className={`customer_table_list customer_table_border`}>
                                <tbody>
                                    {baseInfo.map((item, index) => {
                                    return (<tr key={index}>
                                            <td>{item.column_name}</td>
                                            {item.last_name ? <td>{baseSource ? baseSource[item.org_full_name] ? baseSource[item.org_full_name] : '--' : '--'}</td>: <td colSpan={'3'}>{baseSource ? baseSource[item.org_full_name] ? baseSource[item.org_full_name] : '--' : '--'}</td>} 
                                            {item.last_name ? <td>{item.last_name}</td> : null}
                                            {item.last_name ?<td>{baseSource ? baseSource[item.org_name] ? baseSource[item.org_name] : '--' : '--'}</td> : null }
                                            </tr>)
                                    })}
                                </tbody>
                             </table> : <Table dataSource={[]} />}
                        </div>

                        <div className={`${ProductStyle.none_bg_wrapper} mt_20`}>
                            <WrapperHeader title={'核心投研人员'} iconStatus={true}></WrapperHeader>
                            <Table columns={coreInfo} dataSource={coreSource || []}  pagination={coreSource && coreSource.length > 20 ? {} : false} />
                        </div>

                        <div className={`${ProductStyle.none_bg_wrapper} mt_20`}>
                            <WrapperHeader title={'投资理念与流程'} iconStatus={true}></WrapperHeader>
                            {
                                investSource ? <table className={`customer_table_list customer_table_border`}>
                                    <tbody>
                                        <tr>
                                            <td style={{width: '100px'}}>投资理念</td>
                                            <td>{investSource.investment_idea}</td>
                                        </tr>
                                        <tr>
                                            <td>投资流程</td>
                                            <td>{investSource.investment_process}</td>
                                        </tr>
                                    </tbody>
                                </table> : <Table dataSource={[]} />
                            }
                            
                        </div>
                    </div>


                    <div className={`float_right ${ProductStyle.ranking_list}`}>
                        <div className={ProductStyle.none_bg_wrapper}>
                            <WrapperHeader title={'股东出资情况'} iconStatus={true}></WrapperHeader>
                            <Table pagination={holderSource && holderSource.length > 20 ? {} : false} className={`table_border_right`} columns={holderInof} dataSource={holderSource || []}/>
                        </div>

                        <div className={`${ProductStyle.none_bg_wrapper} mt_20`}>
                            <WrapperHeader title={'公司组织架构'} iconStatus={true}></WrapperHeader>
                            <Table columns={companyInfo} dataSource={companySource || []}  pagination={companySource && companySource.length > 20 ? {} : false} />
                        </div>

                        <div className={`${ProductStyle.none_bg_wrapper} mt_20`}>
                            <WrapperHeader title={'主要投资策略'} iconStatus={true}></WrapperHeader>
                            <Table columns={strategyInfo} dataSource={strategySource || []} pagination={strategySource && strategySource.length > 20 ? {} : false} />
                        </div>
                    </div>
                </div>


            </div>
        
        )
    }
}