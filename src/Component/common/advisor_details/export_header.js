import React, { Component } from 'react'
import { Button } from 'antd'


export default class ExportHeaders extends Component {
    constructor (...props) {
        super(...props)
        this.state = {}
    }

    render () { 
        
        return (
        <div className={'product_common_header clear-fix'}>
            <div className={'float_left'}>
                {this.props.children}
            </div>
            <div className={'float_right'}>
                <Button type='danger'>导出</Button>
            </div>
        </div>)
    }
}