import React, { Component } from 'react'
import SearchHeader from './search_header'
import ProductStyle from '../../product/product.module.sass'
import WrapperHeader from '../details/wrapper_header'
import { Table } from 'antd'


const columns = [
    {
        title: '标题',
        dataIndex: 'title'
    },
    {
        title: '',
        dataIndex: 'company'
    },
    {
        title: '主题',
        dataIndex: 'theme'
    },
    {
        title: '发布时间',
        dataIndex: 'pubDate'
    },
    {
        title: '',
        dataIndex: 'actions'
    }
]
export default class NewsList extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    render () {
        return (
            <div className={ProductStyle.charts_items_list}>
                <div className={ProductStyle.charts_item_bg}>
                 <SearchHeader></SearchHeader>
                 <div className={`mt_20`}><WrapperHeader title='资讯报告' iconStatus={true} /></div>
                 <div className={`mt_10`}>
                     <Table className={`border_table_grid`} columns={columns} />
                 </div>
                </div>
            </div>
        )
    }
}