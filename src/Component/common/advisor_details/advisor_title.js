import  React, { Component } from 'react'
import { Row, Col, Tag, Button, Icon } from 'antd'
import ProductStyle from '../../product/product.module.sass'
import moment from 'moment'


export default class Detail extends Component {
    constructor (...props) {
        super(...props)

        this.state = {}
    }

    async UNSAFE_componentWillMount () {
        const advisorData = await this.props._getDetails()
        console.log(advisorData)
        this.setState({advisorDetails: advisorData})
        if (advisorData && advisorData.hasOwnProperty('cOrgName')) {
            window.localStorage.setItem('advisorName', advisorData.cOrgName)
        } else {
            window.localStorage.setItem('advisorName', '')
        }
    }

    async componentDidMount () {

    }

    advisorStar (star) {
        if (star === null || star === undefined) {
            return [<em key='1' className='iconfont iconxingxing color_grey'></em>,
            <em key='2' className='iconfont iconxingxing color_grey'></em>,
            <em key='3' className='iconfont iconxingxing color_grey'></em>,
            <em key='4' className='iconfont iconxingxing color_grey'></em>,
            <em key='5' className='iconfont iconxingxing color_grey'></em>]
        }
        const starList = [
        <em key='1' className='iconfont iconxingxing color_grey'></em>,
        <em key='2' className='iconfont iconxingxing color_grey'></em>,
        <em key='3' className='iconfont iconxingxing color_grey'></em>,
        <em key='4' className='iconfont iconxingxing color_grey'></em>,
        <em key='5' className='iconfont iconxingxing color_grey'></em>]
        const stars = parseInt(star)
        if (stars >= 0) {
            for(let i = 0; i < stars; i++) {
                starList[i] = <em key={i} className='iconfont iconxingxing color_yellow'></em>
            }
        }
        return starList
    }

    render () {
        const details = this.state.advisorDetails
        return (
            <div className={ProductStyle.product_info_wrapper}>
                <Row type={'flex'} align='middle'>
                    <Col span={11} className={`${ProductStyle.display_flex_align_center}`}>
                        <h2 className={ProductStyle.product_name}>{details ? details.cOrgName : '--'}</h2>
                        <div className={ProductStyle.info_tags}>
                            <Tag>股票多头</Tag>
                            <Tag>成长型</Tag>
                            <Tag>选股型</Tag>
                            <Tag className={ProductStyle.border_tags}>上升市道</Tag>
                            <Tag className={ProductStyle.border_tags}>量化</Tag>
                        </div>
                    </Col>
                    
                    <Col span={8}>
                        <span>投顾星级：</span>
                        {this.advisorStar(details ? details.cOrgStar : 0)}

                    </Col>

                    <Col span={5} className={`${ProductStyle.right_buttons} float_right`}> 
                        <Row>
                            {/* <Button type='danger'>已关注</Button> */}
                            <Button type='danger'>导出报告<Icon type="caret-down" /></Button>
                        </Row>
                    </Col>
                </Row>

                <Row>
                    <Col span={11}>
                        <ul className={`clear-fix ${ProductStyle.advisor_title_company}`}>
                            <li><span>公司全称：</span><em className={`color_red`}>{details ? details.cOrgFullName : '--'}</em></li>
                            <li><span>成立时间：</span><em className={`color_red`}>{details ? details.tFoundationDate ? moment(details.tFoundationDate).format('YYYY-MM-DD') : '--' : '--'}</em></li>
                            <li><span>注册资本：</span><em>{details ? details.nRegCapital ? details.nRegCapital : '0' : '--'}</em></li>
                        </ul>
                    </Col>
                    
                    <Col span={8}>
                        <ul className={`clear-fix ${ProductStyle.advisor_title_company}`}>
                            <li><span>机构类型：</span><em>{details ? details.cOrgCategory ? details.cOrgCategory : '--' : '--'}</em></li>
                            <li><span>所在区域：</span><em className={`color_red`}>{details ? details.adress ? details.adress : '--' : '--'}</em></li>
                        </ul>
   
                    </Col>

                    <Col span={5} className={`${ProductStyle.right_buttons} float_right`}> 
                        <Row>
                            <ul className={`clear-fix ${ProductStyle.advisor_title_company}`}>
                                <li><span>登记编号：</span><em>{details ? details.cRegCode ?  details.cRegCode : '--' : '--'}</em></li>
                            </ul>
                        </Row>
                    </Col>
                </Row>
                 
            </div>
        )
    }
}