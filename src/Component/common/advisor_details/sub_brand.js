import React, { Component } from 'react'
import ProductStyle from '../../product/product.module.sass'
import WrapperHeader from '../details/wrapper_header'
// import ExportHeaders from '../advisor_details/export_header'
import  { DatePicker, message } from 'antd'
import locale from 'antd/es/date-picker/locale/zh_CN'
import CommonBar from '../charts/common_bar'
import ProductList from '../common_table/index'
import SubTable from '../common_table/product_table'
import { axios } from '../../../Utils/index'
import cookie from 'js-cookie'
import url from 'url'
import { chartsOptions, chartsColors } from '../../../Utils/charts_config/charts_config'

// const Option = Select.Option

export default class Performance extends Component {
    constructor (...props) {
        super(...props)
        this.state = {
            dataSource: []
        }
    }

    async _getSubjectOrg (params) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
            url: '/api/hszcpz/org/orgFunds',
            method: 'POST',
            data: JSON.stringify({
                c_org_id: query.id,
                token: cookie.get('token'),
                pageSize: '15',
                pageNo: '1',
                ...params
            })
        })
        
        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }
        
        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)
        
        if (result.result === 'success') {
            // console.log(result, 'getReturnCal-------')
            return result
        } else {
            message.error('获取数据失败')
        }

    }

    async _getAdvisorStar (params) {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
            url: '/api/hszcpz/org/orgFundsStar',
            method: 'POST',
            data: JSON.stringify({
                c_org_id: query.id,
                token: cookie.get('token'),
                ...params
            })
        })
        
        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }
        
        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)
        
        if (result.result === 'success') {
            // console.log(result, 'advisor items-------')
            return result
        } else {
            message.error('获取数据失败')
        }
    }

    async _chartAdvisor () {
        const data = await this._getAdvisorStar()
        const nameSpace = {'5': '五星', '4': '四星', '3': '三星', 'threebelow': '三星以下', 'other': '未评星级'}
        if (data) {
            const resultAdvisor = data.orgFundsStar
            let pieBar1Options = Object.assign(chartsOptions, {})
            pieBar1Options.legend.data = []
            pieBar1Options.title = {
                x:'center',
                top: 10,
                text: '基金星级评价',
                textStyle: {
                    color: '#666',
                    fontSize: 16,
                    fontWeight: '500'
                }
            }
            pieBar1Options.xAxis[0].axisLine.show = false
            let datas = []
            resultAdvisor.forEach(item => {
                const keys = Object.keys(item)
                datas.push({name: nameSpace[item[keys[0]]+''], value: item[keys[1]]})
            })
            // console.log(datas)
            let seriesArr = [ 
                {
                    type: 'pie',
                    animation: false,
                    color: chartsColors,
                    radius: ['32%', '54%'],
                    center: ['50%', '50%'],
                    labelLine: {
                        normal: {
                            length: 20,
                            length2: 30,
                            lineStyle: {
                                color: '#ccc'
                            }
                        }
                    },
                    label: {
                        formatter: '{b}: {d}% ',
                        textStyle: {
                            color: '#666',
                            fontSize: 14
                        }
                    },
                    data: datas,
                },

                {
                    radius: ['50%', '54%'],
                    center: ['50%', '50%'],
                    type: 'pie',
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    animation: false,
                    tooltip: {
                        show: false
                    },
                    itemStyle: {
                        normal: {
                            color:'rgba(250,250,250,0.5)'
                        }
                    },
                    data: [{
                        value: 1,
                    }],
                }
            ]

            pieBar1Options.series = seriesArr

            this.refs.productPieBar2.setOptions(pieBar1Options)
        }
    }
    async UNSAFE_componentWillMount () {
        // await this._getAdvisorStar()
    }

    async componentDidMount () {
        // await this._getSubjectOrg()
        let pieBar1Options = Object.assign(chartsOptions, {})
        pieBar1Options.legend.data = []
        pieBar1Options.xAxis[0].axisLine.show = false
        pieBar1Options.title = {
            x:'center',
            top: 10,
            text: '产品分布',
            textStyle: {
                color: '#666',
                fontSize: 16,
                fontWeight: '500'
            }
        }

        pieBar1Options.series = [ 
            {
                type: 'pie',
                animation: false,
                color: chartsColors,
                radius: ['32%', '54%'],
                center: ['50%', '50%'],
                labelLine: {
                    normal: {
                        length: 20,
                        length2: 30,
                        lineStyle: {
                            color: '#ccc'
                        }
                    }
                },
                label: {
                    formatter: '{b}: {d}% ',
                    textStyle: {
                        color: '#666',
                        fontSize: 14
                    }
                },
                data: [
                    {value: 300, name: '非金属材料'}
                ],
            },

            {
                radius: ['50%', '54%'],
                center: ['50%', '50%'],
                type: 'pie',
                label: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: false
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: false
                    }
                },
                animation: false,
                tooltip: {
                    show: false
                },
                itemStyle: {
                    normal: {
                        color:'rgba(250,250,250,0.5)'
                    }
                },
                data: [{
                    value: 1,
                }],
            }
        ]

        this.refs.productPieBar1.setOptions(pieBar1Options)
        
        this._chartAdvisor()
    }


    noCheckHeader = (title) => {
        return (
            <div className='clear-fix charts_nav_header'>
                <div className='float_left'>
                    <strong className={`charts_title chart_item_title`}>{title}</strong>
                </div>
            </div>
        )
    }

    render () {
        return (
            <div className={ProductStyle.charts_items_list}>
                <div className={ProductStyle.charts_item_bg}>
                 {/* <ExportHeaders>
                    <div className={'float_left left_common_header_content ml_20'}>
                        <span>产品状态：</span>
                        <Select style={{width: '130px'}} placeholder='全部'>
                            <Option value='全部'>全部</Option>
                        </Select>
                    </div>
                    <div className={'float_left left_common_header_content ml_20'}>
                        <span>最近净值日期：</span>
                        <Select  style={{width: '130px'}} placeholder='不限'>
                            <Option value='不限'>不限</Option>
                        </Select>
                    </div>
                 </ExportHeaders> */}


                <div className={`mt_20`}>
                    <WrapperHeader title={'产品表现'}></WrapperHeader>
                    <div className={`mt_20`}>
                        <ProductList _initData={this._getSubjectOrg.bind(this)} />
                    </div>
                </div>
                </div>
                

                <div className={`${ProductStyle.charts_item_bg} mt_20`}>
                    <WrapperHeader title={'产品概况'}></WrapperHeader>
                    <div className='mt_20 clear-fix'>
                        <div className={`float_left ${ProductStyle.bonds_left}`}>
                            <CommonBar ref={`productPieBar1`} idName={`pieBar1`} />
                        </div>
                        <div className={`float_right ${ProductStyle.bonds_right}`}>
                            <CommonBar ref={`productPieBar2`} idName={`pieBar2`}  />
                        </div>
                    </div>
                </div>

                <div className={`${ProductStyle.charts_item_bg} mt_20`}>
                    <WrapperHeader title={'产品相关性'}>
                        <div className='float_right product_select_date'>
                            <DatePicker locale={locale} placeholder={'开始日期'} />
                            <strong>~</strong>
                            <DatePicker locale={locale}  placeholder={'结束日期'} />
                        </div>
                    </WrapperHeader>
                    <div className='mt_20 clear-fix'>
                    <div className={`float_left ${ProductStyle.bonds_left}`}>
                            <SubTable />
                        </div>
                        <div className={`float_right  ${ProductStyle.bonds_right}`}>
                            <SubTable />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

