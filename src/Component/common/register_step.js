import React, { Component } from 'react'
import { Form, Input, Button, message } from 'antd'
import { axios } from '../../Utils/index'
import qs from 'querystring'

const FormItem = Form.Item

class NextStep extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    componentDidMount () { console.log(this.props) }

    _reset = () => {
        this.props.form.resetFields()
        this.props.hideModal()
        this.props.showVisible()
    }

    _registerStep = e => {
        e.preventDefault()
        const { stateTime } = this.props

        if (new Date().getTime() > (stateTime + 1000 * 60 * 5)) {
            message.info('注册页面时间过长，注册失败！')
            this.props.hideModal()
            setTimeout(() => {
                window.location.reload()
            }, 500)
        }
        
        this.props.form.validateFields((err, values) => {
            if (!err) {
                axios({
                  method: 'post',
                  url: '/api/sso/account/register',
                  data: qs.stringify({
                      phone: this.props.userName.phone,
                      verCode: this.props.userName.verCode,
                      username: values.username,
                      password: values.password,
                      repeatPassword: values.repeatPassword,
                      email: values.email || ''
                  })
                })
                .then(data => {
                    if (data.status !== 'success') {
                        message.error(data.errorReason)
                        return
                    }
                    data = JSON.parse(data.result)
                    if (data.result === 'success') {
                        message.info(data.reason)
                        this.props.hideModal()
                    } else {
                        message.info('注册失败，请稍后再试')
                    }
                })
              }
        })
    }

    render () {
        const { getFieldDecorator } = this.props.form
        const { userName } = this.props.userName
        
        return (
            <Form onSubmit={this._registerStep}>
                <FormItem>
                    {
                        getFieldDecorator('username', 
                        {
                            initialValue: userName,
                            rules: [{
                                required: true,
                                message: '用户名只能由4-16个数字、汉字、字母组成，必须含有字母'
                            },{
                                validator: (rule, value, callback) => {
                                    if (!value) {
                                        callback()
                                        return
                                    }
                                    if (value.length > 16) {
                                        callback('用户名只能小于或等于16位')
                                        return
                                    }
                                    if (value.length <4) {
                                        callback('用户名必须大于4位')
                                        return
                                    }
                                    if (!(/\w+/ig.test(value))) {
                                        callback('用户名必须包含字母')
                                        return
                                    }
                                    callback()
                                }
                            }]
                        })(
                            <Input
                            className='user-modal-default'
                            id={'userName'}
                            allowClear={true}
                            prefix={<span className="user-label">用户名</span>}
                            placeholder='请输入用户名' /> 
                        )
                    }
                </FormItem>
                <FormItem>
                    {
                        getFieldDecorator('password', {
                            rules: [{
                                required: true,
                                message: '用户密码不能为空'
                            },{
                                validator: (rule, value, callback) => {
                                    if (!value) {
                                        callback()
                                        return
                                    }
                                    if (value.length > 20) {
                                        callback('密码可由字母/数字/符号组成，6-20位，区分大小写')
                                        return
                                    }
                                    if (value.length < 6) {
                                        callback('密码可由字母/数字/符号组成，6-20位，区分大小写')
                                        return
                                    }
                                
                                    callback()
                                }
                            }]
                        })(
                            <Input.Password
                            autoComplete={'new-password'}
                            className='user-modal-default'
                            allowClear={true}
                            prefix={<span className="user-label">用户密码</span>}
                            type='password' placeholder='请输入密码' />
                        )
                    }
                </FormItem>
                <FormItem>
                    {
                        getFieldDecorator('repeatPassword', {
                            rules: [{
                                required: true,
                                message: '请确认用户密码'
                            },{
                                validator: (rule, value, callback) => {
                                    if (!value) {
                                        callback()
                                        return
                                    }
                                    if (value.length > 20) {
                                        callback('密码可由字母/数字/符号组成，6-20位，区分大小写')
                                        return
                                    }
                                    if (value.length < 6) {
                                        callback('密码可由字母/数字/符号组成，6-20位，区分大小写')
                                        return
                                    }
                                
                                    callback()
                                }
                            }]
                        })(
                            <Input.Password
                            className='user-modal-default'
                            allowClear={true}
                            prefix={<span className="user-label">确认密码</span>}
                            type='password' placeholder='请确认密码' />
                        )
                    }
                </FormItem>
                <FormItem>
                    {
                        getFieldDecorator('email', {
                            rules: [{
                                required: false,
                                message: '请输入email'
                            }]
                        })(
                            <Input
                            className='user-modal-default'
                            prefix={<span className="user-label">Email 邮箱</span>}
                            placeholder='请输入Email' />
                        )
                    }
                </FormItem>
                <FormItem className="next-button-wrapper">
                    <Button type='primary' onClick={() => {this._reset()}} className='register-reset'>上一步</Button>
                    <Button type='primary' htmlType='submit' className='register-step-submit'>确定</Button>
                </FormItem>
            </Form>
        )
    }
}

const WrapperNextForm = Form.create({})(NextStep)

export default WrapperNextForm
