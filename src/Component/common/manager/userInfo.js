import  React, { Component } from 'react'
import { Row, Col } from 'antd'
import ProductStyle from '../../product/product.module.sass'
import ManagerStyle from '../../investment_manager/manager.module.sass'


export default class ManagerTitle extends Component {
    constructor (...props) {
        super(...props)

        this.state = {}
    }

    async UNSAFE_componentWillMount () {
        if (this.props._getDetails) {
            const advisorData = await this.props._getDetails()
            // console.log(advisorData)
            this.setState({advisorDetails: advisorData})
            if (advisorData && advisorData.hasOwnProperty('cOrgName')) {
                window.localStorage.setItem('advisorName', advisorData.cOrgName)
            } else {
                window.localStorage.setItem('advisorName', '')
            }    
        }
    }

    async componentDidMount () {

    }

    render () {
        const details = this.state.advisorDetails
        return (
            <div className={ProductStyle.product_info_wrapper}>
                <Row type={'flex'} align='middle'>
                    <Col span={21} className={`${ProductStyle.display_flex_align_center}`}>
                        <h2 className={ProductStyle.product_name}>{details ? details.c_person_name : '--'}</h2>
                    </Col>

                    {/* <Col span={3} className={`${ProductStyle.right_buttons} float_right`}> 
                        <Row>
                            <Button type='danger'>导出报告<Icon type="caret-down" /></Button>
                        </Row>
                    </Col> */}
                </Row>

                <Row>  
                    <ul className={`clear-fix ${ProductStyle.advisor_title_company} ${ManagerStyle.float_manager_list}`}>
                        <li><span>职位：</span><em className={`color_red`}>{details ? details.c_duty : '--'}</em></li>
                        <li><span>学历：</span><em>{details ? details.c_education ? details.c_education : '--' : '--'}</em></li>
                        <li><span>从业年限：</span><em>{details ? details.c_investment_years ? details.c_investment_years : '--' : '--'}</em></li>
                        <li><span>职业背景：</span><em>{details ? details.c_background ? details.c_background : '--' : '--'}</em></li>
                    </ul>
                </Row>
                <Row>  
                    <ul className={`clear-fix ${ProductStyle.advisor_title_company} ${ManagerStyle.float_manager_list}`}>
                        <li><span>管理产品（数不含已终止）：</span><em>{details ? details.n_fund_num : '--'}</em></li>
                        <li><span>所在机构：</span><em className={`color_red`}>{details ? details.c_org_name ? details.c_org_name : '--' : '--'}</em></li>
                        <li><span>当前公司任职年数：</span><em>{details ? details.working_years ? details.working_years : '--' : '--'}</em></li>
                        <li><span>擅长策略：</span><em>{details ? details.c_master_strategy ? details.c_master_strategy : '--' : '--'}</em></li>
                    </ul>
                </Row>

                <Row>
                    <p className={`${ProductStyle.manager_introduce}`}>
                       {details ? details.perIntroduce ? details.perIntroduce : '' : '--'}
                    </p>
                </Row>
                 
            </div>
        )
    }
}