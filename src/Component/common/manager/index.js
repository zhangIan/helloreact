import React, { Component } from 'react'
import Avatar from '../avatar/avatar'
import Userinfo from './userInfo'

export default class Manager extends Component {
    render () {
        return <div>
            <div>
                <Avatar />
            </div>
            <div>
                <Userinfo />
            </div>
        </div>
    }
}