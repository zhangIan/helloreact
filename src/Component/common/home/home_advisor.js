import React, { Component } from 'react'
import HomeAdvisorStyle from './indexAdvisor.module.sass'
import { Tabs, Table, Input, Button, Icon } from 'antd'
import './indexAdvisor.sass'

const { TabPane } = Tabs

const commonColumns = [
    {title: '序号', key: '1', dataIndex: 'serial'},
    {title: '投顾名称', key: '2', dataIndex: 'name'},
    {title: '核心人物', key: '3', dataIndex: 'core_person'},
    {title: '主要策略', key: '4', dataIndex: 'core_person'},
    {title: '成立日期', key: '5', dataIndex: 'core_person'},
    {title: '产品数量', key: '6', dataIndex: 'core_person', children: [
        {
            title: '续存中',
            dataIndex: 'rolling',
            key: 'rolling'
        },
        {
            title: '累计数',
            dataIndex: 'totalNumber',
            key: 'totalNumber'
        }
    ]},

    {title: '代表产品', key: '7', children: [
        {
            title: '产品名称',
            dataIndex: 'productName',
            key: 'productName'
        },
        {
            title: '成立以来收益',
            dataIndex: 'totalRate',
            key: 'totalRate'
        },
        {
            title: '成立以来最大回撤',
            dataIndex: 'maxDrop',
            key: 'maxDrop'
        }
    ]}
]

export default class HomeAdvisor extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    render () {
        return (
        <div className={`${HomeAdvisorStyle.home_advisor_wrapper} customer_home_advisor_wrapper`}>
            <div className={`${HomeAdvisorStyle.customer_home_tab_search} clear-fix`}>
                <ul className={HomeAdvisorStyle.tab_fixed_content}>
                    <li className={`position_relative ${HomeAdvisorStyle.product_name_input}`}>
                        <Input className='searchProduct' value={this.state.fund_name} type='text' data='fund_name' placeholder={'投资顾问/基金经理人'} />
                    </li>
                    <li><Button type="primary" onClick={() => {this.searchProduct()}}>搜索</Button></li>
                    <li><Button type="primary" onClick={ () => { this.resetOptions() }}>重置</Button></li>
                    <li><Button type="primary" onClick={this.toggle}>{this.state.expend ? '收起筛选项' : '展开筛选项'} <Icon type={this.state.expend ? 'up' : 'down'} /></Button></li>
                </ul>

                <div className={`${HomeAdvisorStyle.customer_search_options}`}>
                    
                </div>
            </div>
            <div>
                <Tabs className={`${HomeAdvisorStyle.home_advisor_taps} customer_home_advisor_tabs`}>
                    <TabPane tab={`核心池`} key={`1`}>
                        <Table bordered columns={commonColumns} className={`customer_home_advisor_tables`} />
                    </TabPane>
                    <TabPane tab={`初选池`} key={`2`}>
                        <Table bordered columns={commonColumns} />
                    </TabPane>
                </Tabs>
            </div>
        </div>
        )
    }
}