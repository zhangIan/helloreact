import React, { Component } from 'react'
import { Input, Button, Icon, Row, Col, DatePicker, Select, message } from 'antd'
import Search from '../../style/tab_search.module.sass'
import '../../style/common.sass'
import HomeTable from './home_table'
import locale from 'antd/es/date-picker/locale/zh_CN'
import { axios } from '../../Utils/index'
import cookie from 'js-cookie'

const { Option } = Select



export default class TabsSearch extends Component {
    constructor (props) {
        super(props)
        this.state = {
            dataSource: [],
            expend: false,
            changeN: true,
            backN: true,
            sharpeN: true,
            coreSelect: true,
            stock: false,
            marketing: false,
            bonds: false,
            future: false,
            options: false,
            currentPage: 1,
            totalPage: 1,
            pageSize: 20,
            searchParams: null,
            loading: false,
            selectionStatus: false,
            asset_type: [],
            strategy_type: [],
            consultant_level: [],
            consultant_star: [],
            begin_org_foundation_date: '',
            end_org_foundation_date: '',
            is_reg: [],
            fund_status: [],
            begin_fund_foundation_date: '',
            end_fund_foundation_date: '',
            begin_new_nav_date: '',
            end_new_nav_date: '',
            issue_type: '',
            fund_star: [],
            date_type: '',
            returnType: 'section',
            sharpeType: 'section',
            drawdownType: 'section',
            fund_consultant: '',
            fund_manager: '',
            fund_name: '',
            reg_address: '',
            drawdown_end: '',
            drawdown_begin: '',
            sharpe_end: '',
            sharpe_begin: '',
            return_end: '',
            return_begin: ''
        }
    }

    toggleLoading = (status = false, tips= '在等一下～') => {
        this.setState({
             tips,
             loading: status
        })
    }
    
    
    componentWillUnmount () {
        window.__LOADING__ = null
    }

    componentDidMount () {
        window.__LOADING__ = this.toggleLoading
        let dls = document.getElementsByClassName('customer_selections')
        const _this = this
        let iList = document.querySelectorAll('.stars_lister_wrapper i')
            
        let selectType = []
        let dataStatus = []
        let consultantStar = []
        let dataReg = []
        let dataIssue = []
        let dataFundStar = []
        this._initData()

        Array.from(iList).forEach(item => {
            item.addEventListener('click', e => {
                e.target.setAttribute('class', 'stars_item_checked')
            }, false)
        })


        Array.from(dls).forEach(item => {
            
            item.addEventListener('click', function (e) {
                const eTarget = e.target.getAttribute('class')
                let consultantArr = []
                let typeArr = []

                
                if (eTarget === 'customer_selections' ||  eTarget === null) {
                    e.target.setAttribute('class', 'customer_selections customer_select_checked')
                    const type = e.target.getAttribute('data-type')
                    
                    if (type) {
                        selectType.push(type)
                    }

                    // 投资策略
                    if (e.target.getAttribute(`data-strategy`) !== '' && e.target.getAttribute(`data-strategy`) !== null) {
                        const filterType = e.target.getAttribute('data-strategy')
                        let strategy = _this.state.strategy_type
                        if (strategy.indexOf(filterType) !== -1) {
                            strategy.push(filterType)
                        }
                        
                        _this.setState({strategy_type: strategy})
                    }

                    //投顾星级
                    if (e.target.getAttribute(`data-consultant`) !== '' && e.target.getAttribute(`data-consultant`) !== null) {
                        consultantStar.push(e.target.getAttribute(`data-consultant`))
                        _this.setState({consultant_star: consultantStar})
                    }

                    // 备案状态
                    if (e.target.getAttribute('data-reg') !== '' && e.target.getAttribute('data-reg') !== null) {
                        dataReg.push(e.target.getAttribute('data-reg'))
                        _this.setState({is_reg: dataReg})
                    }

                    // 产品状态
                    if (e.target.getAttribute('data-status') !== '' && e.target.getAttribute('data-status') !== null) {
                        dataStatus.push(e.target.getAttribute('data-status'))
                        _this.setState({fund_status: dataStatus})
                    }

                    if (e.target.getAttribute('data-issue') !== '' && e.target.getAttribute('data-issue') !== null) {
                        dataIssue.push(e.target.getAttribute('data-issue'))
                        _this.setState({issue_type: dataIssue})
                    }

                    if (e.target.getAttribute('data-fund_star') !== '' && e.target.getAttribute('data-fund_star') !== null) {
                        dataFundStar.push(e.target.getAttribute('data-fund_star'))
                        _this.setState({fund_star: dataFundStar})
                    }

                    //
                    if (selectType.indexOf('stock') !== -1) {
                        typeArr.push('股票策略')
                        _this.setState({stock: true, asset_type: typeArr})
                    }

                    if (selectType.indexOf('marketing') !== -1) {
                        typeArr.push('市场中性')
                        _this.setState({marketing: true, asset_type: typeArr})
                    }

                    if (selectType.indexOf('bonds') !== -1) {
                        typeArr.push('债券')
                        _this.setState({bonds: true, asset_type: typeArr})
                    }

                    if (selectType.indexOf('future') !== -1) {
                        typeArr.push('期货')
                        _this.setState({future: true, asset_type: typeArr})
                    }

                    if (selectType.indexOf('options') !== -1) {
                        typeArr.push('期权')
                        _this.setState({options: true, asset_type: typeArr})
                    }


                    if (selectType.indexOf('abserve') !== -1) {
                        consultantArr.push('观察池')
                        _this.setState({consultant_level: consultantArr})
                    }

                    if (selectType.indexOf('bak') !== -1) {
                        consultantArr.push('储备池')
                        _this.setState({consultant_level: consultantArr})
                    }

                    if (selectType.indexOf('core') !== -1) {
                        consultantArr.push('核心池')
                        _this.setState({coreSelect: false, consultant_level: consultantArr})
                        let stars = document.getElementsByClassName('stars')
                        let consultantStar = []
        
                        stars.forEach(item => {item.addEventListener('click', (e) => {
                            const checkStatus = e.target.getAttribute('class')
                            if (checkStatus !== 'customer_select_checked') {
                                consultantStar.push(e.target.getAttribute('data-consultant'))
                                e.target.setAttribute('class', 'customer_select_checked')
                                _this.setState({consultant_star: consultantStar})
                            } else {
                                e.target.setAttribute('class', 'stars')
                                consultantStar.splice(consultantStar.indexOf(e.target.getAttribute('data-consultant')), 1)
                                _this.setState({consultant_star: consultantStar})
                            }
                            
                        }, false)})
                    }
                    

                } else {

                

                const index = selectType.indexOf(e.target.getAttribute('data-type'))

                if (e.target.getAttribute('data-type')) {
                    selectType.splice(index, 1)
                    _this.setState({marketing: true})
                }


                if (selectType.indexOf('stock') === -1) {
                    _this.setState({stock: false})
                }

                if (selectType.indexOf('marketing') === -1) {
                    _this.setState({marketing: false})
                }

                if (selectType.indexOf('bonds') === -1) {
                    _this.setState({bonds: false})
                }

                if (selectType.indexOf('future') === -1) {
                    _this.setState({future: false})
                }

                if (selectType.indexOf('options') === -1) {
                    _this.setState({options: false})
                }

                if (selectType.indexOf('core') === -1) {
                    _this.setState({coreSelect: true})
                }

                e.target.setAttribute('class', 'customer_selections')

                // 取消选择
                if (e.target.getAttribute('class') === 'customer_selections') {
                    console.log(e.target)
                    if (e.target.getAttribute('data-type') === 'stock') {
                        let stateArr = _this.state.asset_type
                        stateArr = stateArr.filter(item => item !== '股票策略')
                        _this.setState({asset_type: stateArr})
                    }

                    if (e.target.getAttribute('data-type') === 'marketing') {
                        let stateArr = _this.state.asset_type
                        stateArr = stateArr.filter(item => item !== '市场中性')
                        _this.setState({asset_type: stateArr})
                    }
                    
                    if (e.target.getAttribute('data-type') === 'bonds') {
                        let stateArr = _this.state.asset_type
                        stateArr = stateArr.filter(item => item !== '债券')
                        _this.setState({asset_type: stateArr})
                    }

                    if (e.target.getAttribute('data-type') === 'future') {
                        let stateArr = _this.state.asset_type
                        stateArr = stateArr.filter(item => item !== '期货')
                        _this.setState({asset_type: stateArr})
                    }

                    if (e.target.getAttribute('data-type') === 'options') {
                        let stateArr = _this.state.asset_type
                        stateArr = stateArr.filter(item => item !== '期权')
                        _this.setState({asset_type: stateArr})
                    }


                    //  //
                    if (e.target.getAttribute('data-type') === 'bak') {
                        console.log(consultantArr)
                        let stateArr = _this.state.consultant_level
                        stateArr = stateArr.filter(item => item !== '储备池')
                        _this.setState({consultant_level: stateArr})
                    }

                    if (e.target.getAttribute('data-type') === 'abserve') {
                        let stateArr = _this.state.consultant_level
                        stateArr = stateArr.filter(item => item !== '观察池')
                        _this.setState({consultant_level: stateArr})
                    }

                    if (e.target.getAttribute('data-type') === 'core') {
                        let stateArr = _this.state.consultant_level
                        stateArr = stateArr.filter(item => item !== '核心池')
                        _this.setState({consultant_level: stateArr, consultant_star: []})
                    }

                    if (e.target.getAttribute('data-strategy') !== null) {
                        const filterType = e.target.getAttribute('data-strategy')
                        let strategy = _this.state.strategy_type
                        strategy = strategy.filter( item => item !== filterType)
                        _this.setState({strategy_type: strategy})
                    }

                    if (e.target.getAttribute('data-reg') !== null) {
                        const regStat = e.target.getAttribute('data-reg')
                        let regArr = _this.state.is_reg
                        regArr = regArr.filter(item => item !== regStat)
                        _this.setState({is_reg: regArr})
                    }

                    if (e.target.getAttribute('data-issue') !== null) {
                        const regStat = e.target.getAttribute('data-issue')
                        let regArr = _this.state.issue_type
                        regArr = regArr.filter(item => item !== regStat)
                        _this.setState({issue_type: regArr})
                    }

                    if (e.target.getAttribute('data-fund_star') !== null) {
                        const regStat = e.target.getAttribute('data-fund_star')
                        let regArr = _this.state.issue_type
                        regArr = regArr.filter(item => item !== regStat)
                        _this.setState({fund_star: regArr})
                    }

                    if (e.target.getAttribute('data-status') !== null) {
                        const regStat = e.target.getAttribute('data-status')
                        let regArr = _this.state.fund_status
                        regArr = regArr.filter(item => item !== regStat)
                        _this.setState({fund_status: regArr})
                    }
                }
            }
                
            }, false)
        })
        
    }

    toggle = () => {
        this.setState({
            expend: !this.state.expend,
            selectionStatus: !this.state.selectionStatus
        })
    }

    backChange = val => {
        if (val === 'ranking') {
            this.setState({backN: false})
        } else {
            this.setState({backN: true})
        }
        this.setState({drawdownType: val})
    }

    sharpeChange = val => {
        if (val === 'ranking') {
            this.setState({sharpeN: false})
        } else {
            this.setState({sharpeN: true})
        }
        this.setState({sharpeType: val})
    }

    inChange = val => {
        if (val === 'ranking') {
            this.setState({changeN: false})
        } else {
            this.setState({changeN: true})
        }
        this.setState({returnType: val})
    }

    // 搜索开始
    searchProduct = () => {
        let thisState = this.state
        let searchParams = {
            token: cookie.get('token')
        }

        let return_begin = document.getElementById('return_begin').value || '0'
        let return_end = document.getElementById('return_end').value || '100'
        let sharpe_begin = document.getElementById('sharpe_begin').value || '0'
        let sharpe_end = document.getElementById('sharpe_end').value || '100'
        let drawdown_begin = document.getElementById('drawdown_begin').value || '0'
        let drawdown_end = document.getElementById('drawdown_end').value || '100'

        if (return_begin && thisState.selectionStatus) {
            searchParams['return_begin'] = return_begin
        }

        if (return_end && thisState.selectionStatus) {
            searchParams['return_end'] = return_end
        }

        if (sharpe_begin && thisState.selectionStatus) {
            searchParams['sharpe_begin'] = sharpe_begin
        }

        if (sharpe_end && thisState.selectionStatus) {
            searchParams['sharpe_end'] = sharpe_end
        }

        if (drawdown_begin && thisState.selectionStatus) {
            searchParams['drawdown_begin'] = drawdown_begin
        }

        if (drawdown_end && thisState.selectionStatus) {
            searchParams['drawdown_end'] = drawdown_end
        }

        if (thisState.asset_type.length > 0) {
            searchParams['asset_type'] = thisState.asset_type
        }

        if (thisState.strategy_type.length > 0 && thisState.strategy_type[0]!== null) {
            searchParams['strategy_type'] = thisState.strategy_type
        }

        if (thisState.consultant_level.length > 0 && thisState.consultant_level[0]!== null) {
            searchParams['consultant_level'] = thisState.consultant_level
        }

        if (thisState.consultant_star.length > 0 && thisState.consultant_star[0]!== null) {
            searchParams['consultant_star'] = thisState.consultant_star
        }

        if (thisState.begin_org_foundation_date) {
            searchParams['begin_org_foundation_date'] = thisState.begin_org_foundation_date
        }

        if (thisState.end_org_foundation_date) {
            searchParams['end_org_foundation_date'] = thisState.end_org_foundation_date
        }

        if (thisState.is_reg.length > 0 && thisState.is_reg[0]!== null) {
            searchParams['is_reg'] = thisState.is_reg
        }

        if (thisState.fund_status.length > 0 && thisState.fund_status[0]!== null) {
            searchParams['fund_status'] = thisState.fund_status
        }

        if (thisState.begin_fund_foundation_date) {
            searchParams['begin_fund_foundation_date'] = thisState.begin_fund_foundation_date
        }

        if (thisState.end_fund_foundation_date) {
            searchParams['end_fund_foundation_date'] = thisState.end_fund_foundation_date
        }

        if (thisState.begin_new_nav_date) {
            searchParams['begin_new_nav_date'] = thisState.begin_new_nav_date
        }

        if (thisState.end_new_nav_date) {
            searchParams['end_new_nav_date'] = thisState.end_new_nav_date
        }

        if (thisState.issue_type.length > 0 && thisState.issue_type[0]!== null) {
            searchParams['issue_type'] = thisState.issue_type
        }

        if (thisState.fund_star.length > 0 && thisState.fund_star[0]!== null) {
            searchParams['fund_star'] = thisState.fund_star
        }

        if (thisState.date_type) {
            searchParams['date_type'] = thisState.date_type
        }

        if (thisState.returnType && thisState.selectionStatus) {
            searchParams['returnType'] = thisState.returnType
        }

        if (thisState.sharpeType && thisState.selectionStatus) {
            searchParams['sharpeType'] = thisState.sharpeType
        }

        if (thisState.drawdownType && thisState.selectionStatus) {
            searchParams['drawdownType'] = thisState.drawdownType
        }



        let searchInput = document.getElementsByClassName('searchProduct')
        Array.from(searchInput).forEach(item => {
            if (item.value) {
                searchParams[item.getAttribute('data')] = item.value
            }
        })
        
        this.setState({searchParams})
        this._axiosData(searchParams)
        
    }

    beginDate = (e, str) => {
        this.setState({begin_org_foundation_date: new Date(str).getTime() / 1000})
    }

    endDate = (e, str) => { 
        this.setState({end_org_foundation_date: new Date(str).getTime() / 1000})
    }

    beginFund = (e, str) => {
        this.setState({begin_fund_foundation_date: new Date(str).getTime() / 1000})
    }
    endFund = (e, str) => {
        this.setState({end_fund_foundation_date: new Date(str).getTime() / 1000})
    }

    beginNav = (e, str) => {
        this.setState({begin_new_nav_date: new Date(str).getTime() / 1000})
    }

    endNav = (e, str) => {
        this.setState({end_new_nav_date: new Date(str).getTime() / 1000})
    }

    selectionCheck = e => {
        console.log(e)
        this.setState({date_type: e})
    }

    downloadExcel = () => {
        window.location.href=`http://192.168.122.190:8090/netFilter/hszcpz/index/exportExcel?token=${cookie.get('token')}`
    }

    attetionParent = () => {
        this.props.setAttentions()
    }

    getTableData = (params) => {
        // console.log(params)
        if (params) {
            window.__LOADING__(false)
            this.setState({dataSource: params})
            // console.log(this.state)
        }
    }

    _axiosData = (params) => {
        
        axios(window.__LOADING__)({
            // method: 'GET',  
            method: 'POST',  
            url: '/api/hszcpz/index/selectByScreen', 
            data: JSON.stringify({
                pageSize:  this.state.pageSize,
                pageNo: this.state.currentPage,
                ...params
            })
        })
        .then(data => {
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            // console.log(data)
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }
            data = JSON.parse(data.result)
            console.log(data)
            
            if (data.result === 'success') {
                let dataList = data.sumlist
                // console.log(data.sumlist)
                if (dataList) {
                    for (let i = 0; i < dataList.length; i++) {
                        dataList[i].key = i  + 1
                    }
                }
                
                this.setState({
                    dataSource: dataList,
                    totalPage: data.pageCount
                })
            } else {
            // console.log(data)
                if (data.code !== '200') {
                message.info(data.errMsg)
                } else {
                message.info('获取数据失败')
                }
            }
        })
    }


    _initData = () => {
        
        axios(window.__LOADING__)({
            method: 'POST',  
            url: '/api/hszcpz/index/selectInit', 
            data: JSON.stringify({
                pageSize:  this.state.pageSize,
                pageNo: this.state.currentPage,
                token: cookie.get('token')
            })
        })
        .then(data => {
            if (!data.hasOwnProperty('status')) {
                message.info('接口获取数据失败')
                return
            }
            
            this.setState({loading: false})
            // console.log(data)
            if (data.status !== 'success') {
                message.info(data.errorReason)
                return
            }
            if (!data.hasOwnProperty('result')) {
                message.info('接口获取数据失败')
                return
            }
            data = JSON.parse(data.result)
            console.log(data)
            
            if (data.result === 'success') {
                let dataList = data.sumlist
                if (dataList) {
                    for (let i = 0; i < dataList.length; i++) {
                        dataList[i].key = i  + 1
                    }
                }
                
                this.setState({
                    dataSource: dataList,
                    totalPage: data.pageCount
                })
            } else {
                if (data.code !== '200') {
                message.info(data.errMsg || '获取数据失败')
                } else {
                message.info('获取数据失败')
                }
            }
        })
    }

    topOptions = () => {
        let searchInput = document.getElementsByClassName('searchProduct')
        
        Array.from(searchInput).forEach(item => {
           item.value = ''
        })
    }
    
    resetState = () => {
        this.setState({
            asset_type: [],
            strategy_type: [],
            consultant_level: [],
            consultant_star: [],
            begin_org_foundation_date: '',
            end_org_foundation_date: '',
            is_reg: [],
            fund_status: [],
            begin_fund_foundation_date: '',
            end_fund_foundation_date: '',
            begin_new_nav_date: '',
            end_new_nav_date: '',
            issue_type: '',
            fund_star: [],
            date_type: '',
            returnType: '',
            sharpeType: 'section',
            drawdownType: '',
            fund_consultant: '',
            fund_manager: '',
            fund_name: '',
            reg_address: ''
        })
    }
    resetOptions = () => {
        let selectElement = document.getElementsByClassName('customer_select_checked')
        let dataInput = document.getElementsByClassName('ant-calendar-picker-input')
        let selectInput = document.getElementsByClassName('ant-select-selection-selected-value')
        Array.from(selectElement).forEach(item => {
            item.setAttribute('class', 'customer_selections')
            this.resetState()
        })

        Array.from(dataInput).forEach(item => item.value = '')
        Array.from(selectInput).forEach(item => item.innerHTML = '')
        
        document.getElementById('return_begin').value = ''
        document.getElementById('return_end').value  = ''
        document.getElementById('sharpe_begin').value  = ''
        document.getElementById('sharpe_end').value  = ''
        document.getElementById('drawdown_begin').value  = ''
        document.getElementById('drawdown_end').value  = ''
        this.topOptions()
    }

    render () {
    
        return (
            <div className={Search.tab_container}>
                <div className={Search.tab_content}>
                    <ul className={Search.tab_fixed_content}>
                        <li><Input className='searchProduct' data='fund_consultant' placeholder={'投顾/基金管理人名称'} /></li>
                        <li><Input className='searchProduct' data='fund_manager' placeholder={'投资经理/基金经理名称'} /></li>
                        <li><Input className='searchProduct' data='fund_name' placeholder={'产品名称'} /></li>
                        <li><Input className='searchProduct' data='reg_address' placeholder={'注册地址'} /></li>
                        <li><Button type="primary" onClick={() => {this.searchProduct()}}>搜索</Button></li>
                        <li><Button type="primary" onClick={ () => { this.resetOptions() }}>重置</Button></li>
                        <li><Button type="primary" onClick={this.toggle}>{this.state.expend ? '收起筛选项' : '展开筛选项'} <Icon type={this.state.expend ? 'up' : 'down'} /></Button></li>
                    </ul>

                    <div className={Search.tab_slider_content} style={{display: this.state.expend ? 'block' : 'none'}}>
                        <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: '80px'}}><span className={Search.tab_column_tit}>大类资产：</span></Col>
                            <Col span={12}>
                                <dl className='clear-fix'>
                                    <dt>不限</dt>
                                    <dd data-type='stock' className='customer_selections'>股票</dd>
                                    <dd data-type='marketing' className='customer_selections'>市场中性</dd>
                                    <dd data-type='bonds' className='customer_selections'>债券</dd>
                                    <dd data-type='future' className='customer_selections'>期货</dd>
                                    <dd data-type='options' className='customer_selections'>期权</dd>
                                </dl>
                            </Col>
                        </Row>

                        <Row className={Search.tab_tit_row} type='flex' justify='start'>
                            <Col style={{width: '80px'}}><span className={Search.tab_column_tit}>投资策略：</span></Col>
                            <Col>
                                <dl className='clear-fix'>
                                    <dt>不限</dt>
                                </dl>
                            </Col>
                            <Col>
                                <ul style={{display: this.state.stock ? 'block': 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>股票</li>
                                    <li data-strategy='主观权益' className='customer_selections'>主观权益</li>
                                    <li data-strategy='量化选股' className='customer_selections'>量化选股</li>
                                    <li data-strategy='指数增强' className='customer_selections'>指数增强</li>
                                    <li data-strategy='ETF' className='customer_selections'>ETF</li>
                                    <li data-strategy='期货' className='customer_selections'>期货</li>
                                </ul>

                                <ul style={{display: this.state.marketing ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>市场中性</li>
                                    <li data-strategy='TO' className='customer_selections'>TO</li>
                                    <li data-strategy='高频' className='customer_selections'>高频</li>
                                    <li data-strategy='低频' className='customer_selections'>低频</li>
                                </ul>

                                <ul style={{display: this.state.bonds ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>债券</li>
                                    <li data-strategy='利率和高等级信用债' className='customer_selections'>利率和高等级信用债</li>
                                    <li data-strategy='低等级信用债' className='customer_selections'>低等级信用债</li>
                                    <li data-strategy='可转债' className='customer_selections'>可转债</li>
                                </ul>

                                <ul style={{display: this.state.future ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>期货</li>
                                    <li data-strategy='高频' className='customer_selections'>高频</li>
                                    <li data-strategy='中高频' className='customer_selections'>中高频</li>
                                    <li data-strategy='低频' className='customer_selections'>低频</li>
                                </ul>

                                <ul style={{display: this.state.options ? 'block' : 'none'}} className={`${Search.list_options} clear-fix`}>
                                    <li>期权</li>
                                    <li data-strategy='波动率曲面套利' className='customer_selections'>波动率曲面套利</li>
                                    <li data-strategy='做市套利' className='customer_selections'>做市套利</li>
                                    <li data-strategy='择时' className='customer_selections'>择时</li>
                                </ul>
                                
                                <div style={{display: (!(this.state.options) && 
                                    !(this.state.bonds) && 
                                    !(this.state.future) && 
                                    !(this.state.marketing) && 
                                    !(this.state.stock)) ? 'block' : 'none'}} className={Search.core_rating}>请选择大类资产</div>
                            </Col>
                        </Row>


                        <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: '80px'}}><span className={Search.tab_column_tit}>投顾层级：</span></Col>
                            <Col>
                                <span>投顾成立时间：</span>
                                <DatePicker locale={locale} onChange={this.beginDate} style={{width: '100px'}} className={Search.tab_search_radius} placeholder='起始日期' />
                                <span className={Search.split_tit}></span>
                                <DatePicker locale={locale}  onChange={this.endDate} style={{width: '100px'}} className={Search.tab_search_radius} placeholder='截止日期' />
                                
                            </Col>
                            <Col className={Search.line_split_tab}>
                                <dl className='clear-fix'>
                                    <dt>不限</dt>
                                    <dd data-type='core' className='customer_selections'>核心池</dd>
                                    <dd data-type='abserve' className='customer_selections'>观察池</dd>
                                    <dd data-type='bak' className='customer_selections'>储备池</dd>
                                </dl>
                            </Col>

                            <Col className={Search.line_split_tab}>
                                <span className={Search.tab_normal_tit}>投顾星级：</span>
                                {
                                    (<div className={`${Search.stars_wrapper} stars_lister_wrapper`}>
                                        <dl className='clear-fix'>
                                            <dd data-consultant='1' className='stars'>一星</dd>
                                            <dd data-consultant='2' className='stars'>二星</dd>
                                            <dd data-consultant='3' className='stars'>三星</dd>
                                            <dd data-consultant='4' className='stars'>四星</dd>
                                            <dd data-consultant='5' className='stars'>五星</dd>
                                        </dl>
                                    </div>)
                                }
                            </Col>

                            
                        </Row>

                        <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: '110px'}}><span className={Search.tab_column_tit}>产品运行状态：</span></Col>
                            <Col>
                                <dl className='clear-fix'>
                                    <dt>不限</dt>
                                    <dd data-status='运行中' className='customer_selections'>续存中</dd>
                                    <dd data-status='终止' className='customer_selections'>已终止</dd>
                                </dl>
                            </Col>
                            <Col className={Search.line_split_tab}>
                                <span>最新净值日期：</span>
                                <Select defaultValue='year-今年以来' onChange={this.selectionCheck} className={Search.selection_content}>
                                    <Option value="year-今年以来">今年以来</Option>
                                    <Option value="m1-近一月">近一个月</Option>
                                    <Option value="m3-近三月">近三月</Option>
                                    <Option value="m6-近六月、">近六月</Option>
                                    <Option value="y1-近一年">近一年</Option>
                                    <Option value="y2-近两年">近二年</Option>
                                    <Option value="y3-近三年">近三年</Option>
                                    <Option value="y5-近五年">近五年</Option>
                                    <Option value="total-成立以来">成立以来</Option>
                                </Select>
                            </Col>

                            <Col className={Search.line_split_tab}>
                                <span>产品成立时间：</span>
                                <DatePicker locale={locale} onChange={this.beginFund} className={Search.tab_search_radius} placeholder='起始日期' />
                                <span className={Search.split_tit}></span>
                                <DatePicker locale={locale} onChange={this.endFund} className={Search.tab_search_radius} placeholder='截止日期' />
                            </Col>

                            {/* <Col className={Search.line_split_tab}>
                                <span className={Search.tab_normal_tit}>产品运行状态：</span>
                                <dl className={`${Search.tab_float_left} clear-fix`}>
                                    <dt>不限</dt>
                                    <dd data-status='续存中' className='customer_selections'>续存中</dd>
                                    <dd data-status='已终止' className='customer_selections'>已终止</dd>
                                </dl>
                            </Col> */}
                        </Row>

                        {/* <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: '110px'}}><span className={Search.tab_column_tit}>产品成立时间：</span></Col>

                            <Col>
                                <DatePicker locale={locale} onChange={this.beginFund} className={Search.tab_search_radius} placeholder='起始日期' />
                                <span className={Search.split_tit}></span>
                                <DatePicker locale={locale} onChange={this.endFund} className={Search.tab_search_radius} placeholder='截止日期' />
                            </Col>

                            <Col className={Search.line_split_tab}>
                                <span>最新净值日期：</span>
                                <DatePicker locale={locale} onChange={this.beginNav} className={Search.tab_search_radius} placeholder='起始日期' />
                                <span className={Search.split_tit}></span>
                                <DatePicker locale={locale} onChange={this.endNav} className={Search.tab_search_radius} placeholder='截止日期' />
                            </Col>
                        </Row> */}

                        <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: '110px'}}><span className={Search.tab_column_tit}>产品发行方式：</span></Col>
                            <Col>
                                <dl className='clear-fix'>
                                    <dt>不限</dt>
                                    <dd data-issue='私募基金' className='customer_selections'>私募基金</dd>
                                    <dd data-issue='公募专户' className='customer_selections'>公募专户</dd>
                                    <dd data-issue='券商资管' className='customer_selections'>券商资管</dd>
                                    <dd data-issue='信托' className='customer_selections'>信托</dd>
                                    <dd data-issue='期货资管' className='customer_selections'>期货资管</dd>
                                    <dd data-issue='保险及子公司' className='customer_selections'>保险及子公司</dd>
                                </dl>
                            </Col>

                            <Col className={Search.line_split_tab}>
                                <span className={Search.tab_normal_tit}>产品星级：</span>
                                <div className={`${Search.stars_wrapper} stars_lister_wrapper`}>
                                    <dl className='clear-fix'>
                                        <dd data-fund_star='1' className='customer_selections'>一星</dd>
                                        <dd data-fund_star='2' className='customer_selections'>二星</dd>
                                        <dd data-fund_star='3' className='customer_selections'>三星</dd>
                                        <dd data-fund_star='4' className='customer_selections'>四星</dd>
                                        <dd data-fund_star='5' className='customer_selections'>五星</dd>
                                    </dl>
                                </div>
                            </Col>
                        </Row>

                        <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col style={{width: '122px'}}><span className={Search.tab_column_tit}>收益风险及区间：</span></Col>
                            <Col>
                                <Row></Row>
                                <div className={Search.tab_selection_wrapper}>
                                    <span className={Search.tab_selections_tit}>收益风险区间：</span>
                                    <Select defaultValue='year-今年以来' onChange={this.selectionCheck} className={Search.selection_content}>
                                        <Option value="year-今年以来">今年以来</Option>
                                        <Option value="m1-近一月">近一个月</Option>
                                        <Option value="m3-近三月">近三月</Option>
                                        <Option value="m6-近六月、">近六月</Option>
                                        <Option value="y1-近一年">近一年</Option>
                                        <Option value="y2-近两年">近二年</Option>
                                        <Option value="y3-近三年">近三年</Option>
                                        <Option value="y5-近五年">近五年</Option>
                                        <Option value="total-成立以来">成立以来</Option>
                                    </Select>
                                </div>

                                <div className={Search.tab_selection_wrapper}>
                                    <span className={Search.tab_selections_tit}>收益：</span>
                                    <Select defaultValue='section' onChange={this.inChange} className={Search.selection_content}>
                                        <Option value="section">绝对收益区间</Option>
                                        <Option value="ranking">收益市场排名</Option>
                                    </Select>
                                    <Input id='return_begin' className={Search.percent_tab_input} />
                                    <span>{this.state.changeN ? '%' : '/N'}</span>
                                    <span className={Search.selection_percent_split}>~</span>
                                    <Input id='return_end' className={Search.percent_tab_input} /><span>{this.state.changeN ? '%' : '/N'}</span>
                                </div>

                                <div className={Search.tab_selection_wrapper}>
                                    <span className={Search.tab_selections_tit}>Sharpe比率：</span>
                                    <Select defaultValue='section' onChange={this.sharpeChange} className={Search.selection_content}>
                                        <Option value="section">绝对Sharpe比区间</Option>
                                        <Option value="ranking">Sharpe比市场排名</Option>
                                    </Select>
                                    <Input id='sharpe_begin' className={Search.percent_tab_input} />
                                    <span>{this.state.sharpeN ? '%' : '/N'}</span>
                                    <span className={Search.selection_percent_split}>~</span>
                                    <Input id='sharpe_end' className={Search.percent_tab_input} /><span>{this.state.sharpeN ? '%' : '/N'}</span>
                                </div>
                            </Col>

                            <Col>
                                <div>
                                    <span className={Search.tab_selections_tit}>回撤：</span>
                                    <Select defaultValue='section' onChange={this.backChange} className={Search.selection_content}>
                                        <Option value="section">绝对回撤区间</Option>
                                        <Option value="ranking">回撤市场排名</Option>
                                    </Select>
                                    <Input id='drawdown_begin' className={Search.percent_tab_input} />
                                    <span>{this.state.backN ? '%' : '/N'}</span>
                                    <span className={Search.selection_percent_split}>~</span>
                                    <Input id='drawdown_end' className={Search.percent_tab_input} /><span>{this.state.backN ? '%' : '/N'}</span>
                                </div>
                            </Col>
                        </Row>

                        {/* <Row type='flex' className={Search.tab_tit_row} justify='start'>
                            <Col>
                                <Button type='primary' style={{marginRight: '20px'}}>确定</Button>
                                <Button type='primary'>重置</Button>
                            </Col>
                        </Row> */}
                    </div>
                </div>

                <div className={Search.slider_container}>
                    <div className={Search.tab_buttons}>
                        <ul className='clear-fix'>
                            <li>产品表现</li>
                            <li>
                                <Button type="primary" onClick={() => {this.downloadExcel()}}><Icon type="download" /> 下载 </Button>
                            </li>
                        </ul>
                    </div>
                    <HomeTable getTableData={this.getTableData} attetionParent={this.props.setAttentions} {...this.state} />
                </div>
            </div>
        )
    }
}