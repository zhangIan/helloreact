import React, { Component } from 'react'
import LogoImg from '../../Image/logo_2.png'
import IHeader from './inner_header.module.sass'


export default class InnerHeader extends Component {
    constructor (props) {
        super(props)
        this.state ={
            navList: [
                {
                    name: '资产策略研究',
                    link: '/'
                },
                {
                    name: '投顾产品挖掘',
                    link: '/'
                },
                {
                    name: '组合配置策略',
                    link: '/'
                },
                {
                    name: '投后管理监控',
                    link: '/'
                },
                {
                    name: '配置流程管理',
                    link: '/'
                }
            ],
            pageName: '资产研究配置'
        }
    }

    componentDidMount () {}

    render () {
        const { navList } = this.props.navList || this.state
        return (
            <header className={`${IHeader.header_container} clear-fix`}>
                <div className={`${IHeader.header_content} clear-fix`}>
                    <div className={IHeader.logo_img}>
                        <img src={LogoImg} alt="logo img" />
                        <h2>{this.state.pageName}</h2>
                    </div>
                    <div className={IHeader.nav_list}>
                        <ul className="clear-fix">
                            {
                                navList.map((item, index) => (
                                    <li key={`nav_${index}`}><a href={item.link}>{item.name}</a></li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </header>
        )
    }
}