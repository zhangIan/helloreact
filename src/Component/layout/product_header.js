import React, { Component } from 'react'
import { Layout, Menu, Avatar, Icon, Badge, Dropdown } from 'antd'
import Inner from './product.module.sass'
import LogoImg from '../../Image/logo.svg'
import { axios } from '../../Utils/index'
import cookie from 'js-cookie'


const { Header } = Layout


export default class InnerHead extends Component {
    constructor (props) {
        super(props)
        this.state ={
            navList: [
                {
                    name: '资产策略研究',
                    link: '/',
                    iconClass: 'iconfont assets iconzichan',
                    subMenu: [
                        {
                            name: '大类资产研究1',
                            link: '/'
                        },
                        {
                            name: '因子研究',
                            link: '/'
                        },
                        {
                            name: '策略研究',
                            link: '/'
                        },
                        {
                            name: '产品研究',
                            link: '/'
                        }
                    ]
                },
                {
                    name: '投顾产品挖掘',
                    link: '/',
                    iconClass: 'iconfont advisor iconzichanshaixuan',
                    subMenu: []
                },
                {
                    name: '组合配置策略',
                    link: '/',
                    iconClass: 'iconfont group iconjuecebiao',
                    subMenu: [
                        {
                            name: '关注3',
                            link: '/'
                        },
                        {
                            name: '产品3',
                            link: '/'
                        }
                    ]
                },
                {
                    name: '投后管理监控',
                    link: '/',
                    iconClass: 'iconfont abserve iconjianguan1',
                    subMenu: [
                        {
                            name: '关注4',
                            link: '/'
                        },
                        {
                            name: '产品4',
                            link: '/'
                        }
                    ]
                },
                {
                    name: '配置流程管理',
                    link: '/',
                    iconClass: 'iconfont reset iconliuchengguanli',
                    subMenu: [
                        {
                            name: '关注5',
                            link: '/'
                        },
                        {
                            name: '产品5',
                            link: '/'
                        }
                    ]
                }
            ],
            pageName: '资产研究配置',
            messageCount: '5',
            defaultKey: '1',
            subKey: 0
        }
    }

    handlerClick = e => {
        this.setState({
            subKey: e.key
        })
    }

    logout () {
        axios({url: '/api/sso/account/loginOut', method: 'POST', data: JSON.stringify({token: cookie.get('token')})})
        .then(res => {
            cookie.remove('token')
            window.localStorage.setItem('permission', '')
            window.location.href='/first-login'
        })
    }

    linkTo (link) {
        window.location.href = link
    }

    render () {
        const menuList = this.props.navList || this.state.navList
        const userInfo = JSON.parse(window.localStorage.getItem('userInfo'))
        const menu = (
            <Menu>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
                    消息1
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">
                    消息2
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">
                    消息3
                </a>
              </Menu.Item>
            </Menu>
          )

          const userMenu = (
            <Menu>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">
                    个人中心
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href={`/system-list`}>
                    登录中心
                </a>
              </Menu.Item>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href={`${process.env.REACT_APP_BACK_URL}?token=${cookie.get('token')}`}>
                    后台管理
                </a>
              </Menu.Item>
              <Menu.Item>
                <span rel="noopener noreferrer" onClick={this.logout.bind(this)}>
                    退出
                </span>
              </Menu.Item>
            </Menu>
          )
        return (
            <div>
                <Header className={Inner.nav_container}>
                    <div className={Inner.header_logo}>
                        <img src={LogoImg} alt="logo img" />
                        <h2>{this.state.pageName}</h2>
                    </div>
                    <div className={Inner.header_avatar}>
                        <Dropdown overlay={menu} placement="bottomCenter">
                            <Badge dot count={this.state.messageCount} className={Inner.header_message}>
                                <span className={`iconfont iconxiaoxi1 ${Inner.icon_bell}`}></span>
                            </Badge>
                        </Dropdown>
                        <Dropdown overlay={userMenu} placement="bottomCenter">
                            <Avatar className={Inner.user_avart} size='large' src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"></Avatar>
                        </Dropdown>
                        <span className={Inner.header_username}>{userInfo ? userInfo.userName ? userInfo.userName: '游客您好' : '游客您好'}</span>
                        <Icon className={Inner.header_change_menu} type='swap' />
                    </div>
                    <div>
                        <Menu
                        theme='dark'
                        onClick={this.handlerClick}
                        mode='horizontal'
                        className={Inner.header_menu_items}
                        defaultSelectedKeys={[this.state.defaultKey]}>
                            {
                                menuList.map((item, index) => (
                                    <Menu.Item key={index} className={Inner.position_relative} onClick={() => {this.linkTo(item.link)}}>
                                        <span className={`${item.iconClass}`}></span>
                                        <span>{item.name}</span>
                                        <div style={{display: item.subMenu.length > 0 ? 'block' : 'none', marginBottom: '50px'}} className={`position_sub_menu`}>
                                            {
                                                item.subMenu.map((itm, ix) => (<span key={ix}>{itm.name}</span>))
                                            }
                                        </div>
                                    </Menu.Item>
                                ))
                            }
                            
                        </Menu>
                    </div>
                    
                </Header>
                <div className={Inner.sub_menus}>
                </div>
            </div>
        )
    }
}
