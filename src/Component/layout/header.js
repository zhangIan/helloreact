import React, { Component } from 'react'
import LogoImg from '../../Image/logo.png'

export default class Header extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    componentDidMount () {}

    render () {
        return (
            <header className="container-header">
                <div className="header-content">
                    <div className="logo-img">
                        <img src={LogoImg} alt="logo img" />
                    </div>
                    <div className="nav-list">
                        <ul className="clear-fix">
                            <li><a href="/link">基金管理</a></li>
                            <li><a href="/link">投资管理</a></li>
                            <li><a href="/link">资产配置</a></li>
                            <li><a href="/link">投后管理</a></li>
                        </ul>
                    </div>
                </div>
            </header>
        )
    }
}
