import React, { Component } from 'react'
import { Tabs, Checkbox } from 'antd'
import ComForm from '../common/form'
import Qrcode from '../../Image/qrcode.png'
import  Register from './first.module.sass'
import './login.sass'
import Logo from '../../Image/logo_2.png'

const { TabPane } = Tabs
export default class FirstRegister extends Component {
    constructor (props) {
        super(props)
        this.state = {
            size: 'small',
            defaultActiveKey: '1',
            agreeStatus: false,
            standStatus: false
        }
    }

    change = key => {
        this.setState({
            defaultActiveKey: key
        })
    }
    UNSAFE_componentWillMount () {
        document.getElementsByTagName('body')[0].setAttribute('class', 'first_login')
    }
    componentDidMount () {}
    componentWillUnmount () {
        document.getElementsByTagName('body')[0].setAttribute('class', '')
    }
    pushLink () {
        this.props.history.push('/home')
    }

    checkRadio (e) {
        const status = e.target.checked
        this.setState({agreeStatus: status})
    }

    standCheck (e) {
        const status = e.target.checked
        this.setState({standStatus: status})
    }
    render () {
        return (
            <div className={`${Register.first_register} first_login`}>
                <header>
                    <div className={Register.header_logo}>
                        <div className={Register.logo_img}>
                            <img src={Logo} alt="company_logo" />
                        </div>
                        <div className={Register.logo_name}>
                            <h1 onClick={() => { this.pushLink() }}>资产研究和配置系统</h1>
                        </div>
                    </div>
                </header>
                <section className={Register.register_modal}>
                        <Tabs className={Register.login_tabs} activeKey={this.state.defaultActiveKey} onChange={this.change} size={this.state.size}>
                            <TabPane tab="用户名密码登录" key="1" className="common_form">
                                <ComForm {...this.state} {...this.props} />
                                <ul className={Register.radio_list}>
                                        <li>
                                            <Checkbox onClick={this.checkRadio.bind(this)} data-check='userAgree' className="agree-radio"  />
                                            <p className='color-fff font-13'>我已阅读并同意汇升投资<a className='color-fff' href="www.baidu.com">《用户服务协议》</a>及<a className='color-fff' href="www.baidu.com">《隐私保护指引》</a></p>
                                        </li>
                                        <li>
                                            <Checkbox onChange={this.standCheck.bind(this)} data-check='cert' className="agree_radio"  />
                                            <p className="color-fff font-13">本人承诺符合特定的合格投资者条件</p>
                                        </li>
                                        <li>
                                            <span></span>
                                            <p className="color-fff font-12">根据《私募投资基金募集行为管理办法》之规定，汇升投资只对“具有相应风险识别能力和风险承担能力，投资于单只私募基金的金额不低于100万元，且个人金融资产不低于300万元或者最近三年个人年均收入不低于50万元”的特定投资者宣传、推介相关私募投资基金产品。您需要进行身份认证后才能访问。</p>
                                        </li>
                                </ul>
                            </TabPane>
                            <TabPane tab="手机扫码登录" key="2" className="common_form">
                                <div className="qrcode-scan">
                                    <div className="scan-img">
                                        <img src= {Qrcode} alt="scan" />
                                    </div>
                                </div>
                                <div className="agree-desc">
                                    <ul className={Register.radio_list}>
                                            <li>
                                                <Checkbox onClick={this.checkRadio.bind(this)} className="agree-radio"  />
                                                <p className='color-fff font-13'>我已阅读并同意汇升投资<a className='color-fff' href="www.baidu.com">《用户服务协议》</a>及<a className='color-fff' href="www.baidu.com">《隐私保护指引》</a></p>
                                            </li>
                                            <li>
                                                <Checkbox onChange={this.standCheck.bind(this)} className="agree_radio"  />
                                                <p className="color-fff font-13">本人承诺符合特定的合格投资者条件</p>
                                            </li>
                                            <li>
                                                <span></span>
                                                <p className="color-fff font-12">根据《私募投资基金募集行为管理办法》之规定，汇升投资只对“具有相应风险识别能力和风险承担能力，投资于单只私募基金的金额不低于100万元，且个人金融资产不低于300万元或者最近三年个人年均收入不低于50万元”的特定投资者宣传、推介相关私募投资基金产品。您需要进行身份认证后才能访问。</p>
                                            </li>
                                    </ul>
                                </div>
                            </TabPane>
                        </Tabs>
                </section>
            </div>
        )
    }
}

