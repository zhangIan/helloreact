import React, { Component } from 'react'
import './manager.sass'
import ManagerStyle from './manager.module.sass'
import InnerHeader from '../layout/product_header'
import { Layout, Table, message } from 'antd'
import ManagerTitle from '../common/manager/userInfo'
import WrapperHeader from '../common/details/wrapper_header'
import HomeTable from '../common/common_table/index_table'
import { axios } from '../../Utils/index'
import url from 'url'
import cookie from 'js-cookie'
import moment from 'moment'
import CommonCharts from '../common/charts/common_charts'
import { chartsColors } from '../../Utils/charts_config/charts_config'



const { Content } = Layout

export default class Manager extends Component {
    constructor (props) {
        super(props)
        this.state = {
            columns: [
                {
                    dataIndex: 'date',
                    title: <span className={`${ManagerStyle.table_columns_tit}`}>任职时间</span>,
                    key: '1'
                },
                {
                    dataIndex: 'c_org_name',
                    title: <span className={`${ManagerStyle.table_columns_tit}`}>任职单位</span>,
                    key: '2'
                },
                {
                    dataIndex: 'duty',
                    title: <span className={`${ManagerStyle.table_columns_tit}`}>职务</span>,
                    key: '3'
                }
            ]
        }
    }

    // 初始化
    async _initManagerData () {
        const { query } = url.parse(window.location.href, true)
        const data = await axios({
            method: 'POST',
            url: '/api/hszcpz/investment/init',
            data: JSON.stringify({
                token: cookie.get('token'),
                c_fund_id: query.id
            })
        })

        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)

        if (result.result === 'success') {
            // console.log(result)
            return result.investmentM
        } else {
            message.error('获取数据失败')
        }

    }

    // table 个人履历
    async _personIntroduce () {
        let { query } = url.parse(window.location.href, true)
        const data = await axios({
            method: 'POST',
            url: '/api/hszcpz/investment/findPersonResume',
            data: JSON.stringify({
                token: cookie.get('token'),
                c_person_id: query.id
            })
        })
        // console.log(data)

        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)

        if (result.result === 'success') {
            // console.log(result)
            return result.personResume
        } else {
            message.error('获取数据失败')
        }
    }

    // 旗下产品
    async _getSubjectOrg (parmas) {
        let { query } = url.parse(window.location.href, true)
        const data = await axios({
            method: 'POST',
            url: '/api/hszcpz/investment/invManagerFunds',
            data: JSON.stringify({
                token: cookie.get('token'),
                c_person_id: query.id,
                ...parmas
            })
        })
        // console.log(data)

        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)

        if (result.result === 'success') {
            // console.log(result)
            return result
        } else {
            message.error('获取数据失败')
        }
    }

    // 个人概况调整
    async _managerCharts (parmas) {
        let { query } = url.parse(window.location.href, true)
        const data = await axios({
            method: 'POST',
            url: '/api/hszcpz/investment/personProfile',
            data: JSON.stringify({
                token: cookie.get('token'),
                c_person_id: query.id,
                ...parmas
            })
        })
        // console.log(data)

        message.destroy()
        if (!data.hasOwnProperty('status')) {
            message.info('接口获取数据失败')
            return
        }

        this.setState({loading: false})
        if (data.status !== 'success') {
            message.info(data.errorReason)
            return
        }
        if (!data.hasOwnProperty('result')) {
            message.info('接口获取数据失败')
            return
        }

        let result  = JSON.parse(data.result)

        if (result.result === 'success') {
            // console.log(result)
            return result.personProfile
        } else {
            message.error('获取数据失败')
        } 
    }

    async _setCharts () {
        let data = await this._managerCharts()
        
        let yearArr = [], avgYear =[], workYear = [], avgWork = [], productArr = [], avgProduct = [], starArr = [], avgStar = [], legend =[]
        if (data) {
            yearArr.push(data.years || 0)
            avgYear.push(data.avgWorkingSeniority || 0)
            workYear.push(data.workingDay || 0)
            avgWork.push(data.avgWorkingDay || 0)
            productArr.push(data.productCount || 0)
            avgProduct.push(data.avgProductCount || 0)
            starArr.push(data.fourProductCount || 0)
            avgStar.push(data.avgFourProductCount || 0)
            legend.push(data.c_person_name)
        } else {
            data = {c_person_name: ''}
        }


        let managerOptions = {
            
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'none'
                }
            },
            legend: [{
                data: [data.c_person_name ? data.c_person_name : '','私募平均'],
                bottom: 0,
                itemWidth: 13,
                itemHeight: 13,
                }
            ],
            yAxis: [
                    {
                        type: 'value',
                        gridIndex: 0,
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: '#ccc',
                                type: 'dashed'
                            }
                        },
                        axisTick: { show: false },
                        axisLine: { show: false },
                        axisLabel: {show: false },
                        max: 60,
                        interval: 60 / 5,
                    },
                    {
                        type: 'value',
                        gridIndex: 1,
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: '#ccc',
                                type: 'dashed'
                            }
                        },
                        axisTick: { show: false },
                        axisLine: { show: false },
                        axisLabel: {show: false },
                        max: 10000,
                        interval: 10000 / 5,
                    },{
                        type: 'value',
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: '#ccc',
                                type: 'dashed'
                            }
                        },
                        axisTick: { show: false },
                        axisLine: { show: false },
                        gridIndex: 2,
                        axisLabel: {show: false },
                        max: 500,
                        interval: 500 / 5,
                    },{
                        type: 'value',
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: '#ccc',
                                type: 'dashed'
                            }
                        },
                        axisTick: { show: false },
                        axisLine: { show: false },
                        gridIndex: 3,
                        axisLabel: {show: false },
                        max: 100,
                        interval: 100 / 5,
                    },
            ],
            xAxis: [
                {
                    axisTick: {show: false},//去掉间隔线
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#ccc',
                            width: 1,
                            type: 'solid'
                        }
                    },//去掉轴线
                    data: ['从业年限'],
                    type: 'category',
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#999'
                        }
                    },
                    gridIndex: 0
                },
                {
                    axisTick: {show: false}, //去掉间隔线
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#ccc',
                            width: 1,
                            type: 'solid'
                        }
                    }, //去掉轴线
                    type: 'category',
                    data: ['当前公司任职天数'],
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#999'
                        }
                    },
                    gridIndex: 1
                },
                {
                    axisTick: {show: false},//去掉间隔线
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#ccc',
                            width: 1,
                            type: 'solid'
                        }
                    },//去掉轴线
                    data: ['产品数量（续存中）'],
                    type: 'category',
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#999'
                        }
                    },
                    gridIndex: 2
                },
                {
                    axisTick: {show: false},//去掉间隔线
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#ccc',
                            width: 1,
                            type: 'solid'
                        }
                    },//去掉轴线
                    type: 'category',
                    data: ['四星以上产品数（续存中）'],
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#999'
                        }
                    },
                    gridIndex: 3
                }
            ],
            grid: [
                {
                    top:'6%',
                    bottom: '10%',
                    left:'0%',
                    width: '25%'
                },
                {
                    top:'6%',
                    bottom: '10%',
                    width: '25%',
                    left:'25%'
                },
                {
                    top:'6%',
                    bottom: '10%',
                    width: '25%',
                    left: '50%'
                },
                {
                    top:'6%',
                    bottom: '10%',
                    width: '25%',
                    left: '75%'
                }
            ],
            series: [
                // 1
                {	
                    name: data.c_person_name ? data.c_person_name : '',
                    type: 'bar',
                    data: yearArr || [0],
                    itemStyle: {
                        normal: {
                            color: chartsColors[3],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    }
                }, 
                {	
                    name: '私募平均',
                    type: 'bar',
                    data: avgYear || [0],
                    itemStyle: {
                        normal: {
                            color: chartsColors[4],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    }
                }, 
                // 2
                {
                    name: data ? data.c_person_name ? data.c_person_name : '' : '',
                    type: 'bar',
                    data: workYear || [0],
                    itemStyle: {
                        normal: {
                            color: chartsColors[3],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    xAxisIndex: 1,
                    yAxisIndex: 1
                },
                {
                    name: '私募平均',
                    type: 'bar',
                    data: avgWork || [0],
                    itemStyle: {
                        normal: {
                            color: chartsColors[4],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    xAxisIndex: 1,
                    yAxisIndex: 1
                },
                // 3
                {
                    name: data.c_person_name ? data.c_person_name : '',
                    type: 'bar',
                    data: productArr || [0],
                    itemStyle: {
                        normal: {
                            color: chartsColors[3],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    xAxisIndex: 2,
                    yAxisIndex: 2
                },
                {
                    name: '私募平均',
                    type: 'bar',
                    data: avgProduct || [0],
                    itemStyle: {
                        normal: {
                            color:  chartsColors[4],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    xAxisIndex: 2,
                    yAxisIndex: 2
                },
                // 4
                {
                    name: data.c_person_name ? data.c_person_name : '',
                    type: 'bar',
                    data: starArr || [0],
                    itemStyle: {
                        normal: {
                            color:  chartsColors[3],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    xAxisIndex: 3,
                    yAxisIndex: 3
                },
                {
                    name: '私募平均',
                    type: 'bar',
                    data: avgStar || [0],
                    itemStyle: {
                        normal: {
                            color: chartsColors[4],
                            shadowColor: 'rgba(0, 0, 0, 0.2)',
                            shadowOffsetY: 2,
                            shadowOffsetX: 2,
                            shadowBlur: 5,
                            barBorderRadius: 4,
                        }
                    },
                    barWidth: '40',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    xAxisIndex: 3,
                    yAxisIndex: 3
                }
            ]
        }
        
        this.refs.managerCharts.setOpitions(managerOptions)
    }
    async componentDidMount () {
        document.getElementsByTagName('body')[0].setAttribute('class', 'manager_wrapper')
        await this._setCharts()
    }

    async UNSAFE_componentWillMount () {
       const dataSource = await this._personIntroduce()
       if (dataSource && Array.isArray(dataSource)) {
           let data = []
           dataSource.forEach((item, index) => {
               data.push({
                key: index,
                date: `${item.t_tenure_date ? moment(item.t_tenure_date).format('YYYY-MM') : '--'} ~ ${item.t_dimission_date ? moment(item.t_dimission_date).format('YYYY-MM') : '--'}`,
                duty: item.c_duty,
                c_org_name: item.c_org_name
               })
           })

           this.setState({dataSource: data})
       }

       await this._managerCharts()
    }

    

    render () {
        const { columns, dataSource } = this.state
        return (
            <div className={`manager_wrapper`}>
                <Layout>
                    <InnerHeader />
                    <Content className={`mt_20 manager_container`}>
                        <ManagerTitle _getDetails={this._initManagerData.bind(this)} />
                        <div className={`manager_infos clear-fix`}>
                            <div className={`float_left ${ManagerStyle.manager_wrapper}`}>
                                <div className={`${ManagerStyle.manager_content}`}>
                                    <WrapperHeader title={'个人概况'} iconStatus={true} />
                                    <CommonCharts idName={`managerChart`} ref={`managerCharts`} styles={{width: '100%', height: '380px'}} />
                                </div>
                            </div>
                            <div className={`float_right ${ManagerStyle.manager_wrapper}`}>
                                <div className={`${ManagerStyle.manager_content}`}>
                                    <WrapperHeader title={'个人履历'} iconStatus={true} />
                                    <Table style={{width: '100%', height: '370px'}} className={`mt_10 table_top_border`} columns={columns} dataSource={dataSource || []} pagination={dataSource && dataSource.length > 10 ? {} : false} />
                                </div>
                            </div>
                        </div>

                        <div className={`mt_20 ${ManagerStyle.manager_sub}`}>
                            <WrapperHeader title={'旗下产品'} iconStatus={true} />
                            <HomeTable _initData={this._getSubjectOrg.bind(this)} />
                        </div>
                    </Content>
                </Layout>
            </div>
        )
    }
}
