import 'core-js'
import 'raf/polyfill'
import 'core-js/stable'
import 'core-js/es/map'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import Router from  './Routes/Routes'
import * as serviceWorker from './serviceWorker'
import { ConfigProvider } from 'antd'
import locale from 'antd/es/locale/zh_CN'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

let store = createStore((state = {}, action) => state)

ReactDOM.render( <Provider store={store}><ConfigProvider locale={locale}><Router /></ConfigProvider></Provider>, document.getElementById('root'))
// serviceWorker.unregister()
serviceWorker.register()