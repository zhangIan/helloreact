import { axios } from '../Utils/index'

const exportDetailsExcel = async (params) => {
    console.log(params)
    const data = await axios({
        url: '/api/hszcpz/exportExcel/produceProductExcelFile',
        method: 'POST',
        data: JSON.stringify({
            ...params
        })
    })
    if (data) {
        return data
    } else {
        return { message: 'error'}
    }
}

const downloadExcel = async (params) => {
    const data = await axios({
        url: '/api/hszcpz/exportExcel/produceProductExcelFile',
        method: 'POST',
        data: JSON.stringify({
            ...params
        })
    })

    if (data) {
        return data
    }
}

export {
    exportDetailsExcel,
    downloadExcel
}
