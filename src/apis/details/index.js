import { axios } from '../../Utils/index'

const _getRankingData = async (params) => {
    let data = null
    if (params) {
         data = await axios({
            url: '/api/hszcpz/product/yearRankings',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false }
    }
}

const _sceneSocketMarket = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/api/hszcpz/product/getIssueRightTotalNav',
            method: 'POST',
            data: JSON.stringify({
                ...params
            })
        })
    } else {
        return { message: 'error', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

const _sceneMarketVar = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/api/hszcpz/product/getKeyMarketVariable',
            method: 'POST',
            data: JSON.stringify({
                ...params
            }) 
        })
    } else {
        return { message: '参数为空', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

const _sceneMarketNeutral = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/api/hszcpz/product/getMarketPerformanceNeutral',
            method: 'POST',
            data: JSON.stringify({
                ...params
            }) 
        })
    } else {
        return { message: '参数为空', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

const _sceneBondMarket = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/api/hszcpz/product/getMarketPerformanceBond',
            method: 'POST',
            data: JSON.stringify({
                ...params
            }) 
        })
    } else {
        return { message: '参数为空', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}

const _sceneFuture = async (params) => {
    let data = null
    if (params) {
        data = await axios({
            url: '/api/hszcpz/product/getMarketPerformanceFuture',
            method: 'POST',
            data: JSON.stringify({
                ...params
            }) 
        })
    } else {
        return { message: '参数为空', status: false}
    }

    if (data) {
        return data
    } else {
        return { message: 'error', status: false}
    }
}


export  {
    _getRankingData,
    _sceneSocketMarket,
    _sceneMarketVar,
    _sceneMarketNeutral,
    _sceneBondMarket,
    _sceneFuture
}