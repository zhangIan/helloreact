import Home from "../Component/Home/Home";
import Login from "../Component/Login/Login";
import Register from '../Component/Register/register'
import Attention from '../Component/attention/attention'
import FirstRegister from '../Component/first_register/register'
import Product from '../Component/product/product'
import Advisor from '../Component/investment_advisor/advisor'
import SystemList from '../Component/system_list'
import Manager from '../Component/investment_manager/manager.js'

export default [
    {
        name: '首页',
        path: '/',
        component: Home
   },
    {
        name: '首页',
        path: '/home',
        component: Home
   },
   {
        name: '注册',
        path: '/register',
        component: Register
    },
    {
        name: '登录',
        path: '/first-login',
        component: FirstRegister
    },
    {
        name: '登录',
        path: '/login',
        component: Login
    },
    {
        name: '关注',
        path: '/attention',
        component: Attention
    },
    {
        name: '产品详情',
        path: '/product',
        component: Product 
    },
    {
        name: '投顾',
        path: '/advisor',
        component: Advisor
    },
    {
        name: '统一授权登录平台',
        path: '/system-list',
        component: SystemList
    }
    ,
    {
        name: '投资经理页面',
        path: '/manager',
        component: Manager
    }
]