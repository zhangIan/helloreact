const proxy = require('http-proxy-middleware')

module.exports = function(app) {
 app.use(proxy('/api',
   {
    //  target: "http://192.168.122.193:80/HSZCPZ",
    //  target: "http://192.168.0.125:8090/netFilter/",
    // target: "http://192.168.122.190:8090/netFilter/",
    //  target: "http://192.168.0.125:8090/netFilter/",
    //  target: "http://192.168.1.51:8090/netFilter/",
     target: "http://192.168.1.51:9900",
    // target: "https://www.fastmock.site/mock/851ac391e4d1f5202abf82abadfe4d4b/hszcpz",
     changeOrigin: true,
     "secure": false,
     pathRewrite: {
                 "^/api": "/api"
             }
   }),
   proxy('/download',
   {
     target: "http://192.168.1.51:80/HSZCPZ/",
    //  target: "http://192.168.0.175:80/HSZCPZ/",
    //  target: "http://192.168.0.125:8090/HSZCPZ/",
    // target: "http://192.168.122.193:80/HSZCPZ/",
    // target: "https://www.fastmock.site/mock/851ac391e4d1f5202abf82abadfe4d4b/hszcpz",
     changeOrigin: true,
     "secure": false,
     pathRewrite: {
                 "^/download": "/"
             }
   }))
}
