import React, { Component } from 'react'
import 'antd/dist/antd.css';
import './register.sass'
import LogoImg from "../../Image/logo.png"

export default class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    UNSAFE_componentWillMount () {}

    render () {
        return (
            <div className="container">
                <header className="container-header">
                    <div className="logo-img">
                        <img src={LogoImg} alt="logo img" />
                    </div>
                </header>

                <div className="content"></div>
            </div>
        )
    }
}