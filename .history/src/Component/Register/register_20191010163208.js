import React, { Component } from 'react'
import 'antd/dist/antd.css'
import './register.sass'
import LogoImg from '../../Image/logo.png'
import Swiper from 'swiper/dist/js/swiper.js'
import { Tabs } from 'antd'

const { TabPane } = Tabs

export default class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            size: 'small',
            defaultActiveKey: '1'
        }
    }

    change = key => {
        console.log(key)
        this.setState({
            defaultActiveKey: key
        })
    }

    componentDidMount () {
        ;(() => {
            new Swiper('.swiper-container', {
                autoplay: true,
                loop: true,
                pagination: {
                  el: '.swiper-pagination',
                }
              })
        })()
    }

    render () {
        return (
            <div className="container">
                <header className="container-header">
                    <div className="logo-img">
                        <img src={LogoImg} alt="logo img" />
                    </div>
                </header>

                <div className="content">
                    <div className = "swiper-container" >
                        <div className = "swiper-wrapper" >
                            < div className = "swiper-slide" >
                            < div className = "box4" />
                            </div> 
                            <div className = "swiper-slide" > 
                            <div className="box1"/>
                            </div> 
                            <div className = "swiper-slide" >
                            <div className ="box2"/>
                            </div> 
                            <div className = "swiper-slide" >
                            < div className ="box3"/>
                            </div>
                        </div> 
                        <div className = 'swiper-pagination' > </div>
                    </div>


                    <div className="form-content">
                        <Tabs activeKey={this.state.defaultActiveKey} onChange={this.change} size={this.state.size}>
                            <TabPane tab="注册" key="1"></TabPane>
                            <TabPane tab="登录" key="2"></TabPane>
                        </Tabs>
                    </div>
                </div>
            </div>
        )
    }
}