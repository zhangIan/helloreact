import React, {Component} from 'react';
import 'antd/dist/antd.css';
import Swiper from 'swiper/dist/js/swiper.js'

import 'swiper/dist/css/swiper.min.css'
import './Login.scss'
import Img from "../../Image/logo.png"
import Imgs from "../../Image/02.png"
import { axios } from '../../Utils/index'
import qs from 'querystring'

import {
Layout, Tabs, Form,  Input, Button, Radio, message
} from 'antd';
import FormItem from 'antd/lib/form/FormItem';

const {
  Header,
  Sider,
  Content
} = Layout;
const {
  TabPane
} = Tabs;

class Login extends Component {
  constructor(props) {
    super(props)
    this.state ={
      defaultActiveKey: '1',
      codeImage: ''
    }
  }

  _handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  _sendMsg = () => {
    const phone = document.getElementById('login_phone').value
    if (phone) {
      axios({
        method: 'post',
        url: '/api/account/sendCode',
        data: qs.stringify({ "phone": phone })
      })
      .then(data => {
        console.log(data)
        if (data.result === 'success') {
          message.info(data.reason)
        } else {
          message.info('短信验证码发送失败，请稍后再试')
        }
      })
    }
  }

  _handlerRegister = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios({
          method: 'post',
          url: '/api/account/register',
          data: qs.stringify({...values})
        })
        .then(data => {
          if (data.result === 'success') {
            message.info(data.reason)
          } else {
            message.info('注册失败，请稍后再试')
          }
        })
      }
    })
  }

  _handlerLogin = (e) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios({
          method: 'post',
          url: '/api/account/login',
          data: qs.stringify({...values})
        })
        .then(data => {
          if (data.result === 'success') {
            message.info(data.reason)
          } else {
            message.info('注册失败，请稍后再试')
          }
        })
      }
    })
  }

  _getImagCode = () => {
    axios({
      method: 'post',
      url: '/api/account/imgCode',
      data: qs.stringify({})
    })
    .then(data => {
      console.log(data)
      this.setState({
        codeImage: data
      })
      if (data.result === 'success') {

      } else {

      }
    })
  }

  _checkActive = (key) => {
    this.setState({
      defaultActiveKey: key
    })
  }

  _goRegister = () => {
    console.log(this.state)
    this.setState({
      defaultActiveKey: '1'
    })
  }

  componentDidMount() {
    this._getImagCode()
    ;(() => {
      new Swiper('.swiper-container', {
        autoplay: true,
        loop: true,
        pagination: {
          el: '.swiper-pagination',
        }
      });
    })()
  }
  
  render() {
  const { getFieldDecorator } = this.props.form;
  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24
      },
      sm: {
        span: 7
      },
    },
    wrapperCol: {
      xs: {
        span: 24
      },
      sm: {
        span: 17
      },
    },
  };

    return ( 
      <div>
         < Layout >
           <Header >
            < div className = "logo" >
              <img src = {Img} alt = "" />
            </div>
          </Header> 
          < Layout >
            <Sider  className="siders" />
              <Content > 
               < div className = "App" >
                 <div className = "swiper-container" >
                  <div className = "swiper-wrapper" >
                    < div className = "swiper-slide" >
                      < div className = "box4" />
                    </div> 
                    <div className = "swiper-slide" > 
                     <div className="box1"/>
                    </div> 
                    <div className = "swiper-slide" >
                      <div className ="box2"/>
                     </div> 
                    <div className = "swiper-slide" >
                      < div className ="box3"/>
                    </div>
                  </div> 
                  <div className = 'swiper-pagination' > </div>
                 </div> 
                </div>
                <div className="titles">
                  < div className="background">
                     <div id="name" className="mains">
                      < img src = {Imgs} alt = ""/>
                    </div>


                    < div id = "name" className = "login" >
                      < Tabs activeKey = {this.state.defaultActiveKey}
                      onChange = {this._checkActive} >
                        <TabPane tab = "注册" key = "1" >
                         <div className="logins">
                            <Form onSubmit = {this._handlerRegister} className = "register-form" >
                              <Form.Item > {
                                getFieldDecorator('phone', {
                                  rules: [{
                                    required: false,
                                    message: '请输入手机号码!'
                                  }],
                                })(
                                  <Input id="phone" placeholder = "手机号" /> ,
                                  )
                                } 
                              </Form.Item> 
                              <Form.Item> {
                                   getFieldDecorator('verCode', {
                                     rules: [{
                                       required: false,
                                       message: '请输入手机验证码!'
                                     }],
                                   })( 
                                     <Input placeholder="手机验证码" name="code" style={{width:'233px'}} />)} 
                                     <Button style = {
                                       {
                                         marginLeft: '5px'
                                       }
                                     }
                                     type = "primary"
                                     onClick= { () => { this._sendMsg() }}
                                     className = "login-form-button" >
                                      发送验证码
                                       </Button>
                              </Form.Item>
                              <Form.Item > {
                                getFieldDecorator('password', {
                                  rules: [{
                                    required: false,
                                    message: '请输入密码!'
                                  }],
                                })(
                                  <Input type="password" placeholder = "请输入密码" /> ,
                                  )
                                } 
                              </Form.Item>
                              <FormItem>
                                {
                                  getFieldDecorator('repeatPassword', {
                                    rules: [{
                                      required: false,
                                      message: '请在此输入密码'
                                    }]
                                  })(<Input type="password" placeholder = "请再次输入密码" />)
                                }
                              </FormItem>  
                              < Form.Item >
                                    < Button style = {
                                      {
                                        width: '340px',
                                        textAlign: 'center'
                                      }
                                    }
                                    type = "primary"
                                    htmlType = "submit"
                                    className = "login-form-button" >
                                      注册 
                                      </Button>
                                </Form.Item> 
 
                              <Form.Item > {
                                    getFieldDecorator('remember', {
                                      valuePropName: 'radio',
                                      initialValue: false,
                                    })( 
                                    < Radio><span style={{fontSize:'12px'}}> 我已阅读并同意汇升投资《 用户服务协议》 及《 隐私保护指引》 </span></Radio > )
                                    }
                                </Form.Item>
                              < Form.Item > {
                                  getFieldDecorator('remembers', {
                                    valuePropName: 'radio',
                                    initialValue: false,
                                  })( 
                                  <Radio > 
                                    <span style = { {fontSize: '13px',width: '50px'}}>本人承诺符合特定的合格投资者条件 </span></Radio > )
                                } 
                                </Form.Item> 
                              </Form>
                            </div>
                        </TabPane> 
                        <TabPane tab = "登录"key = "2" >
                          <div className="logins">
                            < Form {
                              ...formItemLayout
                            }
                            onSubmit = {
                              this.handleSubmits
                            }
                            className = "login-form" >
                              <Form.Item label="手机号"> {
                                getFieldDecorator('usernames', {
                                  rules: [{
                                    required: false,
                                    message: '请输入手机号!'
                                  }],
                                })( <
                                  Input placeholder = "手机号" / > ,
                                )
                              } 
                              </Form.Item>  
                              < Form.Item label = "验证码" > {
                                getFieldDecorator('passwords', {
                                  rules: [{
                                    required: false,
                                    message: 'Please input your Password!'
                                  }],
                                })(
                                  <Input type = "text"
                                  style={{width:'130px'}}
                                  placeholder = "图形验证码" />
                                )}
                                <Button style={{width: '50px', marginLeft: '5px'}}>
                                  <img src={this.state.codeImage} alt="" />
                                </Button>
                              </Form.Item>
                               < Form.Item label = "手机验证码" > {
                                   getFieldDecorator('code', {
                                     rules: [{
                                       required: false,
                                       message: 'Please input your Password!'
                                     }],
                                   })( 
                                     <Input style={{width:'130px'}} type = "text" placeholder = "手机验证码" />)} 
                                     <Button style = {
                                       {
                                         marginLeft: '5px'
                                       }
                                     }
                                     type = "primary"
                                     htmlType = "submit"
                                     className = "login-form-button" >
                                      发送验证码
                                       </Button>
                              </Form.Item> 
                              <Form.Item >
                                <div style={{width:'300px',textAlign:'center'}}>
                              <Button  type = "primary"
                                htmlType = "submit"
                                className = "login-form-button" >
                              登录 
                              </Button> 
                              <p>
                                没有账号?<a onClick={() => { this._goRegister() }}>点击注册</a>
                              </p>
                              </div>
                            </Form.Item> 
                            </Form>
                          </div>
                        </TabPane> 
                      </Tabs>,
                    </div>
                  </div>
                </div>
              </Content> 
          </Layout>
          </Layout>
        
      </div>
    )
  }
}
const WrappedNormalLoginForm = Form.create({
  name: 'login'
})(Login);
export default WrappedNormalLoginForm